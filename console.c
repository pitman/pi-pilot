/*
 * File:   console.c
 *
 * Autopilot for UAV
 *
 * Print messages via USB CDC
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <p32xxxx.h>
#include "usb.h"
#include "usb_function_cdc.h"
#include "console.h"
#include "FreeRTOS.h"
/* Project setting includes */
#include "projdefs.h"

void vConsolePrintData(char *pcData)
{
    if(USBUSARTIsTxTrfReady())
    {
        putsUSBUSART(pcData);
        vTaskDelay(10/portTICK_RATE_MS);
    }
}

void vConsolePrintDataROM(const ROM char *pcData)
{
    if(USBUSARTIsTxTrfReady())
    {
        putrsUSBUSART(pcData);
        vTaskDelay(10/portTICK_RATE_MS);
    }
}
