/*
 * File:   autopilot.c
 *
 * Autopilot for UAV
 *
 * Autopilot service tasks for each type of UAVs
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <p32xxxx.h>
#include <xc.h>
#include <sys/attribs.h>
#include <math.h>

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "croutine.h"
#include "timers.h"
#include "queue.h"

/* Hardware specific includes. */
#include "ConfigPerformance.h"
#include "pwm-driver.h"
#include "pid.h"

/* Project setting includes */
#include "projdefs.h"
#include "autopilot.h"

/* Queue for the signals to AUTOPILOT code */
extern xQueueHandle xQueueAUTOPILOTSIG;

/* Main data structure of Pi-Pilot */
extern PROJDEFS_PIPILOT pilot;

/* Autopilot for non motor planer */
void vAUTOPILOT_PLANER(void *pvParameters)
{
    static AUTOPILOT_SIG_TYPE prvASIG;
    float pfPID_ROLL, pfPID_PITCH, pfPID_YAW;

    for(;;)
    {
        /* Receive new a signal for autopilot task */
        xQueueReceive(xQueueAUTOPILOTSIG, &prvASIG, portMAX_DELAY);

        switch(prvASIG)
        {
        case AUTOPILOT_SIG_NULL:
            pilot.pwm.hpwm[AUTOPILOT_PLANER_AILERON_LEFT] = pilot.pwm.hpwm_null_point[AUTOPILOT_PLANER_AILERON_LEFT];
            pilot.pwm.hpwm[AUTOPILOT_PLANER_AILERON_RIGHT] = pilot.pwm.hpwm_null_point[AUTOPILOT_PLANER_AILERON_RIGHT];
            pilot.pwm.hpwm[AUTOPILOT_PLANER_ELEVATOR] = pilot.pwm.hpwm_null_point[AUTOPILOT_PLANER_ELEVATOR];
            break;
        case AUTOPILOT_SIG_EULER_REFRESH:
            /* Calculate control signals from new Euler angles */
            pfPID_ROLL = fPidUpdate(&pilot.roll_pid, pilot.course.roll, pilot.roll);
            pfPID_PITCH = fPidUpdate(&pilot.pitch_pid, pilot.course.pitch, pilot.pitch);
            /* Set new value for servo PWM */
            pilot.pwm.hpwm[AUTOPILOT_PLANER_AILERON_LEFT] = pilot.pwm.hpwm_null_point[AUTOPILOT_PLANER_AILERON_LEFT] +
                    pilot.pwm.hpwm_servo_mode[AUTOPILOT_PLANER_AILERON_LEFT] * (unsigned int)pfPID_ROLL;
            if (pilot.pwm.hpwm[AUTOPILOT_PLANER_AILERON_LEFT] < pilot.pwm.hpwm_min[AUTOPILOT_PLANER_AILERON_LEFT])
                pilot.pwm.hpwm[AUTOPILOT_PLANER_AILERON_LEFT] = pilot.pwm.hpwm_min[AUTOPILOT_PLANER_AILERON_LEFT];
            else if (pilot.pwm.hpwm[AUTOPILOT_PLANER_AILERON_LEFT] > pilot.pwm.hpwm_max[AUTOPILOT_PLANER_AILERON_LEFT])
                pilot.pwm.hpwm[AUTOPILOT_PLANER_AILERON_LEFT] = pilot.pwm.hpwm_max[AUTOPILOT_PLANER_AILERON_LEFT];
            pilot.pwm.hpwm[AUTOPILOT_PLANER_AILERON_RIGHT] = pilot.pwm.hpwm_null_point[AUTOPILOT_PLANER_AILERON_RIGHT] -
                    pilot.pwm.hpwm_servo_mode[AUTOPILOT_PLANER_AILERON_RIGHT] * (unsigned int)pfPID_ROLL;
            if (pilot.pwm.hpwm[AUTOPILOT_PLANER_AILERON_RIGHT] < pilot.pwm.hpwm_min[AUTOPILOT_PLANER_AILERON_RIGHT])
                pilot.pwm.hpwm[AUTOPILOT_PLANER_AILERON_RIGHT] = pilot.pwm.hpwm_min[AUTOPILOT_PLANER_AILERON_RIGHT];
            else if (pilot.pwm.hpwm[AUTOPILOT_PLANER_AILERON_RIGHT] > pilot.pwm.hpwm_max[AUTOPILOT_PLANER_AILERON_RIGHT])
                pilot.pwm.hpwm[AUTOPILOT_PLANER_AILERON_RIGHT] = pilot.pwm.hpwm_max[AUTOPILOT_PLANER_AILERON_RIGHT];
            pilot.pwm.hpwm[AUTOPILOT_PLANER_ELEVATOR] = pilot.pwm.hpwm_null_point[AUTOPILOT_PLANER_ELEVATOR] -
                    pilot.pwm.hpwm_servo_mode[AUTOPILOT_PLANER_ELEVATOR] * (unsigned int)pfPID_PITCH;
            if (pilot.pwm.hpwm[AUTOPILOT_PLANER_ELEVATOR] < pilot.pwm.hpwm_min[AUTOPILOT_PLANER_ELEVATOR])
                pilot.pwm.hpwm[AUTOPILOT_PLANER_ELEVATOR] = pilot.pwm.hpwm_min[AUTOPILOT_PLANER_ELEVATOR];
            else if (pilot.pwm.hpwm[AUTOPILOT_PLANER_ELEVATOR] > pilot.pwm.hpwm_max[AUTOPILOT_PLANER_ELEVATOR])
                pilot.pwm.hpwm[AUTOPILOT_PLANER_ELEVATOR] = pilot.pwm.hpwm_max[AUTOPILOT_PLANER_ELEVATOR];
            break;
        case AUTOPILOT_SIG_BAR_ALT:
            /* Calculate control signals from new Altitude data */
            break;
        default:
            /* Unknown signal */
            break;
        }
    }
    vTaskDelete(NULL);
}
