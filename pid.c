/*
 * File:   pid.c
 *
 * Autopilot for UAV
 *
 * The PID source
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <p32xxxx.h>

#include "pid.h"
#include "projdefs.h"

/* Update control for PWM */
float fPidUpdate(PID_STRUCT *pid, float position_hold, float position_measured)
{
    float pfTempP, pfTempd, pfTempi, err;

    err = position_hold - position_measured;
    /* Calculate proportional value */
    pfTempP = pid->Kp_gain * err;
    /* Calculate integral value */
    pfTempi = pid->int_prev + pid->Ki_gain * err;
    /* Limit max integral value */
    if (pfTempi > pid->Ki_max)
        pfTempi = pid->Ki_max;
    else if (pfTempi < -pid->Ki_max)
        pfTempi = -pid->Ki_max;
    pid->int_prev = pfTempi;
    /* Calculate derivative value */
    pfTempd = (err - pid->err_prev) * pid->Kd_gain;
    pid->err_prev = err;
    return pfTempP + pfTempi + pfTempd;
}
