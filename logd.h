/*
 * File:   log.h
 *
 * Autopilot for UAV
 *
 * The header file for logging subsystem.
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef LOGD_H
#define	LOGD_H

#ifdef	__cplusplus
extern "C" {
#endif

typedef struct
{
    unsigned long time;
    unsigned char flag;
    char message[64];
} LOGD_MESSAGE_STRUCT;

/* Size of queue elements */
#define LOGD_MESSAGE_QUEUE_SIZE  16
/* Size of char buffer */
#define LOGD_MESSAGE_CHAR_BUFFER 64

/* Information level of messaging */
/* a TODO */
#define LOGD_TODO     0
/* a BUG */
#define LOGD_BUG      1
/* INFO */
#define LOGD_INFO     2
/* ERROR */
#define LOGD_ERROR    3
/* WARNING */
#define LOGD_WARN     4
/* DEBUG */
#define LOGD_DEBUG    5

/* Defines for logging the startup process */
/* logging queue creation */
#define LOGD_INIT_QUEUE_LED        0
#define LOGD_INIT_QUEUE_CONFD      1
#define LOGD_INIT_QUEUE_MSG        2
#define LOGD_INIT_QUEUE_I2C1       3
#define LOGD_INIT_QUEUE_I2CRWPKT   4
#define LOGD_INIT_QUEUE_I2CRWRES   5
#define LOGD_INIT_QUEUE_FLYSIG     6
#define LOGD_INIT_QUEUE_AUTOPILOTSIG 7
#define LOGD_INIT_QUEUE_PPM_DECODER  8
#define LOGD_INIT_QUEUE_RFLINK_TX  9
#define LOGD_INIT_QUEUE_RFLINK_RX 10

/* logging task creation */
#define LOGD_INIT_TASK_LED        0
#define LOGD_INIT_TASK_CONF       1
#define LOGD_INIT_TASK_MSG        2
#define LOGD_INIT_TASK_CALC       3
#define LOGD_INIT_TASK_CONSOLE    4
#define LOGD_INIT_TASK_INIT       5
#define LOGD_INIT_SENS_SLOW_DATA  6
#define LOGD_INIT_TASK_FLY        7
#define LOGD_INIT_TASK_PPM        8
#define LOGD_INIT_TASK_RFLINK_TX  9
#define LOGD_INIT_TASK_RFLINK_RX 10
#define LOGD_INIT_TASK_MAVLINKD  11

/* Send log messages to USB-TTY */
void vLogdPrint(unsigned char flag, char *messages, unsigned char time);

/* Send log message from ROM to USB-TTY */
void vLogdPrintROM(unsigned char flag, const char *messages, unsigned char time);

#ifdef	__cplusplus
}
#endif

#endif	/* LOGD_H */

