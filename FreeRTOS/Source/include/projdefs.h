/*
    FreeRTOS V7.5.3 - Copyright (C) 2013 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

    ***************************************************************************
     *                                                                       *
     *    FreeRTOS provides completely free yet professionally developed,    *
     *    robust, strictly quality controlled, supported, and cross          *
     *    platform software that has become a de facto standard.             *
     *                                                                       *
     *    Help yourself get started quickly and support the FreeRTOS         *
     *    project by purchasing a FreeRTOS tutorial book, reference          *
     *    manual, or both from: http://www.FreeRTOS.org/Documentation        *
     *                                                                       *
     *    Thank you!                                                         *
     *                                                                       *
    ***************************************************************************

    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation >>!AND MODIFIED BY!<< the FreeRTOS exception.

    >>! NOTE: The modification to the GPL is included to allow you to distribute
    >>! a combined work that includes FreeRTOS without being obliged to provide
    >>! the source code for proprietary components outside of the FreeRTOS
    >>! kernel.

    FreeRTOS is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  Full license text is available from the following
    link: http://www.freertos.org/a00114.html

    1 tab == 4 spaces!

    ***************************************************************************
     *                                                                       *
     *    Having a problem?  Start by reading the FAQ "My application does   *
     *    not run, what could be wrong?"                                     *
     *                                                                       *
     *    http://www.FreeRTOS.org/FAQHelp.html                               *
     *                                                                       *
    ***************************************************************************

    http://www.FreeRTOS.org - Documentation, books, training, latest versions,
    license and Real Time Engineers Ltd. contact details.

    http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
    including FreeRTOS+Trace - an indispensable productivity tool, a DOS
    compatible FAT file system, and our tiny thread aware UDP/IP stack.

    http://www.OpenRTOS.com - Real Time Engineers ltd license FreeRTOS to High
    Integrity Systems to sell under the OpenRTOS brand.  Low cost OpenRTOS
    licenses offer ticketed support, indemnification and middleware.

    http://www.SafeRTOS.com - High Integrity Systems also provide a safety
    engineered and independently SIL3 certified version for use in safety and
    mission critical applications that require provable dependability.

    1 tab == 4 spaces!
*/

#ifndef PROJDEFS_H
#define PROJDEFS_H

#include <xc.h>
#include <GenericTypeDefs.h>
/* Kernel includes. */
#include "i2c-driver.h"
#include "bmp085.h"
#include "lsm303dlhc.h"
#include "l3g4200d.h"
#include "autopilot.h"
#include "pwm-driver.h"
#include "pid.h"
#include "ppm-driver.h"
#include "rf-link.h"
#include "mavlink/v1.0/common/mavlink.h"

/* Defines the prototype to which task functions must conform. */
typedef void (*pdTASK_CODE)( void * );

#define pdFALSE		( ( portBASE_TYPE ) 0 )
#define pdTRUE		( ( portBASE_TYPE ) 1 )

#define pdPASS									( pdTRUE )
#define pdFAIL									( pdFALSE )
#define errQUEUE_EMPTY							( ( portBASE_TYPE ) 0 )
#define errQUEUE_FULL							( ( portBASE_TYPE ) 0 )

/* Error definitions. */
#define errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY	( -1 )
#define errNO_TASK_TO_RUN						( -2 )
#define errQUEUE_BLOCKED						( -4 )
#define errQUEUE_YIELD							( -5 )

#define SYS_FREQ        (80000000L)

#define	GetSystemClock() 	(80000000ul)
#define	GetPeripheralClock()	(GetSystemClock()/(1 << OSCCONbits.PBDIV))
#define	GetInstructionClock()	(GetSystemClock())

/* ---------------------------- */
/* Some defines for debug start */

#define DEBUG_PRINT_ENABLE      1
//#define DEBUG_I2C_BUFFER_ENABLE 1
#define DEBUG_SIGNAL_ENABLE     1

/* Some defines for debug stop  */
/* ---------------------------- */

/* -------------------------------- */
/* Defines for some constants start */

/* Define for Pi */
#define PROJDEFS_CONST_PI 3.14159265

/* Define RAD to Degree */
#define PROJDEFS_CONST_RAD_2_DEG 57.295779513082320876798154814105

/* Define Degree to RAD */
#define PROJDEFS_CONST_DEG_2_RAD 0.01745329251994

/* Complimentary filter default */
#define PROJDEFS_CONST_ACC_CF     0.1
#define PROJDEFS_CONST_HYR_CF     (1 - PROJDEFS_CONST_ACC_CF)

#define PROJDEFS_SENS_FILTER_COMPLIMENTARY
//#define PROJDEFS_SENS_FILTER_MADGWICK
//#define PROJDEFS_SENS_FILTER_MADGWICK_WITHMAG

/* Defines for some constants stop */
/* -------------------------------- */

/* -------------------------------- */
/* Defines for a char buffers start */

#define CONSOLE_BUFFER_SIZE 64

/* Defines for a char buffers stop  */
/* -------------------------------- */

/* -------------------------- */
/* Defines for FLY FSM start  */

/* State of FLYing */
typedef enum {
    FF_IDLE,
    FF_SENS_SKIP,
    FF_SENS_COLL,
    FF_START,
    FF_FLY_ON,
    FF_FLY,
    FF_LANDING,
    FF_STOP
} PROJDEFS_FLYFSM;

/* Signal for FLYFSM */
typedef enum {
    SIG_CMD_NONE           = 0,
    SIG_CMD_BUTTON         = 1,
    SIG_CMD_TOFF_FREEFALL  = 2,
    SIG_CMD_RC_UPDATE      = 3,
    SIG_CMD_MAVLINK        = 4,
} PROJDEFS_FLYFSM_SIG;

/* Structure for signals */
typedef struct {
    PROJDEFS_FLYFSM_SIG signal;
    BYTE                  on:1;
} PROJDEFS_FLYFSM_SIG_STRUCT;

#define PROJDEFS_FLYSIG_DEEP 4

#define FLY_START_WAIT      10
#define FLY_TIME_MAX        40
#define FLY_HIGHT_MAX      600
#define FLY_TAKE_OFF_SPEED   2
#define FLY_TAKE_OFF_TIME   30
#define FLY_LANDING_SPEED    1
#define FLY_LANDING_TIME    30

#define FLY_HIGHT_NEAR     250

/* Take off mode of the aircraft */
typedef enum {
    TOFF_UNKNOWN,
    TOFF_MANUAL,
    TOFF_CATAPULT,
} PROJDEFS_TAKEOFF_MODE;

/* Types of aircraft */
typedef enum {
    TYPE_PLANER,
    TYPE_AIRCRAFT,
    TYPE_COPTER_4_X,
    TYPE_COPTER_4_P,
} PROJDEFS_AIRCRAFT_TYPE;

/* Defines for FLY FSM stop   */
/* -------------------------- */

/* Definitions for LEDs start */

typedef union
{
    UINT32 all_data;

    struct
    {
        /* All time ON state */
        UINT16 on      :1;
        /* The type of a signal */
        UINT16 type    :4;
        /* The REFRESH flag */
        UINT16 refresh :1;
        /* The MAIN FSM of a LED */
        UINT16 fsm     :6;
        /* The counter of time */
        UINT16 cnt     :6;
    };
} PROJDEFS_LED_STRUCT;

typedef struct
{
    /* State of LED */
    BYTE state     :1;
    BYTE repeate   :1;
    BYTE cnt       :6;
} PROJDEFS_LED_ARRAY;

/* New type for Queue of LEDs */
typedef union
{
    BYTE all_data;
    
    struct
    {
        BYTE led_num    :3;
        BYTE on         :1;
        BYTE type       :4;
    };
} PROJDEFS_LED_QUEUE;

/* All time pulse 0.5s ON - 0.5s OFF */
#define PROJDEFS_LED_PULSE  0x01
/* All time blink the SOS signal */
#define PROJDEFS_LED_SOS    0x02
/* Once pulse 0.1s ON */
#define PROJDEFS_LED_BLINK   0x03
/* All time short pulse 0.1s ON - 0.9s OFF */
#define PROJDEFS_LED_0_1    0x04

/* GPS led */
#define PROJDEFS_LED_GPS    0x00
/* LIVE led */
#define PROJDEFS_LED_LIVE   0x01
/* LINK led */
#define PROJDEFS_LED_LINK   0x02
/* ARM led */
#define PROJDEFS_LED_ARM    0x03

/* All time ON flag mask */
#define PROJDEFS_LED_ON_MASK   0x8000
/* State mask */
#define PROJDEFS_LED_WORK_MASK 0xF800

#define LED_TRIS TRISG
#define LED_PORT PORTG
#define LED_LAT  LATG
#define LED_ODC  ODCG

#define LED_GPS  1 << 12
#define LED_LIVE 1 << 13
#define LED_COM  1 << 14
#define LED_ARM  1 << 15

#define LED_SET_ODC      ODCGSET =  LED_GPS | LED_LIVE | LED_COM | LED_ARM
#define LED_SET_OUTPUT   TRISGCLR = LED_GPS | LED_LIVE | LED_COM | LED_ARM
#define LED_SET_DEFAULT  LATGSET =  LED_GPS | LED_LIVE | LED_COM | LED_ARM

#define LED_LIVE_ON  LATGCLR = LED_LIVE
#define LED_LIVE_OFF LATGSET = LED_LIVE
#define LED_LIVE_TOG LATGINV = LED_LIVE

#define LED_GPS_ON   LATGCLR = LED_GPS
#define LED_GPS_OFF  LATGSET = LED_GPS

#define LED_COM_ON   LATGCLR = LED_COM
#define LED_COM_OFF  LATGSET = LED_COM
#define LED_COM_TOG  LATGINV = LED_COM

#define LED_ARM_ON   LATGCLR = LED_ARM
#define LED_ARM_OFF  LATGSET = LED_ARM
#define LED_ARM_TOG  LATGINV = LED_ARM

/* Definitions for LEDs stop  */
/* -------------------------- */

/* ------------------------------------- */
/* Definitions for Command button start  */

#define BTN_CMD_SET_INPUT TRISESET = 1 << 6;
#define BTN_CMD      PORTEbits.RE6

/* Definitions for Command button stop   */
/* ------------------------------------- */

/* ------------------------------------- */
/* Definitions for control MOSFETs start */

/* Buzzer's MOSFET */
#define BUZZ_SW            1 << 5
#define BUZZ_SW_SET_OUTPUT TRISECLR = BUZZ_SW
#define BUZZ_ON            LATESET = BUZZ_SW
#define BUZZ_OFF           LATECLR = BUZZ_SW

/* I2C1 bus sensors power control MOSFET */
#define SENS_SW            1 << 2
#define SENS_SW_SET_OUTPUT TRISCCLR = SENS_SW
#define SENS_SW_SET_ODC    ODCCSET = SENS_SW
#define SENS_ON            LATCCLR = SENS_SW
#define SENS_OFF           LATCSET = SENS_SW

/* Group 1 power control MOSFET */
#define PCHA1_SW            1
#define PCHA1_SET_ODC       ODCAbits.ODCA0 = 1
#define PCHA1_SW_SET_OUTPUT TRISAbits.TRISA0 = 0
#define PCHA1_ON            LATACLR = PCHA1_SW
#define PCHA1_OFF           LATASET = PCHA1_SW
#define PCHA1_TOG           LATAINV = PCHA1_SW

/* Group 2 power control MOSFET */
#define PCHA2_SW            1 << 8
#define PCHA2_SET_ODC       ODCEbits.ODCE8 = 1
#define PCHA2_SW_SET_OUTPUT TRISEbits.TRISE8 = 0
#define PCHA2_ON            LATECLR = PCHA2_SW
#define PCHA2_OFF           LATESET = PCHA2_SW
#define PCHA2_TOG           LATEINV = PCHA2_SW

/* Group 3 power control MOSFET */
#define PCHA3_SW            1 << 9
#define PCHA3_SET_ODC       ODCEbits.ODCE9 = 1
#define PCHA3_SW_SET_OUTPUT TRISEbits.TRISE9 = 0
#define PCHA3_ON            LATECLR = PCHA3_SW
#define PCHA3_OFF           LATESET = PCHA3_SW
#define PCHA3_TOG           LATEINV = PCHA3_SW

/* Group 4 power control MOSFET */
#define PCHA4_SW            1 << 9
#define PCHA4_SET_ODC       ODCAbits.ODCA9 = 1
#define PCHA4_SW_SET_OUTPUT TRISAbits.TRISA9 = 0
#define PCHA4_ON            LATACLR = PCHA4_SW
#define PCHA4_OFF           LATASET = PCHA4_SW
#define PCHA4_TOG           LATAINV = PCHA4_SW

/* Group 5 power control MOSFET */
#define PCHA5_SW            1 << 10
#define PCHA5_SET_ODC       ODCAbits.ODCA10 = 1
#define PCHA5_SW_SET_OUTPUT TRISAbits.TRISA10 = 0
#define PCHA5_ON            LATACLR = PCHA5_SW
#define PCHA5_OFF           LATASET = PCHA5_SW
#define PCHA5_TOG           LATAINV = PCHA5_SW

/* Group 6 power control MOSFET */
#define PCHA6_SW            1 << 1
#define PCHA6_SET_ODC       ODCAbits.ODCA1 = 1
#define PCHA6_SW_SET_OUTPUT TRISAbits.TRISA1 = 0
#define PCHA6_ON            LATACLR = PCHA6_SW
#define PCHA6_OFF           LATASET = PCHA6_SW
#define PCHA6_TOG           LATAINV = PCHA6_SW

/* Definitions for control MOSFETs stop  */
/* ------------------------------------- */

/* ------------------------------------- */
/* Definitions for signals of MEMS start */

#define BAR_NRST            1 << 1
#define BAR_NRST_SET_OUTPUT TRISCbits.TRISC1 = 0
#define BAR_NRST_ON         PORTCbits.RC1 = 0
#define BAR_NRST_OFF        PORTCbits.RC1 = 1

#define BAR_EOC             1 << 7
#define BAR_EOC_PORT        PORTC

#define BAR_GROUND_SAMPLE    25

/* Definitions for signals of MEMS stop  */
/* ------------------------------------- */

/* ------------------------------------- */
/*      Definitions for ADC start        */
// Define setup parameters for OpenADC10 function
// Turn module on | Ouput in integer format | Trigger mode auto | Enable autosample
#define ADCconfig1     ADC_FORMAT_INTG | ADC_CLK_MANUAL | ADC_AUTO_SAMPLING_OFF | ADC_SAMP_OFF
// ADC ref external | Disable offset test | Disable scan mode | Perform 2 samples | Use dual buffers | Use alternate mode
#define ADCconfig2     ADC_VREF_AVDD_AVSS | ADC_OFFSET_CAL_DISABLE | ADC_SCAN_OFF | ADC_SAMPLES_PER_INT_1 | ADC_BUF_16 | ADC_ALT_INPUT_OFF
// Use ADC internal clock | Set sample time
#define ADCconfig3     ADC_CONV_CLK_INTERNAL_RC | ADC_CONV_CLK_32Tcy
// Do not assign channels to scan
#define ADCconfigscan  SKIP_SCAN_ALL
//#define configport  ENABLE_AN2_ANA | ENABLE_AN4_ANA | ENABLE_AN6_ANA | ENABLE_AN8_ANA | ENABLE_AN10_ANA | ENABLE_AN12_ANA | ENABLE_AN14_ANA
#define ADCconfigport  ENABLE_AN12_ANA
/*        Definitions for ADC stop       */
/* ------------------------------------- */

/* ----------------------------------------- */
/* Defines for UltraSonic range meters start */

#define US_TRIG1             1 << 3
#define US_TRIG1_SET_OUTPUT  TRISCbits.TRISC3 = 0
#define US_TRIG1_ON          PORTCbits.RC3 = 1
#define US_TRIG1_OFF         PORTCbits.RC3 = 0

#define US_TRIG2             1 << 4
#define US_TRIG2_SET_OUTPUT  TRISAbits.TRISA4 = 0
#define US_TRIG2_ON          PORTAbits.RA4 = 1
#define US_TRIG2_OFF         PORTAbits.RA4 = 0

#define US_TRIG3             1 << 5
#define US_TRIG3_SET_OUTPUT  TRISAbits.TRISA5 = 0
#define US_TRIG3_ON          PORTAbits.RA5 = 1
#define US_TRIG3_OFF         PORTAbits.RA5 = 0

#define US_ECHO1             1 << 3
#define US_ECHO1_PORT        PORTBbits.RB3

#define US_ECHO2             1 << 5
#define US_ECHO2_PORT        PORTBbits.RB5

#define US_ECHO3             1 << 7
#define US_ECHO3_PORT        PORTBbits.RB7

#define US_ECHO4             1 << 9
#define US_ECHO4_PORT        PORTBbits.RB9

#define US_ECHO5             1 << 11
#define US_ECHO5_PORT        PORTBbits.RB11

#define US_ECHO6             1 << 13
#define US_ECHO6_PORT        PORTBbits.RB13

/* Defines for UltraSonic range meters stop  */
/* ----------------------------------------- */

/*           RFLINK MAVLINK start            */
#define PROJDEFS_CONF_MCU_FREE          5000

#define PROJDEFS_CONF_RFLINK_MSG_SIZE   MAVLINK_MAX_PACKET_LEN
#define PROJDEFS_CONF_RFLINK_MSG_DEEP   8

typedef struct
{
    char msg[PROJDEFS_CONF_RFLINK_MSG_SIZE];
    unsigned short len;
} PROJDEFS_CONF_RFLINK_MSG;

typedef union
{
    unsigned int all_data;

    struct {
        unsigned int enable:1;
        unsigned int tx_busy:1;
    };
} PROJDEFS_CONF_RFLINK_STATE;

typedef enum
{
    UART_NONE,
    UART_RFLINK,
    UART_GPS,
} PROJDEFS_CONF_UART_TYPE;

typedef struct
{
    unsigned int uart_port;
    unsigned int uart_cfg;
    unsigned int uart_ctrl;
    unsigned int uart_fifo;
    unsigned int uart_rate;
    unsigned int uart_flags;
} PROJDEFS_CONF_UART;

/* State of RX FSM RFLINK UART */
typedef enum
{
    RFLINK_RX_WAIT        = 0,
    RFLINK_RX_START_FOUND = 1,
    RFLINK_RX_SEQ         = 2,
    RFLINK_RX_SYSID       = 3,
    RFLINK_RX_COMPID      = 4,
    RFLINK_RX_MSG_TYPE    = 5,
    RFLINK_RX_PAYLOAD_RCV = 6,
    RFLINK_RX_CRC_1       = 7,
    RFLINK_RX_CRC_2       = 8,
    RFLINK_RX_ERROR       = 9,
} PROJDEFS_RFLINK_RX_STATE;

/* Define time for understanding witch link with ground lost */
/* unit is seconds, maximum value is 31 seconds */
#define PROJDEFS_MAVLINK_TIMEOUT  10

typedef union
{
    unsigned int all_data;

    struct {
        unsigned int enable:1;
        unsigned int lost_ground:1;
        unsigned int link_state:1;
        unsigned int link_prev_state:1;
        unsigned int force_data_send:1;
        unsigned int timeout_cnt:5;
        unsigned int msg_fsm:8;
        unsigned int ground_station_id:8;
    };
} PROJDEFS_CONF_MAVLINK;

/* How many MAVLINK messages send at the same time */
#define PROJDEFS_MAVLINK_MSG_CNT  4
/* MAVLINK shedule table size */
#define PROJDEFS_MAVLINK_SCHED_SIZE 8

/* Datastructure for MAVLINK message sheduler */
typedef struct
{
    /* ID of subsystem */
    unsigned char compid;
    /* message type */
    unsigned char msg_num;
    /* message period, milliseconds */
    unsigned int  msg_period;
    /* send message after this time */
    unsigned long start_time;
} PROJDEFS_MAVLINK_SHED_STRUCT;

/*           RFLINK MAVLINK stop             */
/* ----------------------------------------- */

/*      Defines for Configure task start     */
/* ----------------------------------------- */

/* ENUM for config messages */
typedef enum
{
    /* Power ON for the I2C1 bus */
    PROJDEFS_CONF_I2C1_PON,
    /* Power OFF for the I2C1 bus */
    PROJDEFS_CONF_I2C1_POFF,
    /* Power OFF/ON for the I2C1 bus */
    PROJDEFS_CONF_I2C1_PRST,
    /* Init I2C1 bus */
    PROJDEFS_CONF_I2C1_INIT,
    /* Fini I2C1 bus */
    PROJDEFS_CONF_I2C1_FINI,
    /* Configure the L3G4200D HYR sensor */
    PROJDEFS_CONF_L3G4200D,
    /* Configure the LSM303DLHC ACC/MAG sensor */
    PROJDEFS_CONF_LSM303DLHC,
    /* Configure the BMP085 BAR sensor */
    PROJDEFS_CONF_BMP085,
    /* Configure the OCx for hardware PWM*/
    PROJDEFS_CONF_HPWM,
    /* Read configure data from a SD card */
    PROJDEFS_CONF_READCFG,
    /* Configure main pilot structure from a reading data */
    PROJDEFS_CONF_PILOT,
    /* COnfigure PPM decoder */
    PROJDEFS_CONF_PPM_DECODER,
    /* COnfigure RF-link */
    PROJDEFS_CONF_RFLINK,
} PROJDEFS_CONF_ENUM;

/*      Defines for Configure task stop      */
/* ----------------------------------------- */

/* Main data structure of autopilot start */

typedef struct
{
    unsigned long        init_start;
    unsigned long init_queues_state;
    unsigned long  init_tasks_state;
    I2C_DRIVER_I2Cx            i2c1;
    I2C_DRIVER_I2Cx            i2c2;
    /* Main FLY FSM */
    PROJDEFS_FLYFSM       fly_state;
    /* Take OFF mode of the aircraft */
    PROJDEFS_TAKEOFF_MODE toff_mode;
    /* accel value only for catapult mode */
    float              acc_catapult;
    /* Type of aircraft */
    PROJDEFS_AIRCRAFT_TYPE aircraft;
    /* Autopilot's task handler */
    void *         xHandleAutopilot;
    /* Autopilot course structure */
    AUTOPILOT_COURSE_STRUCT  course;
    /* The Pressure sensor BMP085 structure */
    BMP085_SENSOR_CFG           bar;
    /* The ACC+MAG sensor LSM303DLHC structure */
    LSM303DLHC_SENSOR_CFG   acc_mag;
    /* The HYR sensor L3G4200D structure */
    L3G4200D_SENSOR_CFG         hyr;
    /* Calculated Altitude */
    float                  altitude;
    /* Ground altitude */
    float           ground_altitude;
    /* Pressure at sea level */
    float                        p0;
    /* Euler angles */
    /* Roll */
    float                      roll;
    /* Pitch */
    float                     pitch;
    /* Yaw */
    float                       yaw;
    /* Euler angular speed */
    /* Roll */
    float                roll_speed;
    /* Pitch */
    float               pitch_speed;
    /* Yaw */
    float                 yaw_speed;
    /* Complimentary filter define for ACC */
    float                cf_acc_def;
    /* Complimentary filter define for HYR */
    float                cf_hyr_def;
    /* Time for sensors callibration */
    unsigned long      sensors_time;
    /* Quaternion vars */
    float                        q1;
    float                        q2;
    float                        q3;
    float                        q4;
    /* PWM for servos */
    PWM_DRIVER_STRUCT           pwm;
    /* Input RC control capture */
    PPM_DRIVER_INRC              rc;
    /* Structure for PID of ROLL */
    PID_STRUCT             roll_pid;
    /* Structure for PID of PITCH */
    PID_STRUCT            pitch_pid;
    /* Structure for PID of YAW */
    PID_STRUCT              yaw_pid;
    /* Structure for PID of Altitude */
    PID_STRUCT              alt_pid;
    /* Load of MCU unit 100% is 1000 */
    unsigned short         mcu_load;
    /* Config of RFLINK UART */
    PROJDEFS_CONF_UART       rflink;
    /* State of RFLINK UART */
    PROJDEFS_CONF_RFLINK_STATE rflink_state;
    /* RX state of RFLINK UART */
    PROJDEFS_RFLINK_RX_STATE rflink_rx;
    /* Mavlink protocol data */
    mavlink_system_t mavlink_system;
    unsigned char   mav_system_type;
    unsigned char  mav_system_state;
    unsigned char mav_autopilot_type;
    unsigned char   mav_system_mode;
    unsigned int    mav_custom_mode;
    unsigned short mav_packet_rx_drops;
    unsigned short    mav_packet_rx_ok;
    PROJDEFS_CONF_MAVLINK   mav_msg;
    MAV_SYS_STATUS_SENSOR   sensors_present;
    MAV_SYS_STATUS_SENSOR   sensors_enabled;
    MAV_SYS_STATUS_SENSOR   sensors_health;
} PROJDEFS_PIPILOT;

/* Configure structure for Pipilot*/
typedef struct
{
    /* Take OFF mode of the aircraft */
    PROJDEFS_TAKEOFF_MODE toff_mode;
    /* accel value, only for catapult mode */
    float              acc_catapult;
    /* Type of aircraft */
    PROJDEFS_AIRCRAFT_TYPE aircraft;
    /* The Pressure sensor BMP085 structure */
    BMP085_SENSOR_CFG           bar;
    /* The ACC+MAG sensor LSM303DLHC structure */
    LSM303DLHC_SENSOR_CFG   acc_mag;
    /* The HYR sensor L3G4200D structure */
    L3G4200D_SENSOR_CFG         hyr;
    /* Complimentary filter define for ACC */
    float                cf_acc_def;
    /* Complimentary filter define for HYR */
    float                cf_hyr_def;
    /* Time for sensors callibration */
    unsigned long      sensors_time;
    /* PWM for servos */
    PWM_DRIVER_STRUCT           pwm;
    /* Input RC control capture */
    PPM_DRIVER_INRC              rc;
    /* Structure for PID of ROLL */
    PID_STRUCT             roll_pid;
    /* Structure for PID of PITCH */
    PID_STRUCT            pitch_pid;
    /* Structure for PID of YAW */
    PID_STRUCT              yaw_pid;
    /* Structure for PID of Altitude */
    PID_STRUCT              alt_pid;
    /* Enable/Disable RFLINK via UART1 */
    unsigned char     rflink_enable;
    /* Mavlink protocol data */
    mavlink_system_t mavlink_system;
    unsigned char   mav_system_type;
    unsigned char  mav_system_state;
    unsigned char mav_autopilot_type;
    unsigned char   mav_system_mode;
    unsigned int    mav_custom_mode;
    PROJDEFS_CONF_MAVLINK   mav_msg;
} PROJDEFS_PIPILOT_CFG;
/* Main data structure of autopilot stop */

/* Defines for IDLE resource counter start */

typedef struct
{
    unsigned long idle_cnt;
    unsigned int idle_period_cnt;
    unsigned char show_fl:1;
    unsigned long idle_value;
} PROJDEFS_IDLE_STAT;

/* Defines for IDLE resource counter stop  */

/* Init generic GPIOs */
void vProjdefsInitGPIO(void);

/* Init I2C1 sensors power */
void vProjdefsInitI2C1Power(void);

/* ReInit I2C1 power for sensors */
#define vProjdefsReInitI2C1Power   vProjdefsInitI2C1Power

/* Fini I2C1 sensors power */
void vProjdefsFiniI2C1Power(void);

/* Set blink at once to ARM LED*/
void vProjdefsARMblink(void);

/* Set short blink to ARM LED*/
void vProjdefsARMshortblink(void);

/* compute the 2's compliment of int value val */
short sProjdefs2Compire(short val, unsigned char bits);

/* Configure main data structure */
void vProjdefsConfigurePilot(volatile PROJDEFS_PIPILOT_CFG * cfg, volatile PROJDEFS_PIPILOT * target);

/* UART configure */
void vProjdefsConfigureUART(PROJDEFS_CONF_UART_TYPE port);
#endif /* PROJDEFS_H */
