#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/pipilot.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/pipilot.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../FreeRTOS/Source/portable/MemMang/heap_1.c ../FreeRTOS/Source/portable/MPLAB/PIC32MX/port.c ../FreeRTOS/Source/portable/MPLAB/PIC32MX/port_asm.S ../FreeRTOS/Source/croutine.c ../FreeRTOS/Source/list.c ../FreeRTOS/Source/queue.c ../FreeRTOS/Source/tasks.c ../FreeRTOS/Source/timers.c ../peripherial/source/hpwm-driver.c ../peripherial/source/i2c-driver.c ../peripherial/source/ppm-driver.c ../peripherial/source/pwm-driver.c ../peripherial/source/rf-link.c ../peripherial/source/spwm-driver.c ../peripherial/source/usb_descriptors.c ../peripherial/source/usb_device.c ../peripherial/source/usb_function_cdc.c ../peripherial/source/hpwm_isr.S ../peripherial/source/i2c1_isr.S ../peripherial/source/pinchange_isr.S ../peripherial/source/timer2_isr.S ../peripherial/source/timer4_isr.S ../peripherial/source/uart_rflink.S ../ConfigPerformance.c ../autopilot.c ../config.c ../console.c ../logd.c ../main.c ../pid.c ../projdefs.c ../range_meters.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/167578668/heap_1.o ${OBJECTDIR}/_ext/971107649/port.o ${OBJECTDIR}/_ext/971107649/port_asm.o ${OBJECTDIR}/_ext/381897321/croutine.o ${OBJECTDIR}/_ext/381897321/list.o ${OBJECTDIR}/_ext/381897321/queue.o ${OBJECTDIR}/_ext/381897321/tasks.o ${OBJECTDIR}/_ext/381897321/timers.o ${OBJECTDIR}/_ext/2034902730/hpwm-driver.o ${OBJECTDIR}/_ext/2034902730/i2c-driver.o ${OBJECTDIR}/_ext/2034902730/ppm-driver.o ${OBJECTDIR}/_ext/2034902730/pwm-driver.o ${OBJECTDIR}/_ext/2034902730/rf-link.o ${OBJECTDIR}/_ext/2034902730/spwm-driver.o ${OBJECTDIR}/_ext/2034902730/usb_descriptors.o ${OBJECTDIR}/_ext/2034902730/usb_device.o ${OBJECTDIR}/_ext/2034902730/usb_function_cdc.o ${OBJECTDIR}/_ext/2034902730/hpwm_isr.o ${OBJECTDIR}/_ext/2034902730/i2c1_isr.o ${OBJECTDIR}/_ext/2034902730/pinchange_isr.o ${OBJECTDIR}/_ext/2034902730/timer2_isr.o ${OBJECTDIR}/_ext/2034902730/timer4_isr.o ${OBJECTDIR}/_ext/2034902730/uart_rflink.o ${OBJECTDIR}/_ext/1472/ConfigPerformance.o ${OBJECTDIR}/_ext/1472/autopilot.o ${OBJECTDIR}/_ext/1472/config.o ${OBJECTDIR}/_ext/1472/console.o ${OBJECTDIR}/_ext/1472/logd.o ${OBJECTDIR}/_ext/1472/main.o ${OBJECTDIR}/_ext/1472/pid.o ${OBJECTDIR}/_ext/1472/projdefs.o ${OBJECTDIR}/_ext/1472/range_meters.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/167578668/heap_1.o.d ${OBJECTDIR}/_ext/971107649/port.o.d ${OBJECTDIR}/_ext/971107649/port_asm.o.d ${OBJECTDIR}/_ext/381897321/croutine.o.d ${OBJECTDIR}/_ext/381897321/list.o.d ${OBJECTDIR}/_ext/381897321/queue.o.d ${OBJECTDIR}/_ext/381897321/tasks.o.d ${OBJECTDIR}/_ext/381897321/timers.o.d ${OBJECTDIR}/_ext/2034902730/hpwm-driver.o.d ${OBJECTDIR}/_ext/2034902730/i2c-driver.o.d ${OBJECTDIR}/_ext/2034902730/ppm-driver.o.d ${OBJECTDIR}/_ext/2034902730/pwm-driver.o.d ${OBJECTDIR}/_ext/2034902730/rf-link.o.d ${OBJECTDIR}/_ext/2034902730/spwm-driver.o.d ${OBJECTDIR}/_ext/2034902730/usb_descriptors.o.d ${OBJECTDIR}/_ext/2034902730/usb_device.o.d ${OBJECTDIR}/_ext/2034902730/usb_function_cdc.o.d ${OBJECTDIR}/_ext/2034902730/hpwm_isr.o.d ${OBJECTDIR}/_ext/2034902730/i2c1_isr.o.d ${OBJECTDIR}/_ext/2034902730/pinchange_isr.o.d ${OBJECTDIR}/_ext/2034902730/timer2_isr.o.d ${OBJECTDIR}/_ext/2034902730/timer4_isr.o.d ${OBJECTDIR}/_ext/2034902730/uart_rflink.o.d ${OBJECTDIR}/_ext/1472/ConfigPerformance.o.d ${OBJECTDIR}/_ext/1472/autopilot.o.d ${OBJECTDIR}/_ext/1472/config.o.d ${OBJECTDIR}/_ext/1472/console.o.d ${OBJECTDIR}/_ext/1472/logd.o.d ${OBJECTDIR}/_ext/1472/main.o.d ${OBJECTDIR}/_ext/1472/pid.o.d ${OBJECTDIR}/_ext/1472/projdefs.o.d ${OBJECTDIR}/_ext/1472/range_meters.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/167578668/heap_1.o ${OBJECTDIR}/_ext/971107649/port.o ${OBJECTDIR}/_ext/971107649/port_asm.o ${OBJECTDIR}/_ext/381897321/croutine.o ${OBJECTDIR}/_ext/381897321/list.o ${OBJECTDIR}/_ext/381897321/queue.o ${OBJECTDIR}/_ext/381897321/tasks.o ${OBJECTDIR}/_ext/381897321/timers.o ${OBJECTDIR}/_ext/2034902730/hpwm-driver.o ${OBJECTDIR}/_ext/2034902730/i2c-driver.o ${OBJECTDIR}/_ext/2034902730/ppm-driver.o ${OBJECTDIR}/_ext/2034902730/pwm-driver.o ${OBJECTDIR}/_ext/2034902730/rf-link.o ${OBJECTDIR}/_ext/2034902730/spwm-driver.o ${OBJECTDIR}/_ext/2034902730/usb_descriptors.o ${OBJECTDIR}/_ext/2034902730/usb_device.o ${OBJECTDIR}/_ext/2034902730/usb_function_cdc.o ${OBJECTDIR}/_ext/2034902730/hpwm_isr.o ${OBJECTDIR}/_ext/2034902730/i2c1_isr.o ${OBJECTDIR}/_ext/2034902730/pinchange_isr.o ${OBJECTDIR}/_ext/2034902730/timer2_isr.o ${OBJECTDIR}/_ext/2034902730/timer4_isr.o ${OBJECTDIR}/_ext/2034902730/uart_rflink.o ${OBJECTDIR}/_ext/1472/ConfigPerformance.o ${OBJECTDIR}/_ext/1472/autopilot.o ${OBJECTDIR}/_ext/1472/config.o ${OBJECTDIR}/_ext/1472/console.o ${OBJECTDIR}/_ext/1472/logd.o ${OBJECTDIR}/_ext/1472/main.o ${OBJECTDIR}/_ext/1472/pid.o ${OBJECTDIR}/_ext/1472/projdefs.o ${OBJECTDIR}/_ext/1472/range_meters.o

# Source Files
SOURCEFILES=../FreeRTOS/Source/portable/MemMang/heap_1.c ../FreeRTOS/Source/portable/MPLAB/PIC32MX/port.c ../FreeRTOS/Source/portable/MPLAB/PIC32MX/port_asm.S ../FreeRTOS/Source/croutine.c ../FreeRTOS/Source/list.c ../FreeRTOS/Source/queue.c ../FreeRTOS/Source/tasks.c ../FreeRTOS/Source/timers.c ../peripherial/source/hpwm-driver.c ../peripherial/source/i2c-driver.c ../peripherial/source/ppm-driver.c ../peripherial/source/pwm-driver.c ../peripherial/source/rf-link.c ../peripherial/source/spwm-driver.c ../peripherial/source/usb_descriptors.c ../peripherial/source/usb_device.c ../peripherial/source/usb_function_cdc.c ../peripherial/source/hpwm_isr.S ../peripherial/source/i2c1_isr.S ../peripherial/source/pinchange_isr.S ../peripherial/source/timer2_isr.S ../peripherial/source/timer4_isr.S ../peripherial/source/uart_rflink.S ../ConfigPerformance.c ../autopilot.c ../config.c ../console.c ../logd.c ../main.c ../pid.c ../projdefs.c ../range_meters.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/pipilot.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX795F512L
MP_LINKER_FILE_OPTION=
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/971107649/port_asm.o: ../FreeRTOS/Source/portable/MPLAB/PIC32MX/port_asm.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/971107649 
	@${RM} ${OBJECTDIR}/_ext/971107649/port_asm.o.d 
	@${RM} ${OBJECTDIR}/_ext/971107649/port_asm.o 
	@${RM} ${OBJECTDIR}/_ext/971107649/port_asm.o.ok ${OBJECTDIR}/_ext/971107649/port_asm.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/971107649/port_asm.o.d" "${OBJECTDIR}/_ext/971107649/port_asm.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/971107649/port_asm.o.d"  -o ${OBJECTDIR}/_ext/971107649/port_asm.o ../FreeRTOS/Source/portable/MPLAB/PIC32MX/port_asm.S  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/971107649/port_asm.o.asm.d",--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1 -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include"
	
${OBJECTDIR}/_ext/2034902730/hpwm_isr.o: ../peripherial/source/hpwm_isr.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/hpwm_isr.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/hpwm_isr.o 
	@${RM} ${OBJECTDIR}/_ext/2034902730/hpwm_isr.o.ok ${OBJECTDIR}/_ext/2034902730/hpwm_isr.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/hpwm_isr.o.d" "${OBJECTDIR}/_ext/2034902730/hpwm_isr.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2034902730/hpwm_isr.o.d"  -o ${OBJECTDIR}/_ext/2034902730/hpwm_isr.o ../peripherial/source/hpwm_isr.S  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/2034902730/hpwm_isr.o.asm.d",--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1 -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include"
	
${OBJECTDIR}/_ext/2034902730/i2c1_isr.o: ../peripherial/source/i2c1_isr.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/i2c1_isr.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/i2c1_isr.o 
	@${RM} ${OBJECTDIR}/_ext/2034902730/i2c1_isr.o.ok ${OBJECTDIR}/_ext/2034902730/i2c1_isr.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/i2c1_isr.o.d" "${OBJECTDIR}/_ext/2034902730/i2c1_isr.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2034902730/i2c1_isr.o.d"  -o ${OBJECTDIR}/_ext/2034902730/i2c1_isr.o ../peripherial/source/i2c1_isr.S  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/2034902730/i2c1_isr.o.asm.d",--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1 -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include"
	
${OBJECTDIR}/_ext/2034902730/pinchange_isr.o: ../peripherial/source/pinchange_isr.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/pinchange_isr.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/pinchange_isr.o 
	@${RM} ${OBJECTDIR}/_ext/2034902730/pinchange_isr.o.ok ${OBJECTDIR}/_ext/2034902730/pinchange_isr.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/pinchange_isr.o.d" "${OBJECTDIR}/_ext/2034902730/pinchange_isr.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2034902730/pinchange_isr.o.d"  -o ${OBJECTDIR}/_ext/2034902730/pinchange_isr.o ../peripherial/source/pinchange_isr.S  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/2034902730/pinchange_isr.o.asm.d",--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1 -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include"
	
${OBJECTDIR}/_ext/2034902730/timer2_isr.o: ../peripherial/source/timer2_isr.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/timer2_isr.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/timer2_isr.o 
	@${RM} ${OBJECTDIR}/_ext/2034902730/timer2_isr.o.ok ${OBJECTDIR}/_ext/2034902730/timer2_isr.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/timer2_isr.o.d" "${OBJECTDIR}/_ext/2034902730/timer2_isr.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2034902730/timer2_isr.o.d"  -o ${OBJECTDIR}/_ext/2034902730/timer2_isr.o ../peripherial/source/timer2_isr.S  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/2034902730/timer2_isr.o.asm.d",--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1 -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include"
	
${OBJECTDIR}/_ext/2034902730/timer4_isr.o: ../peripherial/source/timer4_isr.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/timer4_isr.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/timer4_isr.o 
	@${RM} ${OBJECTDIR}/_ext/2034902730/timer4_isr.o.ok ${OBJECTDIR}/_ext/2034902730/timer4_isr.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/timer4_isr.o.d" "${OBJECTDIR}/_ext/2034902730/timer4_isr.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2034902730/timer4_isr.o.d"  -o ${OBJECTDIR}/_ext/2034902730/timer4_isr.o ../peripherial/source/timer4_isr.S  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/2034902730/timer4_isr.o.asm.d",--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1 -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include"
	
${OBJECTDIR}/_ext/2034902730/uart_rflink.o: ../peripherial/source/uart_rflink.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/uart_rflink.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/uart_rflink.o 
	@${RM} ${OBJECTDIR}/_ext/2034902730/uart_rflink.o.ok ${OBJECTDIR}/_ext/2034902730/uart_rflink.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/uart_rflink.o.d" "${OBJECTDIR}/_ext/2034902730/uart_rflink.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2034902730/uart_rflink.o.d"  -o ${OBJECTDIR}/_ext/2034902730/uart_rflink.o ../peripherial/source/uart_rflink.S  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/2034902730/uart_rflink.o.asm.d",--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1 -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include"
	
else
${OBJECTDIR}/_ext/971107649/port_asm.o: ../FreeRTOS/Source/portable/MPLAB/PIC32MX/port_asm.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/971107649 
	@${RM} ${OBJECTDIR}/_ext/971107649/port_asm.o.d 
	@${RM} ${OBJECTDIR}/_ext/971107649/port_asm.o 
	@${RM} ${OBJECTDIR}/_ext/971107649/port_asm.o.ok ${OBJECTDIR}/_ext/971107649/port_asm.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/971107649/port_asm.o.d" "${OBJECTDIR}/_ext/971107649/port_asm.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/971107649/port_asm.o.d"  -o ${OBJECTDIR}/_ext/971107649/port_asm.o ../FreeRTOS/Source/portable/MPLAB/PIC32MX/port_asm.S  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/971107649/port_asm.o.asm.d",--gdwarf-2 -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include"
	
${OBJECTDIR}/_ext/2034902730/hpwm_isr.o: ../peripherial/source/hpwm_isr.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/hpwm_isr.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/hpwm_isr.o 
	@${RM} ${OBJECTDIR}/_ext/2034902730/hpwm_isr.o.ok ${OBJECTDIR}/_ext/2034902730/hpwm_isr.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/hpwm_isr.o.d" "${OBJECTDIR}/_ext/2034902730/hpwm_isr.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2034902730/hpwm_isr.o.d"  -o ${OBJECTDIR}/_ext/2034902730/hpwm_isr.o ../peripherial/source/hpwm_isr.S  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/2034902730/hpwm_isr.o.asm.d",--gdwarf-2 -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include"
	
${OBJECTDIR}/_ext/2034902730/i2c1_isr.o: ../peripherial/source/i2c1_isr.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/i2c1_isr.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/i2c1_isr.o 
	@${RM} ${OBJECTDIR}/_ext/2034902730/i2c1_isr.o.ok ${OBJECTDIR}/_ext/2034902730/i2c1_isr.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/i2c1_isr.o.d" "${OBJECTDIR}/_ext/2034902730/i2c1_isr.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2034902730/i2c1_isr.o.d"  -o ${OBJECTDIR}/_ext/2034902730/i2c1_isr.o ../peripherial/source/i2c1_isr.S  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/2034902730/i2c1_isr.o.asm.d",--gdwarf-2 -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include"
	
${OBJECTDIR}/_ext/2034902730/pinchange_isr.o: ../peripherial/source/pinchange_isr.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/pinchange_isr.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/pinchange_isr.o 
	@${RM} ${OBJECTDIR}/_ext/2034902730/pinchange_isr.o.ok ${OBJECTDIR}/_ext/2034902730/pinchange_isr.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/pinchange_isr.o.d" "${OBJECTDIR}/_ext/2034902730/pinchange_isr.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2034902730/pinchange_isr.o.d"  -o ${OBJECTDIR}/_ext/2034902730/pinchange_isr.o ../peripherial/source/pinchange_isr.S  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/2034902730/pinchange_isr.o.asm.d",--gdwarf-2 -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include"
	
${OBJECTDIR}/_ext/2034902730/timer2_isr.o: ../peripherial/source/timer2_isr.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/timer2_isr.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/timer2_isr.o 
	@${RM} ${OBJECTDIR}/_ext/2034902730/timer2_isr.o.ok ${OBJECTDIR}/_ext/2034902730/timer2_isr.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/timer2_isr.o.d" "${OBJECTDIR}/_ext/2034902730/timer2_isr.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2034902730/timer2_isr.o.d"  -o ${OBJECTDIR}/_ext/2034902730/timer2_isr.o ../peripherial/source/timer2_isr.S  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/2034902730/timer2_isr.o.asm.d",--gdwarf-2 -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include"
	
${OBJECTDIR}/_ext/2034902730/timer4_isr.o: ../peripherial/source/timer4_isr.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/timer4_isr.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/timer4_isr.o 
	@${RM} ${OBJECTDIR}/_ext/2034902730/timer4_isr.o.ok ${OBJECTDIR}/_ext/2034902730/timer4_isr.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/timer4_isr.o.d" "${OBJECTDIR}/_ext/2034902730/timer4_isr.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2034902730/timer4_isr.o.d"  -o ${OBJECTDIR}/_ext/2034902730/timer4_isr.o ../peripherial/source/timer4_isr.S  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/2034902730/timer4_isr.o.asm.d",--gdwarf-2 -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include"
	
${OBJECTDIR}/_ext/2034902730/uart_rflink.o: ../peripherial/source/uart_rflink.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/uart_rflink.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/uart_rflink.o 
	@${RM} ${OBJECTDIR}/_ext/2034902730/uart_rflink.o.ok ${OBJECTDIR}/_ext/2034902730/uart_rflink.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/uart_rflink.o.d" "${OBJECTDIR}/_ext/2034902730/uart_rflink.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2034902730/uart_rflink.o.d"  -o ${OBJECTDIR}/_ext/2034902730/uart_rflink.o ../peripherial/source/uart_rflink.S  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/2034902730/uart_rflink.o.asm.d",--gdwarf-2 -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include"
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/167578668/heap_1.o: ../FreeRTOS/Source/portable/MemMang/heap_1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/167578668 
	@${RM} ${OBJECTDIR}/_ext/167578668/heap_1.o.d 
	@${RM} ${OBJECTDIR}/_ext/167578668/heap_1.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/167578668/heap_1.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/167578668/heap_1.o.d" -o ${OBJECTDIR}/_ext/167578668/heap_1.o ../FreeRTOS/Source/portable/MemMang/heap_1.c   
	
${OBJECTDIR}/_ext/971107649/port.o: ../FreeRTOS/Source/portable/MPLAB/PIC32MX/port.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/971107649 
	@${RM} ${OBJECTDIR}/_ext/971107649/port.o.d 
	@${RM} ${OBJECTDIR}/_ext/971107649/port.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/971107649/port.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/971107649/port.o.d" -o ${OBJECTDIR}/_ext/971107649/port.o ../FreeRTOS/Source/portable/MPLAB/PIC32MX/port.c   
	
${OBJECTDIR}/_ext/381897321/croutine.o: ../FreeRTOS/Source/croutine.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/381897321 
	@${RM} ${OBJECTDIR}/_ext/381897321/croutine.o.d 
	@${RM} ${OBJECTDIR}/_ext/381897321/croutine.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/381897321/croutine.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/381897321/croutine.o.d" -o ${OBJECTDIR}/_ext/381897321/croutine.o ../FreeRTOS/Source/croutine.c   
	
${OBJECTDIR}/_ext/381897321/list.o: ../FreeRTOS/Source/list.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/381897321 
	@${RM} ${OBJECTDIR}/_ext/381897321/list.o.d 
	@${RM} ${OBJECTDIR}/_ext/381897321/list.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/381897321/list.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/381897321/list.o.d" -o ${OBJECTDIR}/_ext/381897321/list.o ../FreeRTOS/Source/list.c   
	
${OBJECTDIR}/_ext/381897321/queue.o: ../FreeRTOS/Source/queue.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/381897321 
	@${RM} ${OBJECTDIR}/_ext/381897321/queue.o.d 
	@${RM} ${OBJECTDIR}/_ext/381897321/queue.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/381897321/queue.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/381897321/queue.o.d" -o ${OBJECTDIR}/_ext/381897321/queue.o ../FreeRTOS/Source/queue.c   
	
${OBJECTDIR}/_ext/381897321/tasks.o: ../FreeRTOS/Source/tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/381897321 
	@${RM} ${OBJECTDIR}/_ext/381897321/tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/381897321/tasks.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/381897321/tasks.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/381897321/tasks.o.d" -o ${OBJECTDIR}/_ext/381897321/tasks.o ../FreeRTOS/Source/tasks.c   
	
${OBJECTDIR}/_ext/381897321/timers.o: ../FreeRTOS/Source/timers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/381897321 
	@${RM} ${OBJECTDIR}/_ext/381897321/timers.o.d 
	@${RM} ${OBJECTDIR}/_ext/381897321/timers.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/381897321/timers.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/381897321/timers.o.d" -o ${OBJECTDIR}/_ext/381897321/timers.o ../FreeRTOS/Source/timers.c   
	
${OBJECTDIR}/_ext/2034902730/hpwm-driver.o: ../peripherial/source/hpwm-driver.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/hpwm-driver.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/hpwm-driver.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/hpwm-driver.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/2034902730/hpwm-driver.o.d" -o ${OBJECTDIR}/_ext/2034902730/hpwm-driver.o ../peripherial/source/hpwm-driver.c   
	
${OBJECTDIR}/_ext/2034902730/i2c-driver.o: ../peripherial/source/i2c-driver.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/i2c-driver.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/i2c-driver.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/i2c-driver.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/2034902730/i2c-driver.o.d" -o ${OBJECTDIR}/_ext/2034902730/i2c-driver.o ../peripherial/source/i2c-driver.c   
	
${OBJECTDIR}/_ext/2034902730/ppm-driver.o: ../peripherial/source/ppm-driver.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/ppm-driver.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/ppm-driver.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/ppm-driver.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/2034902730/ppm-driver.o.d" -o ${OBJECTDIR}/_ext/2034902730/ppm-driver.o ../peripherial/source/ppm-driver.c   
	
${OBJECTDIR}/_ext/2034902730/pwm-driver.o: ../peripherial/source/pwm-driver.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/pwm-driver.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/pwm-driver.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/pwm-driver.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/2034902730/pwm-driver.o.d" -o ${OBJECTDIR}/_ext/2034902730/pwm-driver.o ../peripherial/source/pwm-driver.c   
	
${OBJECTDIR}/_ext/2034902730/rf-link.o: ../peripherial/source/rf-link.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/rf-link.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/rf-link.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/rf-link.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/2034902730/rf-link.o.d" -o ${OBJECTDIR}/_ext/2034902730/rf-link.o ../peripherial/source/rf-link.c   
	
${OBJECTDIR}/_ext/2034902730/spwm-driver.o: ../peripherial/source/spwm-driver.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/spwm-driver.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/spwm-driver.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/spwm-driver.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/2034902730/spwm-driver.o.d" -o ${OBJECTDIR}/_ext/2034902730/spwm-driver.o ../peripherial/source/spwm-driver.c   
	
${OBJECTDIR}/_ext/2034902730/usb_descriptors.o: ../peripherial/source/usb_descriptors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/usb_descriptors.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/usb_descriptors.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/usb_descriptors.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/2034902730/usb_descriptors.o.d" -o ${OBJECTDIR}/_ext/2034902730/usb_descriptors.o ../peripherial/source/usb_descriptors.c   
	
${OBJECTDIR}/_ext/2034902730/usb_device.o: ../peripherial/source/usb_device.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/usb_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/usb_device.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/usb_device.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/2034902730/usb_device.o.d" -o ${OBJECTDIR}/_ext/2034902730/usb_device.o ../peripherial/source/usb_device.c   
	
${OBJECTDIR}/_ext/2034902730/usb_function_cdc.o: ../peripherial/source/usb_function_cdc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/usb_function_cdc.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/usb_function_cdc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/usb_function_cdc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/2034902730/usb_function_cdc.o.d" -o ${OBJECTDIR}/_ext/2034902730/usb_function_cdc.o ../peripherial/source/usb_function_cdc.c   
	
${OBJECTDIR}/_ext/1472/ConfigPerformance.o: ../ConfigPerformance.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/ConfigPerformance.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/ConfigPerformance.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/ConfigPerformance.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/1472/ConfigPerformance.o.d" -o ${OBJECTDIR}/_ext/1472/ConfigPerformance.o ../ConfigPerformance.c   
	
${OBJECTDIR}/_ext/1472/autopilot.o: ../autopilot.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/autopilot.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/autopilot.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/autopilot.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/1472/autopilot.o.d" -o ${OBJECTDIR}/_ext/1472/autopilot.o ../autopilot.c   
	
${OBJECTDIR}/_ext/1472/config.o: ../config.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/config.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/config.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/config.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/1472/config.o.d" -o ${OBJECTDIR}/_ext/1472/config.o ../config.c   
	
${OBJECTDIR}/_ext/1472/console.o: ../console.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/console.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/console.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/console.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/1472/console.o.d" -o ${OBJECTDIR}/_ext/1472/console.o ../console.c   
	
${OBJECTDIR}/_ext/1472/logd.o: ../logd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/logd.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/logd.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/logd.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/1472/logd.o.d" -o ${OBJECTDIR}/_ext/1472/logd.o ../logd.c   
	
${OBJECTDIR}/_ext/1472/main.o: ../main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/1472/main.o.d" -o ${OBJECTDIR}/_ext/1472/main.o ../main.c   
	
${OBJECTDIR}/_ext/1472/pid.o: ../pid.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/pid.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/pid.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/pid.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/1472/pid.o.d" -o ${OBJECTDIR}/_ext/1472/pid.o ../pid.c   
	
${OBJECTDIR}/_ext/1472/projdefs.o: ../projdefs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/projdefs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/projdefs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/projdefs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/1472/projdefs.o.d" -o ${OBJECTDIR}/_ext/1472/projdefs.o ../projdefs.c   
	
${OBJECTDIR}/_ext/1472/range_meters.o: ../range_meters.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/range_meters.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/range_meters.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/range_meters.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/1472/range_meters.o.d" -o ${OBJECTDIR}/_ext/1472/range_meters.o ../range_meters.c   
	
else
${OBJECTDIR}/_ext/167578668/heap_1.o: ../FreeRTOS/Source/portable/MemMang/heap_1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/167578668 
	@${RM} ${OBJECTDIR}/_ext/167578668/heap_1.o.d 
	@${RM} ${OBJECTDIR}/_ext/167578668/heap_1.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/167578668/heap_1.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/167578668/heap_1.o.d" -o ${OBJECTDIR}/_ext/167578668/heap_1.o ../FreeRTOS/Source/portable/MemMang/heap_1.c   
	
${OBJECTDIR}/_ext/971107649/port.o: ../FreeRTOS/Source/portable/MPLAB/PIC32MX/port.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/971107649 
	@${RM} ${OBJECTDIR}/_ext/971107649/port.o.d 
	@${RM} ${OBJECTDIR}/_ext/971107649/port.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/971107649/port.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/971107649/port.o.d" -o ${OBJECTDIR}/_ext/971107649/port.o ../FreeRTOS/Source/portable/MPLAB/PIC32MX/port.c   
	
${OBJECTDIR}/_ext/381897321/croutine.o: ../FreeRTOS/Source/croutine.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/381897321 
	@${RM} ${OBJECTDIR}/_ext/381897321/croutine.o.d 
	@${RM} ${OBJECTDIR}/_ext/381897321/croutine.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/381897321/croutine.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/381897321/croutine.o.d" -o ${OBJECTDIR}/_ext/381897321/croutine.o ../FreeRTOS/Source/croutine.c   
	
${OBJECTDIR}/_ext/381897321/list.o: ../FreeRTOS/Source/list.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/381897321 
	@${RM} ${OBJECTDIR}/_ext/381897321/list.o.d 
	@${RM} ${OBJECTDIR}/_ext/381897321/list.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/381897321/list.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/381897321/list.o.d" -o ${OBJECTDIR}/_ext/381897321/list.o ../FreeRTOS/Source/list.c   
	
${OBJECTDIR}/_ext/381897321/queue.o: ../FreeRTOS/Source/queue.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/381897321 
	@${RM} ${OBJECTDIR}/_ext/381897321/queue.o.d 
	@${RM} ${OBJECTDIR}/_ext/381897321/queue.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/381897321/queue.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/381897321/queue.o.d" -o ${OBJECTDIR}/_ext/381897321/queue.o ../FreeRTOS/Source/queue.c   
	
${OBJECTDIR}/_ext/381897321/tasks.o: ../FreeRTOS/Source/tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/381897321 
	@${RM} ${OBJECTDIR}/_ext/381897321/tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/381897321/tasks.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/381897321/tasks.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/381897321/tasks.o.d" -o ${OBJECTDIR}/_ext/381897321/tasks.o ../FreeRTOS/Source/tasks.c   
	
${OBJECTDIR}/_ext/381897321/timers.o: ../FreeRTOS/Source/timers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/381897321 
	@${RM} ${OBJECTDIR}/_ext/381897321/timers.o.d 
	@${RM} ${OBJECTDIR}/_ext/381897321/timers.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/381897321/timers.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/381897321/timers.o.d" -o ${OBJECTDIR}/_ext/381897321/timers.o ../FreeRTOS/Source/timers.c   
	
${OBJECTDIR}/_ext/2034902730/hpwm-driver.o: ../peripherial/source/hpwm-driver.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/hpwm-driver.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/hpwm-driver.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/hpwm-driver.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/2034902730/hpwm-driver.o.d" -o ${OBJECTDIR}/_ext/2034902730/hpwm-driver.o ../peripherial/source/hpwm-driver.c   
	
${OBJECTDIR}/_ext/2034902730/i2c-driver.o: ../peripherial/source/i2c-driver.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/i2c-driver.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/i2c-driver.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/i2c-driver.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/2034902730/i2c-driver.o.d" -o ${OBJECTDIR}/_ext/2034902730/i2c-driver.o ../peripherial/source/i2c-driver.c   
	
${OBJECTDIR}/_ext/2034902730/ppm-driver.o: ../peripherial/source/ppm-driver.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/ppm-driver.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/ppm-driver.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/ppm-driver.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/2034902730/ppm-driver.o.d" -o ${OBJECTDIR}/_ext/2034902730/ppm-driver.o ../peripherial/source/ppm-driver.c   
	
${OBJECTDIR}/_ext/2034902730/pwm-driver.o: ../peripherial/source/pwm-driver.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/pwm-driver.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/pwm-driver.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/pwm-driver.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/2034902730/pwm-driver.o.d" -o ${OBJECTDIR}/_ext/2034902730/pwm-driver.o ../peripherial/source/pwm-driver.c   
	
${OBJECTDIR}/_ext/2034902730/rf-link.o: ../peripherial/source/rf-link.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/rf-link.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/rf-link.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/rf-link.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/2034902730/rf-link.o.d" -o ${OBJECTDIR}/_ext/2034902730/rf-link.o ../peripherial/source/rf-link.c   
	
${OBJECTDIR}/_ext/2034902730/spwm-driver.o: ../peripherial/source/spwm-driver.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/spwm-driver.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/spwm-driver.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/spwm-driver.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/2034902730/spwm-driver.o.d" -o ${OBJECTDIR}/_ext/2034902730/spwm-driver.o ../peripherial/source/spwm-driver.c   
	
${OBJECTDIR}/_ext/2034902730/usb_descriptors.o: ../peripherial/source/usb_descriptors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/usb_descriptors.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/usb_descriptors.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/usb_descriptors.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/2034902730/usb_descriptors.o.d" -o ${OBJECTDIR}/_ext/2034902730/usb_descriptors.o ../peripherial/source/usb_descriptors.c   
	
${OBJECTDIR}/_ext/2034902730/usb_device.o: ../peripherial/source/usb_device.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/usb_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/usb_device.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/usb_device.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/2034902730/usb_device.o.d" -o ${OBJECTDIR}/_ext/2034902730/usb_device.o ../peripherial/source/usb_device.c   
	
${OBJECTDIR}/_ext/2034902730/usb_function_cdc.o: ../peripherial/source/usb_function_cdc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2034902730 
	@${RM} ${OBJECTDIR}/_ext/2034902730/usb_function_cdc.o.d 
	@${RM} ${OBJECTDIR}/_ext/2034902730/usb_function_cdc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2034902730/usb_function_cdc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/2034902730/usb_function_cdc.o.d" -o ${OBJECTDIR}/_ext/2034902730/usb_function_cdc.o ../peripherial/source/usb_function_cdc.c   
	
${OBJECTDIR}/_ext/1472/ConfigPerformance.o: ../ConfigPerformance.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/ConfigPerformance.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/ConfigPerformance.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/ConfigPerformance.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/1472/ConfigPerformance.o.d" -o ${OBJECTDIR}/_ext/1472/ConfigPerformance.o ../ConfigPerformance.c   
	
${OBJECTDIR}/_ext/1472/autopilot.o: ../autopilot.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/autopilot.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/autopilot.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/autopilot.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/1472/autopilot.o.d" -o ${OBJECTDIR}/_ext/1472/autopilot.o ../autopilot.c   
	
${OBJECTDIR}/_ext/1472/config.o: ../config.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/config.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/config.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/config.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/1472/config.o.d" -o ${OBJECTDIR}/_ext/1472/config.o ../config.c   
	
${OBJECTDIR}/_ext/1472/console.o: ../console.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/console.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/console.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/console.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/1472/console.o.d" -o ${OBJECTDIR}/_ext/1472/console.o ../console.c   
	
${OBJECTDIR}/_ext/1472/logd.o: ../logd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/logd.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/logd.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/logd.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/1472/logd.o.d" -o ${OBJECTDIR}/_ext/1472/logd.o ../logd.c   
	
${OBJECTDIR}/_ext/1472/main.o: ../main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/1472/main.o.d" -o ${OBJECTDIR}/_ext/1472/main.o ../main.c   
	
${OBJECTDIR}/_ext/1472/pid.o: ../pid.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/pid.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/pid.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/pid.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/1472/pid.o.d" -o ${OBJECTDIR}/_ext/1472/pid.o ../pid.c   
	
${OBJECTDIR}/_ext/1472/projdefs.o: ../projdefs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/projdefs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/projdefs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/projdefs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/1472/projdefs.o.d" -o ${OBJECTDIR}/_ext/1472/projdefs.o ../projdefs.c   
	
${OBJECTDIR}/_ext/1472/range_meters.o: ../range_meters.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/range_meters.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/range_meters.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/range_meters.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../mavlink/v1.0/common" -I"../peripherial/include" -I"../FreeRTOS/Source/include" -I"../FreeRTOS/Source/portable/MPLAB/PIC32MX" -I"../" -I"../peripherial/include" -MMD -MF "${OBJECTDIR}/_ext/1472/range_meters.o.d" -o ${OBJECTDIR}/_ext/1472/range_meters.o ../range_meters.c   
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/pipilot.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mdebugger -D__MPLAB_DEBUGGER_PK3=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/pipilot.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}           -mreserve=data@0x0:0x1FC -mreserve=boot@0x1FC02000:0x1FC02FEF -mreserve=boot@0x1FC02000:0x1FC024FF  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1,-L"../FreeRTOS/Source/portable/MPLAB/PIC32MX",-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map"
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/pipilot.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/pipilot.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),-L"../FreeRTOS/Source/portable/MPLAB/PIC32MX",-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map"
	${MP_CC_DIR}\\xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/pipilot.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
