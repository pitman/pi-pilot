/*
 * File:   spwm-driver.c
 *
 * Autopilot for UAV
 *
 * The driver for the software based PWM.
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include <xc.h>
#include <sys/attribs.h>

/* Hardware specific includes. */
#include "ConfigPerformance.h"
#include "peripheral/timer.h"
#include "peripheral/ports.h"

/* Project setting includes */
#include "projdefs.h"
#include "pwm-driver.h"

/* Main data structure of Pi-Pilot */
extern PROJDEFS_PIPILOT pilot;

void __attribute__( (interrupt(ipl0), vector(_TIMER_4_VECTOR), nomips16)) vTIMER4InterruptWrapper( void );

void vTIMER4InterruptHandler( void )
{
    static unsigned int calc_sleep_time = PWM_DRIVER_SPWM_PERIOD;
    static unsigned int temp;

    mT4ClearIntFlag();

    switch (pilot.pwm.spwm_state)
    {
    case SPWM_RST_ON:
        calc_sleep_time -= PWM_DRIVER_SPWM_RST;
        pilot.pwm.spwm_state = SPWM_RST_OFF;
        PWM_DRIVER_SPWM_MRST_OFF;
        WritePeriod4(PWM_DRIVER_SPWM_RST);
        break;
    case SPWM_RST_OFF:
        pilot.pwm.spwm_state = SPWM_CHAN_ON;
        pilot.pwm.spwm_chan_cnt = 0;
        calc_sleep_time -= PWM_DRIVER_SPWM_HALF_0_9;
        PWM_DRIVER_SPWM_CLK_1;
        WritePeriod4(PWM_DRIVER_SPWM_HALF_0_9);
        break;
    case SPWM_CHAN_ON:
        PWM_DRIVER_SPWM_CLK_0;
        pilot.pwm.spwm_state = SPWM_CHAN_OFF;
        temp = PWM_DRIVER_SPWM_HALF_0_9 + pilot.pwm.spwm[pilot.pwm.spwm_chan_cnt + 1];
        calc_sleep_time -= temp;
        WritePeriod4(temp);
        break;
    case SPWM_CHAN_OFF:
        if (pilot.pwm.spwm_chan_cnt >= (PWM_DRIVER_SPWM_CNT - 2))
        {
            pilot.pwm.spwm_state = SPWM_SLEEP;
            WritePeriod4(calc_sleep_time);
        }
        else
        {
            PWM_DRIVER_SPWM_CLK_1;
            pilot.pwm.spwm_state = SPWM_CHAN_ON;
            pilot.pwm.spwm_chan_cnt ++;
            calc_sleep_time -= PWM_DRIVER_SPWM_HALF_0_9;
            WritePeriod4(PWM_DRIVER_SPWM_HALF_0_9);
        }
        break;
    case SPWM_SLEEP:
        pilot.pwm.spwm_state = SPWM_RST_ON;
        PWM_DRIVER_SPWM_MRST_ON;
        temp = PWM_DRIVER_SPWM_RST + pilot.pwm.spwm[0];
        calc_sleep_time = PWM_DRIVER_SPWM_PERIOD - temp;
        WritePeriod4(temp);
        break;
    }
}
