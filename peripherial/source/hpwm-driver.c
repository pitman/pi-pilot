/*
 * File:   hpwm-driver.c
 *
 * Autopilot for UAV
 *
 * The Hardware PWM driver
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include <xc.h>
#include <sys/attribs.h>

/* Hardware specific includes. */
#include "ConfigPerformance.h"

/* Project setting includes */
#include "projdefs.h"
#include "pwm-driver.h"

/* Main data structure of Pi-Pilot */
extern PROJDEFS_PIPILOT pilot;

void __attribute__( (interrupt(ipl0), vector(_TIMER_3_VECTOR), nomips16)) vHPWMInterruptWrapper( void );

void vHPWMInterruptHandler( void )
{
    /* Clear interrupt */
    IFS0CLR = _IFS0_T3IF_MASK;
    /* Refresh PWM duty cicle */
    OC2RS = pilot.pwm.hpwm[0];
    OC3RS = pilot.pwm.hpwm[1];
    OC4RS = pilot.pwm.hpwm[2];
    OC5RS = pilot.pwm.hpwm[3];
}
