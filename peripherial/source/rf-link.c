#include <stdio.h>
#include <stdlib.h>
#include <p32xxxx.h>

/* Kernel includes. */
#include "FreeRTOS.h"

/* Project setting includes */
#include "projdefs.h"
#include "peripheral/uart.h"
#include "mavlink/v1.0/common/mavlink.h"
#include "rf-link.h"


/* Stop RF telemetry link */
void vRFLinkFini(void)
{
    INTEnable(RFLINK_INT_RX, INT_DISABLED);
    UARTEnable(RFLINK_UART, UART_DISABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));
}

#if 0
void __ISR(RFLINK_UART_VECTOR, IPL3) _RFLinkHandler(void)
{
    //mU1AClearAllIntFlags();
    	// Is this an RX interrupt?
	if(mU1RXGetIntFlag())
	{
		// Clear the RX interrupt Flag
	    mU1RXClearIntFlag();
		// Echo what we just received
		putcUART1(ReadUART1());
		// Toggle LED to indicate UART activity
		//mPORTDToggleBits(BIT_0);
	}
	// We don't care about TX interrupt
	if ( mU1TXGetIntFlag() )
	{
		mU1TXClearIntFlag();
	}
}
#endif

int iRFLinkPutc(unsigned char data)
{
    if (UARTTransmitterIsReady(RFLINK_UART))
    {
        UARTSendDataByte(RFLINK_UART, data);
        return pdTRUE;
    }

    return pdFALSE;
}

int iRFLinkGetc(unsigned char * data)
{
    if (UARTReceivedDataIsAvailable(RFLINK_UART))
    {
        *data = UARTGetDataByte(RFLINK_UART);
        return pdTRUE;
    }

    return pdFALSE;
}

int iRFLinkGetLine(unsigned char * buff)
{
    unsigned char rdata;
    unsigned int  rdata_cnt = 0;

    do {
        if (!iRFLinkGetc(&rdata))
            vTaskDelay(1/portTICK_RATE_MS);
        else
            *(buff++) = rdata;
            rdata_cnt++;
    } while (rdata != 0x13);

    rdata_cnt--;

    return rdata_cnt;
}

/* Configure RF-link function */
void vRFLinkConfigure(void)
{
    unsigned char buff[21];

    UARTSetDataRate(RFLINK_UART, GetPeripheralClock(), RFLINK_BAUD);
    /*
     * Wait 1ms
     */
    vTaskDelay(1/portTICK_RATE_MS);
    /*
     * Read config data
     */
    iRFLinkPutc('R');
    iRFLinkPutc('D');
    iRFLinkPutc(0x13);
    iRFLinkPutc(0x10);
}

