/*
 * File:   i2c1_isr.S
 *
 * Autopilot for UAV
 *
 * The Interrupt Service Routine wrapper for the I2C1 bus.
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include <xc.h>
#include <sys/asm.h>
#include "ISR_Support.h"

	.set	nomips16
	.set 	noreorder

	.extern vI2C1InterruptHandler
	.extern xISRStackTop
	.global	vI2C1InterruptWrapper

	.set	noreorder
	.set 	noat
	.ent	vI2C1InterruptWrapper

vI2C1InterruptWrapper:

	portSAVE_CONTEXT
	jal vI2C1InterruptHandler
	nop
	portRESTORE_CONTEXT

	.end	vI2C1InterruptWrapper
