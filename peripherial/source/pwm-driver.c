/*
 * File:   pwm-driver.c
 *
 * Autopilot for UAV
 *
 * The main file for control any type of PWM.
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include <xc.h>
#include <p32xxxx.h>
#include <sys/attribs.h>
#include <peripheral/outcompare.h>
#include <peripheral/timer.h>

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "croutine.h"
#include "timers.h"
#include "queue.h"

/* Hardware specific includes. */
#include "ConfigPerformance.h"

/* Project setting includes */
#include "projdefs.h"
#include "pwm-driver.h"

/* Main data structure of Pi-Pilot */
extern PROJDEFS_PIPILOT pilot;

/* Init value of PWM before start */
void vPWMDRIVER_PWM_UPDATE(void)
{
    unsigned int k;

    /* Set default value for all PWMs */
    for (k = 0; k < PWM_DRIVER_HPWM_CNT; k++)
    {
        switch(pilot.pwm.hpwm_type[k])
        {
            case TYPE_0_100:
                pilot.pwm.hpwm[k] = PWM_DRIVER_HPWM_MIN_VAL;
                pilot.pwm.hpwm_null_point[k] = PWM_DRIVER_HPWM_MIN_VAL;
                break;
            case TYPE_50:
                pilot.pwm.hpwm[k] = PWM_DRIVER_HPWM_MID_VAL;
                pilot.pwm.hpwm_null_point[k] = PWM_DRIVER_HPWM_MID_VAL;
                break;
            case TYPE_100_0:
                pilot.pwm.hpwm[k] = PWM_DRIVER_HPWM_MAX_VAL;
                pilot.pwm.hpwm_null_point[k] = PWM_DRIVER_HPWM_MAX_VAL;
                break;
            case TYPE_USER:
                pilot.pwm.hpwm[k] = pilot.pwm.hpwm_null_point[k];
                break;
        }
    }
    for (k = 0; k < PWM_DRIVER_SPWM_CNT; k++)
    {
        switch(pilot.pwm.spwm_type[k])
        {
            case TYPE_0_100:
                pilot.pwm.spwm[k] = PWM_DRIVER_SPWM_MIN_VAL;
                pilot.pwm.spwm_null_point[k] = PWM_DRIVER_SPWM_MIN_VAL;
                break;
            case TYPE_50:
                pilot.pwm.spwm[k] = PWM_DRIVER_SPWM_MID_VAL;
                pilot.pwm.spwm_null_point[k] = PWM_DRIVER_SPWM_MID_VAL;
                break;
            case TYPE_100_0:
                pilot.pwm.spwm[k] = PWM_DRIVER_SPWM_MAX_VAL;
                pilot.pwm.spwm_null_point[k] = PWM_DRIVER_SPWM_MAX_VAL;
                break;
            case TYPE_USER:
                pilot.pwm.spwm[k] = pilot.pwm.spwm_null_point[k];
                break;
        }
    }
}

/* Set default state for SPWM engine */
void vPWMDRIVER_SPWM_DEFAULT(void)
{
    PWM_DRIVER_SPWM_MRST_OFF;
    PWM_DRIVER_SPWM_CLK_0;
    pilot.pwm.spwm_state = SPWM_RST_ON;
    pilot.pwm.spwm_chan_cnt = 0;
}

/* Init hardware and software PWMs */
void vPWMDRIVER_INIT_PWM(void)
{
    /* Configure OCx for PWM */
    /* PWM  output 1 */
    CloseOC2();
    OpenOC2( OC_OFF | OC_TIMER3_SRC | OC_PWM_FAULT_PIN_DISABLE, 0, 0);
    /* PWM  output 2 */
    CloseOC3();
    OpenOC3( OC_OFF | OC_TIMER3_SRC | OC_PWM_FAULT_PIN_DISABLE, 0, 0);
    /* PWM  output 3 */
    CloseOC4();
    OpenOC4( OC_OFF | OC_TIMER3_SRC | OC_PWM_FAULT_PIN_DISABLE, 0, 0);
    /* PWM  output 4 */
    CloseOC5();
    OpenOC5( OC_OFF | OC_TIMER3_SRC | OC_PWM_FAULT_PIN_DISABLE, 0, 0);
    /* Configure timer source */
    CloseTimer3();
    OpenTimer3( T3_OFF | T3_PS_1_32 | T3_SOURCE_INT, PWM_DRIVER_HPWM_PERIOD );
    ConfigIntTimer3( T3_INT_OFF | T3_INT_PRIOR_4 | T3_INT_SUB_PRIOR_2 );
    /* Configure Software PWM */
    /* GPIO */
    PWM_DRIVER_SPWM_MRST_OUT;
    PWM_DRIVER_SPWM_MRST_ODC;
    PWM_DRIVER_SPWM_CLK_OUT;
    vPWMDRIVER_SPWM_DEFAULT();
    /* Configure Timer4 */
    CloseTimer4();
    /* 40MHz/32 */
    OpenTimer4( T4_OFF | T4_PS_1_32 | T4_SOURCE_INT |  T4_32BIT_MODE_OFF,
            PWM_DRIVER_SPWM_RST );
    ConfigIntTimer4( T4_INT_OFF | T4_INT_PRIOR_4 | T4_INT_SUB_PRIOR_3 );
}

/* Start Hardware and Software PWMs */
void vPWMDRIVER_START_PWM(void)
{
    /* Init default value of PWM before start */
    vPWMDRIVER_PWM_UPDATE();

    /* Start Hardware PWM */
    /* PWM  output 1 */
    CloseOC2();
    if (pilot.pwm.hpwm_type[0] != TYPE_NONE)
        OpenOC2( OC_ON | OC_TIMER3_SRC | OC_PWM_FAULT_PIN_DISABLE, 0, 0);
    /* PWM  output 2 */
    CloseOC3();
    if (pilot.pwm.hpwm_type[1] != TYPE_NONE)
        OpenOC3( OC_ON | OC_TIMER3_SRC | OC_PWM_FAULT_PIN_DISABLE, 0, 0);
    /* PWM  output 3 */
    CloseOC4();
    if (pilot.pwm.hpwm_type[2] != TYPE_NONE)
        OpenOC4( OC_ON | OC_TIMER3_SRC | OC_PWM_FAULT_PIN_DISABLE, 0, 0);
    /* PWM  output 4 */
    CloseOC5();
    if (pilot.pwm.hpwm_type[3] != TYPE_NONE)
        OpenOC5( OC_ON | OC_TIMER3_SRC | OC_PWM_FAULT_PIN_DISABLE, 0, 0);
    /* Set default state SPWM engine */
    vPWMDRIVER_SPWM_DEFAULT();
    /* Configure timer source */
    CloseTimer3();
    CloseTimer4();
    INTDisableInterrupts();
    OpenTimer3( T3_ON | T3_PS_1_32 | T3_SOURCE_INT, PWM_DRIVER_HPWM_PERIOD );
    ConfigIntTimer3( T3_INT_ON | T3_INT_PRIOR_4 | T3_INT_SUB_PRIOR_2 );
    /* Start Software PWM */
    OpenTimer4( T4_ON | T4_PS_1_32 | T4_SOURCE_INT |  T4_32BIT_MODE_OFF,
            PWM_DRIVER_SPWM_RST );
    ConfigIntTimer4( T4_INT_ON | T4_INT_PRIOR_4 | T4_INT_SUB_PRIOR_3 );
    INTEnableInterrupts();
}

/* Stop Hardware and Software PWMs */
void vPWMDRIVER_STOP_PWM(void)
{
    /* Stop HARDWARE PWMs */
    /* PWM  output 1 */
    CloseOC2();
    /* PWM  output 2 */
    CloseOC3();
    /* PWM  output 3 */
    CloseOC4();
    /* PWM  output 4 */
    CloseOC5();
    /* Configure timer source */
    CloseTimer3();
    CloseTimer4();
    INTDisableInterrupts();
    ConfigIntTimer3( T3_INT_OFF | T3_INT_PRIOR_4 | T3_INT_SUB_PRIOR_2);
    /* Stop Software PWM */
    ConfigIntTimer4( T4_INT_OFF | T4_INT_PRIOR_4 | T4_INT_SUB_PRIOR_3 );
    INTEnableInterrupts();
    /* Set default state SPWM engine */
    vPWMDRIVER_SPWM_DEFAULT();
}
