/*
 * File:   i2c-driver.c
 *
 * Autopilot for UAV
 *
 * The driver for the I2C bus.
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <p32xxxx.h>
#include <xc.h>
#include <sys/attribs.h>

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "croutine.h"
#include "timers.h"
#include "queue.h"

/* Hardware specific includes. */
#include "ConfigPerformance.h"
#include "i2c.h"

/* Project setting includes */
#include "projdefs.h"
#include "i2c-driver.h"
#include "lsm303dlhc.h"
#include "l3g4200d.h"
#include "bmp085.h"
#include "sens.h"

/* Queue for data from an I2C1 sensors */
extern xQueueHandle xQueueI2C1Sensors;
/* Queue for result of read/write data to I2C */
extern xQueueHandle xQueueI2CRWResult;
/* Queue for read/write data to I2C */
extern xQueueHandle xQueueI2CRWPKT;
/* Local time with 1ms resolution */
extern unsigned long ulLOCALTIME;
/* Main data structure of Pi-Pilot */
extern PROJDEFS_PIPILOT pilot;

#ifdef DEBUG_I2C_BUFFER_ENABLE
/* Debug I2C bus data structure */
extern I2C_DRIVER_DEBUG i2c_dbg[I2C_DRIVER_DEBUG_DATA_LENGHT];
extern unsigned int i2c_dbg_cnt;
#endif

void __attribute__( (interrupt(ipl0), vector(_I2C_1_VECTOR), nomips16)) vI2C1InterruptWrapper( void );

void vI2C1InterruptHandler( void )
{
    static __I2C1STATbits_t I2C1state;
    static portBASE_TYPE xHigherPriorityTaskWoken;
    static I2C_DRIVER_RW_PKT    i2c_op;
    static unsigned char l_data_cnt, pkt_l_data;
    static portBASE_TYPE prvStreamPktFlag;
    static portBASE_TYPE prvQueueSendResult;
    static SENS_I2C_QUEUE_STRUCT i2c_sens;
    static I2C_DRIVER_RW_RESULT i2c_op_res;
    static I2C_DRIVER_SENSORS sensors[I2C_DRIVER_IRQ_TABLE] = {
        /* OP_TYPE, I2C ADDR, REG, number of bytes, flag use template, template */
        /* Check ACC data ready for read */
        {LSM303DLHC_ACC_READY, LSM303DLHC_ACC_I2C_ADDR, LSM303DLHC_ACC_STAT_REG_A, 1, 1, LSM303DLHC_ACC_DRDY_T},
        /* Read ACC data */
        {LSM303DLHC_ACC_DATA, LSM303DLHC_ACC_I2C_ADDR, (LSM303DLHC_ACC_OUT_X_L | LSM303DLHC_ACC_MULTI_READ), 6, 0, 0},
        /* Check MAG data ready for read */
        {LSM303DLHC_MAG_READY, LSM303DLHC_MAG_I2C_ADDR, LSM303DLHC_MAG_SR_REG_M, 1, 1, LSM303DLHC_MAG_DRDY_T},
        /* Read MAG data */
        {LSM303DLHC_MAG_DATA, LSM303DLHC_MAG_I2C_ADDR, LSM303DLHC_MAG_OUT_X_H, 6, 0, 0},
        /* Check HYR data ready for read */
        {L3G4200D_HYR_READY, L3G4200D_HYR_I2C_ADDR, L3G4200D_HYR_STATUS_REG, 1, 1, L3G4200D_HYR_DRDY_T},
        /* Read HYR data */
        {L3G4200D_HYR_DATA, L3G4200D_HYR_I2C_ADDR, (L3G4200D_HYR_OUT_X_L | L3G4200D_HYR_MULTI_READ), 6, 0, 0},
    };
    static unsigned char sensors_cnt = 0;

    /* Clear interrupt */
    INTClearFlag(INT_I2C1);

#ifdef DEBUG_I2C_BUFFER_ENABLE
    /* Debug I2C, write state to a array */
    i2c_dbg[i2c_dbg_cnt].pwr = pilot.i2c1.pwr;
    i2c_dbg[i2c_dbg_cnt].state = pilot.i2c1.state;
    i2c_dbg[i2c_dbg_cnt].pkt = pilot.i2c1.pkt;
    i2c_dbg[i2c_dbg_cnt].data_flow = pilot.i2c1.data_flow;
    i2c_dbg[i2c_dbg_cnt].irq_cnt = pilot.i2c1.irq_cnt;
    i2c_dbg[i2c_dbg_cnt].i2c_state = I2C1state;
    i2c_dbg_cnt++;
    if (i2c_dbg_cnt >= I2C_DRIVER_DEBUG_DATA_LENGHT)
    {
        i2c_dbg_cnt = 0;
    }
#endif

    /* We want woken a task at the start of the ISR. */
    xHigherPriorityTaskWoken = pdFALSE;

    /* Increment counter of IRQ */
    pilot.i2c1.irq_cnt++;

    /* Read I2C1 state */
    I2C1state = I2C1STATbits;

    /* Check I2C1 BUS COLLISION */
    if (I2C1state.BCL)
    {
        /* Stop read data from a mems sensors */
        prvStreamPktFlag = 0;
        sensors_cnt = 0;
        pilot.i2c1.data_flow = I2C_DF_STOP;
        pilot.i2c1.state = I2C_BUS_COLLISION;
        /* Send message for reinit the I2C1 bus */
        i2c_op_res.uid = ulLOCALTIME;
        i2c_op_res.result = I2C_OP_COLLISION;
        prvQueueSendResult = xQueueSendFromISR(xQueueI2CRWResult, &i2c_op_res,
                &xHigherPriorityTaskWoken);
        if (prvQueueSendResult == errQUEUE_FULL)
        {
            pilot.i2c1.data_overflow_cnt++;
        }
    }
    /* Check I2C1 STOP condition */
    else if (I2C1state.P)
    {
        pilot.i2c1.pkt = I2C_PKT_NONE;
        prvStreamPktFlag = 0;
       
        /* Check command queue for new command or enable data stream from a sensor */
        if ((uxQueueMessagesWaitingFromISR(xQueueI2CRWPKT) != 0) ||
                (pilot.i2c1.data_flow == I2C_DF_WORK))
        {
            pilot.i2c1.state = I2C_BUS_START;
            I2C1CONbits.SEN = 1;
        }
        else
        {
            pilot.i2c1.state = I2C_BUS_IDLE;
            sensors_cnt = 0;
        }
    }
    /* Check START/RESTART state */
    else if (I2C1state.S)
    {
        switch (pilot.i2c1.pkt)
        {
        case I2C_PKT_NONE:
            i2c_op_res.result = I2C_OP_FAIL;
            /* Check command queue for new command */
            if (uxQueueMessagesWaitingFromISR(xQueueI2CRWPKT) != 0)
            {
                /* Start exec new cmd from queue */
                prvStreamPktFlag = 0;
                xQueueReceiveFromISR(xQueueI2CRWPKT, &i2c_op, &xHigherPriorityTaskWoken);
                i2c_op_res.uid = i2c_op.uid;
                /* Buffer overflow protection  */
                if (i2c_op.data_cnt > I2C_DRIVER_MAX_DATA_LENGHT)
                {
                    i2c_op.data_cnt = I2C_DRIVER_MAX_DATA_LENGHT;
                }
                pkt_l_data = i2c_op.data_cnt;
                pilot.i2c1.state = I2C_BUS_SEND_DATA;
                pilot.i2c1.pkt = I2C_PKT_WRITE_ADDR;
                I2C1TRN = (i2c_op.addr & I2C_DRIVER_MASK_WRITE);
            }
            else
            {
                if (pilot.i2c1.data_flow == I2C_DF_WORK)
                {
                    /* TODO: code for dataflow here */
                    if (!sensors[sensors_cnt].template_fl)
                    {
                        i2c_sens.time = ulLOCALTIME;
                        switch (sensors[sensors_cnt].op_type)
                        {
                        case LSM303DLHC_ACC_DATA:
                            i2c_sens.sens_type = SENS_LSM303DLHC_ACC;
                            break;
                        case LSM303DLHC_MAG_DATA:
                            i2c_sens.sens_type = SENS_LSM303DLHC_MAG;
                            break;
                        case L3G4200D_HYR_DATA:
                            i2c_sens.sens_type = SENS_L3G4200D_HYR;
                            break;
                        }
                    }
                    prvStreamPktFlag = 1;
                    if (sensors[sensors_cnt].data_lenght > I2C_DRIVER_MAX_DATA_LENGHT)
                    {
                        sensors[sensors_cnt].data_lenght = I2C_DRIVER_MAX_DATA_LENGHT;
                    }
                    pkt_l_data = sensors[sensors_cnt].data_lenght;
                    
                    pilot.i2c1.state = I2C_BUS_SEND_DATA;
                    pilot.i2c1.pkt = I2C_PKT_WRITE_ADDR;
                    I2C1TRN = (sensors[sensors_cnt].addr & I2C_DRIVER_MASK_WRITE);
                }
                else
                {
                    /* Unknown START found here, STOP any I2C transaction */
                    pilot.i2c1.state = I2C_BUS_STOP;
                    pilot.i2c1.pkt = I2C_PKT_NONE;
                    prvStreamPktFlag = 0;
                    I2C1CONbits.PEN = 1;
                 }
            }
            break;
        case I2C_PKT_WRITE_ADDR:
            if (pilot.i2c1.state == I2C_BUS_SEND_DATA)
            {
                /* ACK */
                if (!I2C1state.ACKSTAT)
                {
                    pilot.i2c1.state = I2C_BUS_SEND_DATA;
                    pilot.i2c1.pkt = I2C_PKT_WRITE_REG;

                    if (prvStreamPktFlag)
                    {
                        I2C1TRN = sensors[sensors_cnt].reg;
                    }
                    else
                    {
                        I2C1TRN = i2c_op.reg;
                    }
                }
                /* NACK */
                else
                {
                    if (prvStreamPktFlag)
                    {
                        /* MEMS sensor didn't answer */
                        /* TODO: error workaround here */
                    }
                    else
                    {
                        i2c_op_res.result = I2C_OP_SLAVE_NOT_FOUND;
                        i2c_op_res.data_cnt = 0;
                        prvQueueSendResult = xQueueSendFromISR(xQueueI2CRWResult,
                            &i2c_op_res, &xHigherPriorityTaskWoken);
                        if (prvQueueSendResult == errQUEUE_FULL)
                        {
                            pilot.i2c1.data_overflow_cnt++;
                        }
                        pilot.i2c1.state = I2C_BUS_STOP;
                        I2C1CONbits.PEN = 1;
                    }
                }
            }
            break;
        case I2C_PKT_WRITE_REG:
            /* ACK */
            if (!I2C1state.ACKSTAT)
            {
                if (prvStreamPktFlag)
                {
                    pilot.i2c1.state = I2C_BUS_RESTART;
                    pilot.i2c1.pkt = I2C_PKT_RESTART;
                    I2C1CONbits.SEN = 1;
                }
                else
                {
                    if (i2c_op.rw_fl == 1)
                    {
                        l_data_cnt = 0;
                        pilot.i2c1.state = I2C_BUS_SEND_DATA;
                        pilot.i2c1.pkt = I2C_PKT_WRITE_DATA;
                        I2C1TRN = i2c_op.data[l_data_cnt];
                    }
                    else
                    {
                        pilot.i2c1.state = I2C_BUS_RESTART;
                        pilot.i2c1.pkt = I2C_PKT_RESTART;
                        I2C1CONbits.SEN = 1;
                    }
                }
            }
            /* NACK */
            else
            {
                if (prvStreamPktFlag)
                {
                    /* MEMS sensor didn't answer */
                    /* TODO: error workaround here */
                }
                else
                {
                    i2c_op_res.result = I2C_OP_SLAVE_REG_ABORT;
                    i2c_op_res.data_cnt = 0;
                    prvQueueSendResult = xQueueSendFromISR(xQueueI2CRWResult,
                        &i2c_op_res, &xHigherPriorityTaskWoken);
                    if (prvQueueSendResult == errQUEUE_FULL)
                    {
                        pilot.i2c1.data_overflow_cnt++;
                    }
                    pilot.i2c1.state = I2C_BUS_STOP;
                    I2C1CONbits.PEN = 1;
                }
            }
            break;
        case I2C_PKT_WRITE_DATA:
            /* ACK */
            if (!I2C1state.ACKSTAT)
            {
                l_data_cnt++;
                if (l_data_cnt >= pkt_l_data)
                {
                    i2c_op_res.result = I2C_OP_WRITE_OK;
                    i2c_op_res.data_cnt = l_data_cnt;
                    prvQueueSendResult = xQueueSendFromISR(xQueueI2CRWResult,
                            &i2c_op_res, &xHigherPriorityTaskWoken);
                    if (prvQueueSendResult == errQUEUE_FULL)
                    {
                        pilot.i2c1.data_overflow_cnt++;
                    }
                    pilot.i2c1.state = I2C_BUS_STOP;
                    I2C1CONbits.PEN = 1;
                }
                else
                {
                    pilot.i2c1.state = I2C_BUS_SEND_DATA;
                    I2C1TRN = i2c_op.data[l_data_cnt];
                }
            }
            /* NACK */
            else
            {
                i2c_op_res.result = I2C_OP_SLAVE_WRITE_DATA_ABORT;
                i2c_op_res.data_cnt = 0;
                prvQueueSendResult = xQueueSendFromISR(xQueueI2CRWResult,
                        &i2c_op_res, &xHigherPriorityTaskWoken);
                if (prvQueueSendResult == errQUEUE_FULL)
                {
                    pilot.i2c1.data_overflow_cnt++;
                }
                pilot.i2c1.state = I2C_BUS_STOP;
                I2C1CONbits.PEN = 1;
            }
            break;
        case I2C_PKT_RESTART:
            pilot.i2c1.state = I2C_BUS_RECV_DATA;
            pilot.i2c1.pkt = I2C_PKT_READ_ADDR;
            if (prvStreamPktFlag)
            {
                I2C1TRN = (sensors[sensors_cnt].addr | I2C_DRIVER_MASK_READ);
            }
            else
            {
                I2C1TRN = (i2c_op.addr | I2C_DRIVER_MASK_READ);
            }
            break;
        case I2C_PKT_READ_ADDR:
            /* ACK */
            if (!I2C1state.ACKSTAT)
            {
                /* Start read data from a device */
                l_data_cnt = 0;
                pilot.i2c1.state = I2C_BUS_RECV_DATA;
                pilot.i2c1.pkt = I2C_PKT_READ_DATA;
                I2C1CONbits.RCEN = 1;
            }
            /* NACK */
            else
            {
                if (prvStreamPktFlag)
                {
                    /* MEMS sensor didn't answer */
                    /* TODO: error workaround here */
                }
                else
                {
                    i2c_op_res.result = I2C_OP_SLAVE_READ_DATA_ABORT;
                    i2c_op_res.data_cnt = 0;
                    prvQueueSendResult = xQueueSendFromISR(xQueueI2CRWResult,
                        &i2c_op_res, &xHigherPriorityTaskWoken);
                    if (prvQueueSendResult == errQUEUE_FULL)
                    {
                        pilot.i2c1.data_overflow_cnt++;
                    }
                    pilot.i2c1.state = I2C_BUS_STOP;
                    I2C1CONbits.PEN = 1;
                }
            }
            break;
        case I2C_PKT_READ_DATA:
            /* Receive data byte */
            if (I2C1state.RBF)
            {
                if (prvStreamPktFlag)
                {
                    i2c_sens.data[l_data_cnt] = I2C1RCV;
                }
                else
                {
                    i2c_op_res.data[l_data_cnt] = I2C1RCV;
                }

                l_data_cnt++;

                if (l_data_cnt >= pkt_l_data)
                {
                    /* Send NACK */
                    pilot.i2c1.state = I2C_BUS_NACK;
                    I2C1CONbits.ACKDT = 1;
                    I2C1CONbits.ACKEN = 1;
                }
                else
                {
                    /* Send ACK */
                    pilot.i2c1.state = I2C_BUS_ACK;
                    I2C1CONbits.ACKDT = 0;
                    I2C1CONbits.ACKEN = 1;
                 }
            }
            /* Receive ACK/NACK */
            else
            {
                if (pilot.i2c1.state == I2C_BUS_ACK)
                {
                    pilot.i2c1.state = I2C_BUS_RECV_DATA;
                    I2C1CONbits.RCEN = 1;
                }
                if (pilot.i2c1.state == I2C_BUS_NACK)
                {
                    if (prvStreamPktFlag)
                    {
                        if (sensors[sensors_cnt].template_fl)
                        {
                            /* Test data ready template */
                            if (sensors[sensors_cnt].template_drdy & i2c_sens.data[0])
                            {
                                sensors_cnt++;
                            }
                            else
                            {
                                sensors_cnt+=2;
                            }
                        }
                        else
                        {
                            i2c_sens.data_cnt = l_data_cnt;
                            xQueueSendFromISR(xQueueI2C1Sensors,
                                  &i2c_sens, &xHigherPriorityTaskWoken);
                            sensors_cnt++;
                        }
                        if (sensors_cnt >= I2C_DRIVER_IRQ_TABLE)
                        {
                            sensors_cnt = 0;
                        }
                    }
                    else
                    {
                        i2c_op_res.result = I2C_OP_READ_OK;
                        i2c_op_res.data_cnt = l_data_cnt;
                        prvQueueSendResult = xQueueSendFromISR(xQueueI2CRWResult,
                            &i2c_op_res, &xHigherPriorityTaskWoken);
                        if (prvQueueSendResult == errQUEUE_FULL)
                        {
                            pilot.i2c1.data_overflow_cnt++;
                        }
                    }
                    pilot.i2c1.state = I2C_BUS_STOP;
                    I2C1CONbits.PEN = 1;
                }
            }
            break;
        }
    }

#ifdef DEBUG_I2C_BUFFER_ENABLE
    /* Debug I2C, write state to a array */
    i2c_dbg[i2c_dbg_cnt].pwr = pilot.i2c1.pwr;
    i2c_dbg[i2c_dbg_cnt].state = pilot.i2c1.state;
    i2c_dbg[i2c_dbg_cnt].pkt = pilot.i2c1.pkt;
    i2c_dbg[i2c_dbg_cnt].data_flow = pilot.i2c1.data_flow;
    i2c_dbg[i2c_dbg_cnt].irq_cnt = pilot.i2c1.irq_cnt;
    i2c_dbg[i2c_dbg_cnt].i2c_state = I2C1state;
    i2c_dbg_cnt++;
    if (i2c_dbg_cnt >= I2C_DRIVER_DEBUG_DATA_LENGHT)
    {
       i2c_dbg_cnt = 0;
    }
#endif

    /* If sending or receiving necessitates a context switch, then switch now. */
    portEND_SWITCHING_ISR( xHigherPriorityTaskWoken );
}
