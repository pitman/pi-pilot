/*
 * File:   ppm-driver.c
 *
 * Autopilot for UAV
 *
 * The driver for capture PPM signals.
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <p32xxxx.h>
#include <xc.h>
#include <sys/attribs.h>
#include <math.h>

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "croutine.h"
#include "timers.h"
#include "queue.h"

/* Hardware specific includes. */
#include "ConfigPerformance.h"
#include "peripheral/timer.h"
#include "peripheral/ports.h"
#include "ppm-driver.h"

/* Project setting includes */
#include "projdefs.h"

/* Queue for send decoded PPM value to main system */
extern xQueueHandle xQueuePPMDECODER;

/* Main data structure of Pi-Pilot */
extern PROJDEFS_PIPILOT pilot;

/* Timer 2 interrupt */
void __attribute__( (interrupt(ipl0), vector(_TIMER_2_VECTOR), nomips16)) vTIMER2InterruptWrapper( void );

/* Pin Change interrupt */
void __attribute__( (interrupt(ipl0), vector(_CHANGE_NOTICE_VECTOR), nomips16)) vPICHInterruptWrapper( void );

/* Overflow Timer2 interrupt handler               */
/* When normal PPM in to autopilot never be called */
/* Must be called if no any PPM found */
void vTIMER2InterruptHandler( void )
{
    static PPM_DRIVER_CHANNEL_QUEUE clear_ppm;
    static portBASE_TYPE xHigherPriorityTaskWoken;

    /* Clear interrupt */
    mT2ClearIntFlag();
    /* We want woken a task at the start of the ISR. */
    xHigherPriorityTaskWoken = pdFALSE;
    /* No any PPM found    */
    /* Clear channel value */
    clear_ppm.channel = PPM_DRIVER_INPUT_LINES + 1;
    clear_ppm.value = 0;
    xQueueSendFromISR(xQueuePPMDECODER, &clear_ppm, &xHigherPriorityTaskWoken);
    /* If sending or receiving necessitates a context switch, then switch now. */
    portEND_SWITCHING_ISR( xHigherPriorityTaskWoken );
}

/* Pin Change interrupt */
void vPICHInterruptHandler( void )
{
    static PPM_DRIVER_CHANNEL_QUEUE raw_ppm;
    static portBASE_TYPE xHigherPriorityTaskWoken;
    static PPM_DRIVER_PULSE_DECODE pulse_fl;
    static unsigned char pulse_old_state[PPM_DRIVER_INPUT_LINES] = {
        0, 0, 0, 0, 0, 0, 0, 0
    };
    static unsigned int channels_start_cnt[PPM_DRIVER_INPUT_LINES];
    static unsigned int channels_mask[PPM_DRIVER_INPUT_LINES] = {
        PPM_DRIVER_IN1, PPM_DRIVER_IN2, PPM_DRIVER_IN3, PPM_DRIVER_IN4,
        PPM_DRIVER_IN5, PPM_DRIVER_IN6, PPM_DRIVER_IN7, PPM_DRIVER_IN8
    };
    static unsigned int temp, temp_cnt;
    static unsigned char k;

    /* Clear interrupt */
    mCNClearIntFlag();
    /* read data from a port */
    temp = mPORTDRead();
    temp_cnt = ReadTimer2();
    /* Set flag pulse found */
    pulse_fl = PULSE_NONE;

    for (k = 0; k < PPM_DRIVER_INPUT_LINES; k++)
    {
        /* Search start of pulse: new state 1, old state 0 */
        if ((temp & channels_mask[k]) && (pilot.rc.channel_type[k].enable))
        {
            pulse_fl = PULSE_FOUND;
            if (!(pulse_old_state[k]))
            {
                pulse_old_state[k] = 1;
                channels_start_cnt[k] = temp_cnt;
            }
        }
        else
        {
            /* Search end of pulse: new state 0, old state 1 */
            if (pulse_old_state[k])
            {
                pulse_old_state[k] = 0;
                raw_ppm.channel = k;
                raw_ppm.value = temp_cnt - channels_start_cnt[k];
                xQueueSendFromISR(xQueuePPMDECODER, &raw_ppm, &xHigherPriorityTaskWoken);
            }
        }
    }

    /* Clear Timer2 counter when no any pulse */
    if (pulse_fl == PULSE_NONE)
        WriteTimer2(0x0000);

    /* If sending or receiving necessitates a context switch, then switch now. */
    portEND_SWITCHING_ISR( xHigherPriorityTaskWoken );
}

/* Init PPM decoder */
void vPPM_DRIVER_INIT(void)
{
    unsigned int temp;

    /* Configure IOs */
    PPM_DRIVER_IN1_INPUT;
    PPM_DRIVER_IN2_INPUT;
    PPM_DRIVER_IN3_INPUT;
    PPM_DRIVER_IN4_INPUT;
    PPM_DRIVER_IN5_INPUT;
    PPM_DRIVER_IN6_INPUT;
    PPM_DRIVER_IN7_INPUT;
    PPM_DRIVER_IN8_INPUT;
    /* Configure Timer2 */
    CloseTimer2();
    INTDisableInterrupts();
    DisableIntT2;
    /* Start timer here */
    WriteTimer2(0x0000);
    /* 40Mhz/16 timer counter */
    OpenTimer2(T2_ON | T2_SOURCE_INT | T2_PS_1_16 | T2_32BIT_MODE_OFF, PPM_DRIVER_TIMER2_TRIG);
    ConfigIntTimer2(T2_INT_ON | T2_INT_PRIOR_2 | T2_INT_SUB_PRIOR_1);
    /* Configure Pin Change interrupt */
    mCNOpen(CN_ON | CN_IDLE_CON, PPM_DRIVER_IN1_PORT |
            PPM_DRIVER_IN2_PORT | PPM_DRIVER_IN3_PORT | PPM_DRIVER_IN4_PORT |
            PPM_DRIVER_IN5_PORT | PPM_DRIVER_IN6_PORT | PPM_DRIVER_IN7_PORT |
            PPM_DRIVER_IN8_PORT, CN_PULLUP_DISABLE_ALL);
    mCNClearIntFlag();
    temp = mPORTDRead();
    mCNSetIntPriority(CHANGE_INT_PRI_2);
    mCNSetIntSubPriority(1);
    mCNIntEnable(1);
    INTEnableInterrupts();
}
