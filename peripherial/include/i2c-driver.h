/*
 * File:   i2c-driver.h
 *
 * Autopilot for UAV
 *
 * The header file for the I2C bus driver.
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef I2C_DRIVER_H
#define	I2C_DRIVER_H

#ifdef	__cplusplus
extern "C" {
#endif

#define I2C_DRIVER_IRQ_LENGHT 2

#define I2C1_CLOCK_FREQ 100000
#define I2C2_CLOCK_FREQ 400000

/* Bit-mask for detection an I2C bus start */
#define I2C_DRIVER_BUS_START  0x00000008
/* Bit-mask for detection an I2C bus stop */
#define I2C_DRIVER_BUS_STOP   0x00000010
/* Bit-mask for detection an I2C receive byte */
#define I2C_DRIVER_BUS_RCV    0x00000002
/* Bit-mask for detection an I2C ACK */
#define I2C_DRIVER_BUS_ACK    0x00008000
/* Bit-mask for detection an I2C NACK */
#define I2C_DRIVER_BUS_NACK    0x00000000
/* Bit-mask for detection an I2C bus collision  */
#define I2C_DRIVER_BUS_COLL   0x00000400

/* I2C addr read mask */
#define I2C_DRIVER_MASK_READ  0x01
/* I2C addr write mask */
#define I2C_DRIVER_MASK_WRITE 0xFE

/* The state of I2C bus */
typedef enum
{
    I2C_BUS_NONE,
    I2C_BUS_IDLE,
    I2C_BUS_START,
    I2C_BUS_RESTART,
    I2C_BUS_SEND_DATA,
    I2C_BUS_RECV_DATA,
    I2C_BUS_ACK,
    I2C_BUS_NACK,
    I2C_BUS_STOP,
    I2C_BUS_COLLISION,
} I2C_DRIVER_BUS_STATE;

/* The state of I2C packet transaction */
typedef enum
{
    I2C_PKT_NONE          = 0,
    I2C_PKT_WRITE_ADDR    = 1,
    I2C_PKT_WRITE_REG     = 2,
    I2C_PKT_WRITE_DATA    = 3,
    I2C_PKT_RESTART       = 4,
    I2C_PKT_READ_ADDR     = 5,
    I2C_PKT_READ_DATA     = 6,
} I2C_DRIVER_PKT_STATE;

/* State of power the I2C bus */
typedef enum
{
    I2C_PWR_OFF,
    I2C_PWR_ON,
} I2C_DRIVER_PWR;

/* State of dataflow via I2C */
typedef enum
{
    I2C_DF_STOP,
    I2C_DF_START,
    I2C_DF_WORK,
} I2C_DRIVER_DF_ENUM;

/* Structure for I2C bus debug */
typedef struct
{
    /* State of power the I2C bus */
    I2C_DRIVER_PWR         pwr;
    /* The state of FSM I2C bus */
    I2C_DRIVER_BUS_STATE state;
    /* The state of FSM I2C packet transaction */
    I2C_DRIVER_PKT_STATE   pkt;
    /* Counter of IRQ handler */
    unsigned long      irq_cnt;
    /* ON/OFF dataflow from the MEMS sensors */
    I2C_DRIVER_DF_ENUM  data_flow;
    /* state of an I2C bus */
    __I2C1STATbits_t i2c_state;
} I2C_DRIVER_DEBUG;

#define I2C_DRIVER_DEBUG_DATA_LENGHT 100

/* Structure of an I2C bus */
typedef struct
{
    /* State of power the I2C bus */
    I2C_DRIVER_PWR         pwr;
    /* The state of FSM I2C bus */
    I2C_DRIVER_BUS_STATE state;
    /* The state of FSM I2C packet transaction */
    I2C_DRIVER_PKT_STATE   pkt;
    /* The Frequency of I2C bus */
    unsigned long         freq;
    /* Counter of IRQ handler */
    unsigned long      irq_cnt;
    /* ON/OFF dataflow from the MEMS sensors */
    I2C_DRIVER_DF_ENUM  data_flow;
    /* Datastream overflow counter */
    unsigned long  data_overflow_cnt;
} I2C_DRIVER_I2Cx;

#define I2C_DRIVER_MAX_DATA_LENGHT     6

/* Structure of command Queue an I2C bus */
typedef struct
{
    /* ID of services CMD, used msec time counter */
    unsigned long uid;
    /* a I2C bus number */
    unsigned char port;
    /* READ/WRITE flag */
    unsigned char rw_fl;
    /* the ADDR of I2C device */
    unsigned char addr;
    /* the REG in I2C device */
    unsigned char reg;
    /* storage for out data */
    unsigned char data[I2C_DRIVER_MAX_DATA_LENGHT];
    /* counter of data byte */
    unsigned char data_cnt;
} I2C_DRIVER_RW_PKT;

#define I2C_DRIVER_PKT_DEEP 8

typedef enum
{
    I2C_OP_FAIL,
    I2C_OP_WRITE_OK,
    I2C_OP_READ_OK,
    I2C_OP_COLLISION,
    I2C_OP_SLAVE_NOT_FOUND,
    I2C_OP_SLAVE_REG_ABORT,
    I2C_OP_SLAVE_WRITE_DATA_ABORT,
    I2C_OP_SLAVE_READ_DATA_ABORT,
} I2C_DRIVER_OP_RESULT;

/* Structure of queue of an I2C bus the result of operation */
typedef struct
{
    /* ID of services CMD, used msec time counter */
    unsigned long uid;
    /* result of operation*/
    I2C_DRIVER_OP_RESULT result;
    /* storage for in data */
    unsigned char data[I2C_DRIVER_MAX_DATA_LENGHT];
    /* counter of data byte */
    unsigned char data_cnt;
} I2C_DRIVER_RW_RESULT;

#define I2C_DRIVER_PKT_RES_DEEP 8

/* Structure of MEMS types */
typedef enum
{
    LSM303DLHC_ACC_READY,
    LSM303DLHC_ACC_DATA,
    LSM303DLHC_MAG_READY,
    LSM303DLHC_MAG_DATA,
    L3G4200D_HYR_READY,
    L3G4200D_HYR_DATA,
} I2C_DRIVER_MEMS_OP_TYPE;

/* Structure of table for I2C driver MEMS sensors */
typedef struct
{
    /* type of MEMS sensor */
    I2C_DRIVER_MEMS_OP_TYPE op_type;
    /* the ADDR of I2C device */
    unsigned char addr;
    /* the REG in I2C device */
    unsigned char reg;
    /* number of bytes read/write */
    unsigned char data_lenght;
    /* Flag use or not template for check REG state */
    unsigned char template_fl;
    /* Template for detect data ready */
    unsigned char template_drdy;
} I2C_DRIVER_SENSORS;

/* Number of rows in array I2C1 IRQ */
#define I2C_DRIVER_IRQ_TABLE 6

#ifdef	__cplusplus
}
#endif

#endif	/* I2C_DRIVER_H */

