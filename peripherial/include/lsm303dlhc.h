/*
 * File:   lsm303dlhc.h
 *
 * Autopilot for UAV
 *
 * The header file for the accelerometer/magnetometer MEMS sensor LSM303DLHC
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef LSM303DLHC_H
#define	LSM303DLHC_H

#ifdef	__cplusplus
extern "C" {
#endif

#define LSM303DLHC_ACC_I2C_ADDR   0x33
#define LSM303DLHC_MAG_I2C_ADDR   0x3D

#define LSM303DLHC_ACC_CTRL_REG_1 0x20
#define LSM303DLHC_ACC_CTRL_REG_2 0x21
#define LSM303DLHC_ACC_CTRL_REG_3 0x22
#define LSM303DLHC_ACC_CTRL_REG_4 0x23
#define LSM303DLHC_ACC_STAT_REG_A 0x27
#define LSM303DLHC_ACC_OUT_X_L    0x28
#define LSM303DLHC_ACC_OUT_X_H    0x29
#define LSM303DLHC_ACC_OUT_Y_L    0x2A
#define LSM303DLHC_ACC_OUT_Y_H    0x2B
#define LSM303DLHC_ACC_OUT_Z_L    0x2C
#define LSM303DLHC_ACC_OUT_Z_H    0x2D

#define LSM303DLHC_ACC_DRDY_T     0x08
#define LSM303DLHC_ACC_AXIS_EN    0x07
#define LSM303DLHC_ACC_MULTI_READ 1 << 7

#define LSM303DLHC_MAG_CRA_REG    0x00
#define LSM303DLHC_MAG_CRB_REG    0x01
#define LSM303DLHC_MAG_MR_REG     0x02
#define LSM303DLHC_MAG_OUT_X_H    0x03
#define LSM303DLHC_MAG_OUT_X_L    0x04
#define LSM303DLHC_MAG_OUT_Y_H    0x05
#define LSM303DLHC_MAG_OUT_Y_L    0x06
#define LSM303DLHC_MAG_OUT_Z_H    0x07
#define LSM303DLHC_MAG_OUT_Z_L    0x08
#define LSM303DLHC_MAG_SR_REG_M   0x09
#define LSM303DLHC_MAG_TEMP_H     0x31
#define LSM303DLHC_MAG_TEMP_L     0x32

#define LSM303DLHC_MAG_DRDY_T     0x01

typedef enum
{
    LSM303DLHC_ACC_RATE_0   = 0,
    LSM303DLHC_ACC_RATE_1   = 1,
    LSM303DLHC_ACC_RATE_10  = 2,
    LSM303DLHC_ACC_RATE_25  = 3,
    LSM303DLHC_ACC_RATE_50  = 4,
    LSM303DLHC_ACC_RATE_100 = 5,
    LSM303DLHC_ACC_RATE_200 = 6,
    LSM303DLHC_ACC_RATE_400 = 7,
} LSM303DLHC_ACC_RATE;

typedef enum
{
    LSM303DLHC_ACC_2G       = 0,
    LSM303DLHC_ACC_4G       = 1,
    LSM303DLHC_ACC_8G       = 2,
    LSM303DLHC_ACC_16G      = 3,
} LSM303DLHC_ACC_SCALE;

typedef enum
{
    LSM303DLHC_MAG_RATE_0_75 = 0,
    LSM303DLHC_MAG_RATE_1_5  = 1,
    LSM303DLHC_MAG_RATE_3    = 2,
    LSM303DLHC_MAG_RATE_7_5  = 3,
    LSM303DLHC_MAG_RATE_15   = 4,
    LSM303DLHC_MAG_RATE_30   = 5,
    LSM303DLHC_MAG_RATE_75   = 6,
    LSM303DLHC_MAG_RATE_220  = 7,
} LSM303DLHC_MAG_RATE;

typedef enum
{
    LSM303DLHC_MAG_GAIN_1_3  = 1,
    LSM303DLHC_MAG_GAIN_1_9  = 2,
    LSM303DLHC_MAG_GAIN_2_5  = 3,
    LSM303DLHC_MAG_GAIN_4_0  = 4,
    LSM303DLHC_MAG_GAIN_4_7  = 5,
    LSM303DLHC_MAG_GAIN_5_6  = 6,
    LSM303DLHC_MAG_GAIN_8_1  = 7,
} LSM303DLHC_MAG_GAIN;

/* State of the sensor init or not */
typedef enum
{
    LSM303DLHC_STOP,
    LSM303DLHC_READY,
} LSM303DLHC_STATE_ENUM;

typedef struct
{
    /* Temperature of the sensor */
    char                          t;
    /* Data rate of the ACC */
    LSM303DLHC_ACC_RATE    acc_rate;
    /* Scale of the ACC */
    LSM303DLHC_ACC_SCALE  acc_scale;
    /* Measure counter */
    long                   acc_mcnt;
    /* RAW data from the sensors X axis */
    short                 acc_raw_x;
    /* RAW data from the sensors Y axis */
    short                 acc_raw_y;
    /* RAW data from the sensors Z axis */
    short                 acc_raw_z;
    /* Real data from the sensors X axis */
    float                     acc_x;
    /* Real data from the sensors Y axis */
    float                     acc_y;
    /* Real data from the sensors Z axis */
    float                     acc_z;
    /* Euler angles from ACC */
    float            acc_euler_roll;
    float           acc_euler_pitch;
    float             acc_euler_yaw;
    /* Data rate of MAG */
    LSM303DLHC_MAG_RATE    mag_rate;
    /* GAIN of MAG */
    LSM303DLHC_MAG_GAIN    mag_gain;
    /* State of the sensor */
    LSM303DLHC_STATE_ENUM     state;
    /* Measure counter */
    long                   mag_mcnt;
    /* RAW data from the sensors X axis */
    short                 mag_raw_x;
    /* RAW data from the sensors Y axis */
    short                 mag_raw_y;
    /* RAW data from the sensors Z axis */
    short                 mag_raw_z;
    /* Real data from the sensors X axis */
    float                     mag_x;
    /* Real data from the sensors Y axis */
    float                     mag_y;
    /* Real data from the sensors Z axis */
    float                     mag_z;
    /* next temperature measure time */
    long                     time_t;
    /* time step between read temperature */
    long                time_t_step;
    /* Euler angles from MAG */
    float            mag_euler_roll;
    float           mag_euler_pitch;
    float             mag_euler_yaw;
} LSM303DLHC_SENSOR_CFG;

#ifdef	__cplusplus
}
#endif

#endif	/* LSM303DLHC_H */

