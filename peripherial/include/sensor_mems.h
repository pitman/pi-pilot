/*
 * File:   sensor_mems.h
 *
 * Autopilot for UAV
 *
 * Some defines for the MEMS sensors.
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef SENSOR_MEMS_H
#define	SENSOR_MEMS_H

#ifdef	__cplusplus
extern "C" {
#endif

/* ------------------------------------------------- */
/* Defines for accelerometer sensor LSM303DLHC start */

#define ACC_TYPE_LSM303DLHC          0
#define ACC_LSM303DLHC_ADDR      (0x33 >> 1)

#define ACC_LSM303DLHC_CTRL_REG_1 0x20
#define ACC_LSM303DLHC_CTRL_REG_2 0x21
#define ACC_LSM303DLHC_CTRL_REG_3 0x22
#define ACC_LSM303DLHC_CTRL_REG_4 0x23
#define ACC_LSM303DLHC_STAT_REG_A 0x27
#define ACC_LSM303DLHC_OUT_X_L    0x28
#define ACC_LSM303DLHC_OUT_X_H    0x29
#define ACC_LSM303DLHC_OUT_Y_L    0x2A
#define ACC_LSM303DLHC_OUT_Y_H    0x2B
#define ACC_LSM303DLHC_OUT_Z_L    0x2C
#define ACC_LSM303DLHC_OUT_Z_H    0x2D

/* Defines for accelerometer sensor LSM303DLHC start */
/* ------------------------------------------------- */

/* ------------------------------------------------ */
/* Defines for magnetometer sensor LSM303DLHC start */

#define MAG_TYPE_LSM303DLHC          0
#define MAG_LSM303DLHC_ADDR      (0x3D >> 1)

#define MAG_LSM303DLHC_CRA_REG    0x00
#define MAG_LSM303DLHC_CRB_REG    0x01
#define MAG_LSM303DLHC_MR_REG     0x02
#define MAG_LSM303DLHC_OUT_X_H    0x03
#define MAG_LSM303DLHC_OUT_X_L    0x04
#define MAG_LSM303DLHC_OUT_Y_H    0x05
#define MAG_LSM303DLHC_OUT_Y_L    0x06
#define MAG_LSM303DLHC_OUT_Z_H    0x07
#define MAG_LSM303DLHC_OUT_Z_L    0x08
#define MAG_LSM303DLHC_SR_REG_M   0x09

/* Defines for magnetometer sensor LSM303DLHC stop  */
/* ------------------------------------------------ */

/* ------------------------------------------- */
/* Defines for hyroscope sensor L3G4200D start */

#define HYR_TYPE_L3G4200D            0
#define HYR_L3G4200D_ADDR        (0xD2 >> 1)

#define HYR_L3G4200D_ID_REG       0x0F
#define HYR_L3G4200D_CTRL_REG1    0x20
#define HYR_L3G4200D_CTRL_REG2    0x21
#define HYR_L3G4200D_CTRL_REG3    0x22
#define HYR_L3G4200D_CTRL_REG4    0x23
#define HYR_L3G4200D_CTRL_REG5    0x24
#define HYR_L3G4200D_REFERENCE    0x25
#define HYR_L3G4200D_OUT_TEMP     0x26
#define HYR_L3G4200D_STATUS_REG   0x27
#define HYR_L3G4200D_OUT_X_L      0x28
#define HYR_L3G4200D_OUT_X_H      0x29
#define HYR_L3G4200D_OUT_Y_L      0x2A
#define HYR_L3G4200D_OUT_Y_H      0x2B
#define HYR_L3G4200D_OUT_Z_L      0x2C
#define HYR_L3G4200D_OUT_Z_H      0x2D

#define HYR_L3G4200D_CHIP_ID      0xD3

/* Defines for hyroscope sensor L3G4200D stop  */
/* ------------------------------------------- */

#ifdef	__cplusplus
}
#endif

#endif	/* SENSOR_MEMS_H */

