/*
 * File:   pid.h
 *
 * Autopilot for UAV
 *
 * The header file for PID
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef PID_H
#define	PID_H

#ifdef	__cplusplus
extern "C" {
#endif

typedef struct {
    /* Previouse error */
    float err_prev;
    /* Previouse integral value */
    float int_prev;
    /* Proportional Gain */
    float  Kp_gain;
    /* Integral Gain */
    float  Ki_gain;
    /* Derivative Gain */
    float  Kd_gain;
    /* Maximum allowable integrator state */
    float   Ki_max;
} PID_STRUCT;


#define PID_ROLL_Kp    10.0
#define PID_ROLL_Ki     2.0
#define PID_ROLL_Kd     0.1
#define PID_ROLL_Ki_max 500

#define PID_PITCH_Kp    10.0
#define PID_PITCH_Ki     2.0
#define PID_PITCH_Kd     0.1
#define PID_PITCH_Ki_max 500

#define PID_YAW_Kp    10.0
#define PID_YAW_Ki     2.0
#define PID_YAW_Kd     0.1
#define PID_YAW_Ki_max 500

#define PID_ALT_Kp    10.0
#define PID_ALT_Ki     2.0
#define PID_ALT_Kd     0.1
#define PID_ALT_Ki_max 500

/* Calculate PID */
float fPidUpdate(PID_STRUCT *pid, float position_hold, float position_measured);

#ifdef	__cplusplus
}
#endif

#endif	/* PID_H */

