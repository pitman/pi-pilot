/*
 * File:   range_meters.h
 *
 * Autopilot for UAV
 *
 * The header file for Range meters
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef RANGE_METERS_H
#define	RANGE_METERS_H

#ifdef	__cplusplus
extern "C" {
#endif

typedef union {
    struct {
        unsigned char IR_near_measure: 1;
        unsigned char IR_near_ok:      1;
        unsigned char IR_long_measure: 1;
        unsigned char IR_long_ok:      1;
        unsigned char US_long_measure: 1;
        unsigned char US_long_ok:      1;
        unsigned char RM_result_ok:    1;
    };
    struct {
        unsigned char ALL_flags:       8;
    };
} stRangeMetersFlags;

struct stRangeMetersBottom {
    unsigned int IR_near;
    unsigned int IR_long;
    unsigned int US_long;
    unsigned int Result;
    stRangeMetersFlags b_flags;
};

struct stRangeMetersSide {
    unsigned int IR_long;
    unsigned int US_long;
    unsigned int Result;
    stRangeMetersFlags s_flags;
};

enum UltraSonicWorkflow {
    US_IDLE,
    US_TRIG,
    US_WAIT_ECHO,
    US_READ_ECHO,
    US_TIMEOUT_AFTER
};

#define IR_NEAR_MIN      30
#define IR_NEAR_MAX     380
#define IR_LONG_MIN     400
#define IR_LONG_MAX    1500
#define US_LONG_MIN     100
#define US_LONG_MAX    2700

/* Define for 10uS TRIG pulse */
/* period 0.8us*7=6.4uS */
#define US_TRIG_PERIOD    0x0003
#define US_TRIG_TIME      2
/* Define for limit wait ECHO, 40mS */
/* period 0.8uS*100=80uS */
#define US_ECHO_PERIOD    0x0064
#define US_WAIT_ECHO_TIME 495
/* Define for limit read ECHO, 40ms */
#define US_MAX_ECHO_TIME  495
/* Define for timeout after measurements */
/* period for timeout after measure 0.8uS*37500=30mS */
#define US_TIMEOUT_PERIOD 0x493E
#define US_AFTER_TIME     2

#define US_DISTANCE_HCSR04_K     13.7931

/*
 * Init some structure for a range meters.
 */
void vRangeMetersInit(void);

/*
 * Calculate distance on mm after ADC measurement
 * function only for Sharp GP2D120X IR range meters
 */
unsigned int uiRange_MetersCalcIRDistance2D120(unsigned int uiData);

/*
 * Calculate distance on mm after UltraSonic measure
 * function only for HC-SR04 US range meters
 */
unsigned int uiRange_MetersCalcUSDistanceHCSR04(unsigned int uiData);

#ifdef	__cplusplus
}
#endif

#endif	/* RANGE_METERS_H */

