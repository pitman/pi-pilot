/*
 * File:   l3g4200d.h
 *
 * Autopilot for UAV
 *
 * The header file for Hyroscope MEMS sensor L3G4200D
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef L3G4200D_H
#define	L3G4200D_H

#ifdef	__cplusplus
extern "C" {
#endif

#define L3G4200D_HYR_I2C_ADDR     0xD2

#define L3G4200D_HYR_ID_REG       0x0F
#define L3G4200D_HYR_CTRL_REG1    0x20
#define L3G4200D_HYR_CTRL_REG2    0x21
#define L3G4200D_HYR_CTRL_REG3    0x22
#define L3G4200D_HYR_CTRL_REG4    0x23
#define L3G4200D_HYR_CTRL_REG5    0x24
#define L3G4200D_HYR_REFERENCE    0x25
#define L3G4200D_HYR_OUT_TEMP     0x26
#define L3G4200D_HYR_STATUS_REG   0x27
#define L3G4200D_HYR_OUT_X_L      0x28
#define L3G4200D_HYR_OUT_X_H      0x29
#define L3G4200D_HYR_OUT_Y_L      0x2A
#define L3G4200D_HYR_OUT_Y_H      0x2B
#define L3G4200D_HYR_OUT_Z_L      0x2C
#define L3G4200D_HYR_OUT_Z_H      0x2D

#define L3G4200D_HYR_DRDY_T       0x08
#define L3G4200D_HYR_ID           0xD3
#define L3G4200D_HYR_AXIS_EN      0x0F
#define L3G4200D_HYR_MULTI_READ   1 << 7

typedef enum
{
    L3G4200D_HYR_RATE_100 = 0,
    L3G4200D_HYR_RATE_200 = 1,
    L3G4200D_HYR_RATE_400 = 2,
    L3G4200D_HYR_RATE_800 = 3,
} L3G4200D_HYR_DATA_RATE;

typedef enum
{
    L3G4200D_HYR_BW_LOW      = 0,
    L3G4200D_HYR_BW_MED_LOW  = 1,
    L3G4200D_HYR_BW_MED_HIGH = 2,
    L3G4200D_HYR_BW_HIGH     = 3,
} L3G4200D_HYR_BANDWIDTH;

typedef enum
{
    L3G4200D_HYR_250  = 0,
    L3G4200D_HYR_500  = 1,
    L3G4200D_HYR_2000 = 2,
} L3G4200D_HYR_SCALE;

/* State of the sensor init or not */
typedef enum
{
    L3G4200D_STOP,
    L3G4200D_READY,
} L3G4200D_STATE_ENUM;

typedef struct
{
    /* Temperature of the sensor */
    char                           t;
    /* Data rate */
    L3G4200D_HYR_DATA_RATE data_rate;
    /* Bandwith */
    L3G4200D_HYR_BANDWIDTH bandwidth;
    /* Full scale */
    L3G4200D_HYR_SCALE         scale;
    /* State of the sensor */
    L3G4200D_STATE_ENUM        state;
    /* Measure counter */
    unsigned long               mcnt;
    /* Calibration counter */
    unsigned long               ccnt;
    /* Previouse measure timestamp */
    unsigned long  prev_measure_time;
    /* Calibration value */
    float                      err_x;
    float                      err_y;
    float                      err_z;
    /* RAW data from the sensors X axis */
    short                      raw_x;
    /* RAW data from the sensors Y axis */
    short                      raw_y;
    /* RAW data from the sensors Z axis */
    short                      raw_z;
    /* Real data from the sensors X axis */
    float                          x;
    /* Real data from the sensors Y axis */
    float                          y;
    /* Real data from the sensors Z axis */
    float                          z;
    /* next temperature measure time */
    long                      time_t;
    /* time step between read temperature */
    long                 time_t_step;
} L3G4200D_SENSOR_CFG;

#ifdef	__cplusplus
}
#endif

#endif	/* L3G4200D_H */

