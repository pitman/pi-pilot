/*
 * File:   pwm-driver.h
 *
 * Autopilot for UAV
 *
 * The header file for PWM
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef PWM_DRIVER_H
#define	PWM_DRIVER_H

#ifdef	__cplusplus
extern "C" {
#endif

/* Hardware controlled PWMs */
#define PWM_DRIVER_HPWM_CNT 4
/* Software controlled PWMs */
#define PWM_DRIVER_SPWM_CNT 8

/* Timer period for Hardware PWM outputs */
#define PWM_DRIVER_HPWM_PERIOD  0x61A8
/* Define minimum Harware PWM value for servos */
#define PWM_DRIVER_HPWM_MIN_VAL    900
/* Define maximum Hardware PWM value for servos */
#define PWM_DRIVER_HPWM_MAX_VAL   2750
/* Define default middle Hardware PWM value for servos */
#define PWM_DRIVER_HPWM_MID_VAL   1825
/* Define minimum Software PWM value for servos */
#define PWM_DRIVER_SPWM_MIN_VAL      0
/* Define maximum Software PWM value for servos */
#define PWM_DRIVER_SPWM_MAX_VAL   1625
/* Define default middle Software PWM value for servos */
#define PWM_DRIVER_SPWM_MID_VAL    810
/* Define half of time 0.9ms */
#define PWM_DRIVER_SPWM_HALF_0_9   560
/* Define RST time of expander 3.0ms */
#define PWM_DRIVER_SPWM_RST        560
/* Define 16ms period time */
#define PWM_DRIVER_SPWM_PERIOD   25000
/* GPIO for software PWM */
/* Reset of the expander pin */
#define PWM_DRIVER_SPWM_MRST_IO  1 << 1
#define PWM_DRIVER_SPWM_MRST_OUT TRISFCLR = PWM_DRIVER_SPWM_MRST_IO
#define PWM_DRIVER_SPWM_MRST_ODC ODCFSET = PWM_DRIVER_SPWM_MRST_IO
#define PWM_DRIVER_SPWM_MRST_ON  LATFSET = PWM_DRIVER_SPWM_MRST_IO
#define PWM_DRIVER_SPWM_MRST_OFF LATFCLR = PWM_DRIVER_SPWM_MRST_IO
/* Clock of the expander pin */
#define PWM_DRIVER_SPWM_CLK_IO   1 << 1
#define PWM_DRIVER_SPWM_CLK_OUT  TRISGCLR = PWM_DRIVER_SPWM_CLK_IO
#define PWM_DRIVER_SPWM_CLK_1    LATGSET = PWM_DRIVER_SPWM_CLK_IO
#define PWM_DRIVER_SPWM_CLK_0    LATGCLR = PWM_DRIVER_SPWM_CLK_IO
/* State of software PWM engine */
typedef enum
{
    SPWM_RST_ON   =  0,
    SPWM_RST_OFF  =  1,
    SPWM_CHAN_ON  =  2,
    SPWM_CHAN_OFF =  3,
    SPWM_SLEEP    =  4,
} PWM_DRIVER_SPWM_STATE;

/* Type of servo */
typedef enum
{
    /* Not connected */
    TYPE_NONE        = 0,
    /* Start point 900, End point 2750 */
    TYPE_0_100       = 1,
    /* Start point 1825, minimum value 900, maximum 2750 */
    TYPE_50          = 2,
    /* Start point 2750, end point 900 */
    TYPE_100_0       = 3,
    /* User defined offset of null point a servo */
    TYPE_USER        = 4,
    /* Static mapped to an input channel */
    TYPE_MAP_STATIC  = 5,
    /* Dynamic mapping to some input channels */
    TYPE_MAP_DYNAMIC = 6,
} PWM_DRIVER_SERVO_TYPE;

/* Limit for servo defines
 * 
 * 4 Type of servos:
 * 
 * 1. Start point 900, End point 2750, limit set to maximum value only
 *           max = 2200;
 * 2. Start point 1825, minimum value 900, maximum 2750, limit setted from 1825 to min/max
 *           min = 1300; max = 2540;
 * 3. Start point 2750, end point 900, limit set to minimum value only
 *           min = 1600;
 * 4. User defined null point, must be filled by user before start FLY.
 */

typedef struct
{
    /* Set limit of minimum value */
    unsigned int hpwm_min[PWM_DRIVER_HPWM_CNT];
    /* Set limit of maxminum value */
    unsigned int hpwm_max[PWM_DRIVER_HPWM_CNT];
    /* Set start null point of servo */
    unsigned int hpwm_null_point[PWM_DRIVER_HPWM_CNT];
    /* Set type of servo normal/inversion */
    int hpwm_servo_mode[PWM_DRIVER_HPWM_CNT];
    /* MAP some input channels to the PWM channels */
    unsigned char hpwm_servo_map[PWM_DRIVER_HPWM_CNT];
    /* Set limit of minimum value */
    unsigned int spwm_min[PWM_DRIVER_SPWM_CNT];
    /* Set limit of maxminum value */
    unsigned int spwm_max[PWM_DRIVER_SPWM_CNT];
    /* Set start null point of servo */
    unsigned int spwm_null_point[PWM_DRIVER_SPWM_CNT];
    /* Set type of servo normal/inversion */
    int spwm_servo_mode[PWM_DRIVER_SPWM_CNT];
    /* MAP some input channels to the PWM channels */
    unsigned char spwm_servo_map[PWM_DRIVER_HPWM_CNT];
    /* value data storage for hardware PWM of servos */
    unsigned int hpwm[PWM_DRIVER_HPWM_CNT];
    /* type of servos with hardware PWM */
    PWM_DRIVER_SERVO_TYPE hpwm_type[PWM_DRIVER_HPWM_CNT];
    /* value data storage for software PWM of servos */
    unsigned int spwm[PWM_DRIVER_SPWM_CNT];
    /* type of servos with software PWM */
    PWM_DRIVER_SERVO_TYPE spwm_type[PWM_DRIVER_SPWM_CNT];
    /* state of software PWM engine */
    PWM_DRIVER_SPWM_STATE spwm_state;
    unsigned char      spwm_chan_cnt;
} PWM_DRIVER_STRUCT;

/* Init hardware and software PWMs */
void vPWMDRIVER_INIT_PWM(void);

/* Start Hardware and Software PWMs */
void vPWMDRIVER_START_PWM(void);

/* Stop Hardware and Software PWMs */
void vPWMDRIVER_STOP_PWM(void);

/* Init value of PWM before start */
void vPWMDRIVER_PWM_UPDATE(void);

#ifdef	__cplusplus
}
#endif

#endif	/* PWM_DRIVER_H */

