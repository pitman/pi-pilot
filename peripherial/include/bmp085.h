/*
 * File:   i2c1_isr.S
 *
 * Autopilot for UAV
 *
 * The header file for pressure sensor BMP085
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef BMP085_H
#define	BMP085_H

#ifdef	__cplusplus
extern "C" {
#endif

#define BMP085_I2C_ADDR  0xEE
#define BMP085_CHIP_ID   0x55
#define BMP085_ROM_ADDR  0xAA
#define BMP085_ROM_LEN     22
#define BMP085_REG_ID    0xD0
#define BMP085_REG_VER   0xD1

#define BMP085_REG_AC1   0xAA
#define BMP085_REG_AC2   0xAC
#define BMP085_REG_AC3   0xAE
#define BMP085_REG_AC4   0xB0
#define BMP085_REG_AC5   0xB2
#define BMP085_REG_AC6   0xB4
#define BMP085_REG_B1    0xB6
#define BMP085_REG_B2    0xB8
#define BMP085_REG_MB    0xBA
#define BMP085_REG_MC    0xBC
#define BMP085_REG_MD    0xBE
#define BMP085_REG_CTRL  0xF4
#define BMP085_REG_ADC_H 0xF6
#define BMP085_REG_ADC_L 0xF7
#define BMP085_REG_ADC_E 0xF8

#define BMP085_REG_SRES  0xE0

#define BMP085_TEMP      0x2E

/* for get real time n*10ms */
#define BMP085_IDLE_TIME    5
#define BMP085_PRESS_TIME   2

/* Oversampling setting of the BMP085 */
typedef enum
{
    BMP085_OSS_0 = 0,
    BMP085_OSS_1 = 1,
    BMP085_OSS_2 = 2,
    BMP085_OSS_3 = 3,
} BMP085_OSS_ENUM;

/* State of the sensor init or not */
typedef enum
{
    BMP085_STOP,
    BMP085_READY,
} BMP085_STATE_ENUM;

typedef struct
{
    /* Oversampling pressure setting for the BMP085 sensor*/
    BMP085_OSS_ENUM     oss;
    /* RAW temperature of the sensor */
    long                 ut;
    /* Calculated real temperature */
    long                  t;
    /* RAW pressure */
    long                 up;
    /* Calculated real pressure */
    long              press;
    /* State of the sensor */
    BMP085_STATE_ENUM state;
    /* Measure counter */
    unsigned long      mcnt;
    /* Calibration counter */
    unsigned long      ccnt;
    /* Calibration value from the sensor's internal EPROM */
    short               ac1;
    long               ac1m;
    short               ac2;
    short               ac3;
    unsigned short      ac4;
    unsigned short      ac5;
    unsigned short      ac6;
    short                b1;
    short                b2;
    long                 b5;
    short                mb;
    short                mc;
    long                mcm;
    short                md;
} BMP085_SENSOR_CFG;

#ifdef	__cplusplus
}
#endif

#endif	/* BMP085_H */

