/*
 * File:   console.h
 *
 * Autopilot for UAV
 *
 * The header file for print messages via USB CDC
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */
#ifndef CONSOLE_H
#define	CONSOLE_H

#include "../../FreeRTOS/Source/include/projdefs.h"

#ifdef	__cplusplus
extern "C" {
#endif

extern void vConsolePrintData(char *pcData);
extern void vConsolePrintDataROM(const ROM char *pcData);

#ifdef	__cplusplus
}
#endif

#endif	/* CONSOLE_H */

