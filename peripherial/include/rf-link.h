/*
 * File:   main.c
 *
 * Autopilot for UAV
 *
 * The header file for RF Link 
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef RF_LINK_H
#define	RF_LINK_H

#ifdef	__cplusplus
extern "C" {
#endif

#define RFLINK_BAUD 57600

#define RFLINK_BAUD_VALUE  ((PBCLK/16/RFLINK_BAUD)-1)

#ifndef RFLINK_UART
#define RFLINK_UART UART1
#endif

#if RFLINK_UART==UART1
#define RFLINK_UART_VECTOR INT_UART_1_VECTOR
#define RFLINK_INT_RX      INT_U1RX
#define RFLINK_INT_TX      INT_U1TX
#define RFLINK_UIO         1 << 4
#elif RFLINK_UART==UART2
#define RFLINK_UART_VECTOR INT_UART_2_VECTOR
#define RFLINK_INT_RX      INT_U2RX
#define RFLINK_INT_RX      INT_U2TX
#define RFLINK_UIO         1 << 3
#elif RFLINK_UART==UART3
#define RFLINK_UART_VECTOR INT_UART_3_VECTOR
#define RFLINK_INT_RX      INT_U3RX
#define RFLINK_INT_RX      INT_U3TX
#define RFLINK_UIO         1 << 2
#else
"ERROR: Incorrect UART number for RF-LINK!"
#endif

#define RFLINK_UIO_OUTPUT  TRISECLR = RFLINK_UIO
#define RFLINK_UIO_SET_0   LATECLR = RFLINK_UIO
#define RFLINK_UIO_SET_1   LATESET = RFLINK_UIO

#ifdef	__cplusplus
}
#endif

#endif	/* RF_LINK_H */

