/*
 * File:   sens.h
 *
 * Autopilot for UAV
 *
 * The header file for any sensors.
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef SENS_H
#define	SENS_H

#ifdef	__cplusplus
extern "C" {
#endif

/* Skip data read time before calculate median error 0.001 sec resolution */
#define SENS_DATA_SKIP_TIME  5000
/* Define time for read sensors on a ground for collibration 0.001 sec resolution */
#define SENS_DATA_COLL_TIME  30000

/* Max databytes for read/write */
#define SENS_DATA_LENGHT_MAX 6

typedef enum
{
    SENS_LSM303DLHC_ACC,
    SENS_LSM303DLHC_MAG,
    SENS_L3G4200D_HYR,
    SENS_LSM303DLHC_TEMP,
    SENS_L3G4200D_TEMP,
    SENS_BMP085_TEMP,
    SENS_BMP085_PRESS,
} SENS_DATA_TYPE;

typedef struct
{
    /* ID of services CMD, used msec time counter */
    unsigned long time;
    /* TYPE of sensor */
    SENS_DATA_TYPE sens_type;
    /* storage for out data */
    unsigned char data[SENS_DATA_LENGHT_MAX];
    /* counter of data byte */
    unsigned char data_cnt;
} SENS_I2C_QUEUE_STRUCT;

/* Define Queue depth for data from a sensor */
#define SENS_I2C_QUEUE_DEEP 16

#ifdef	__cplusplus
}
#endif

#endif	/* SENS_H */

