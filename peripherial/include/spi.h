/*
 * File:   spi.h
 *
 * Autopilot for UAV
 *
 * Some defines for the SPI bus.
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef SPI_H
#define	SPI_H

#ifdef	__cplusplus
extern "C" {
#endif

#define SPI_CS              1 << 9
#define SPI_CS_SET_OUTPUT   TRISDbits.TRISD9 = 0
#define SPI_CS_LOW          PORTDbits.RD9 = 0
#define SPI_CS_HIGH         PORTDbits.RD9 = 1

#ifdef	__cplusplus
}
#endif

#endif	/* SPI_H */

