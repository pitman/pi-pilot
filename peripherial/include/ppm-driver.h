/*
 * File:   ppm-driver.h
 *
 * Autopilot for UAV
 *
 * The header file for PPM capture driver.
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef PPM_DRIVER_H
#define	PPM_DRIVER_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "projdefs.h"
#include "pwm-driver.h"

#define PPM_DRIVER_TIMER2_TRIG 0xFFFF
/* Define number of input lines */
#define PPM_DRIVER_INPUT_LINES    8
/* Define number of signals */
#define PPM_DRIVER_INPUT_SIGNALS  4
/* Define number of switches */
#define PPM_DRIVER_INPUT_SWITCHES 4

/* Type of RC input signal */
typedef enum {
    /* No decode RC lines */
    RC_TYPE_NONE,
    /* All signals via 1 wire link */ /* Not supported yet */
    RC_TYPE_PPM,
    /* Decode 8 input lines PPM */
    RC_TYPE_PPM8,
} PPM_DRIVER_TYPE_RC;

/* Type of input channel
 * - common RC input channel
 * - switch mapping input channels to output
 * - signal to internal autopilot
 */
#define PPM_DRIVER_CHAN_RC     0x01
#define PPM_DRIVER_CHAN_SWITCH 0x02
#define PPM_DRIVER_CHAN_SIGNAL 0x04
#define PPM_DRIVER_CHAN_ON        1
#define PPM_DRIVER_CHAN_OFF       0
#define PPM_DRIVER_CHAN_NMAP   PPM_DRIVER_INPUT_LINES

typedef union {
    BYTE data;

    struct {
        /* Type of input channel */
        BYTE type:4;
        /* Enable/Disable channel */
        BYTE enable:1;
        /* State of input signale decoding: 0 - not decoded, 1 - decode ok */
        BYTE state:1;
    };
} PPM_DRIVER_CHAN_TYPE;

typedef enum {
    PERIOD_CHANNEL_1,
    PERIOD_CHANNEL_2,
    PERIOD_CHANNEL_3,
    PERIOD_CHANNEL_4,
    PERIOD_CHANNEL_5,
    PERIOD_CHANNEL_6,
    PERIOD_CHANNEL_7,
    PERIOD_CHANNEL_8,
} PPM_DRIVER_CHAN_PERIOD;

typedef struct {
    BYTE enable:1;
    BYTE state:1;
    BYTE prev_state:1;
    BYTE channel;
    unsigned int signal;
} PPM_DRIVER_SIGNAL_STRUCT;

typedef struct {
    BYTE enable:1;
    BYTE state:1;
    BYTE prev_state:1;
    BYTE channel;
    unsigned char hpwm_use[PWM_DRIVER_HPWM_CNT];
    unsigned int hpwm_map_on[PWM_DRIVER_HPWM_CNT];
    unsigned int hpwm_map_off[PWM_DRIVER_HPWM_CNT];
    unsigned char spwm_use[PWM_DRIVER_SPWM_CNT];
    unsigned int spwm_map_on[PWM_DRIVER_SPWM_CNT];
    unsigned int spwm_map_off[PWM_DRIVER_SPWM_CNT];
} PPM_DRIVER_SWITCH_STRUCT;

/* Main RC decoder structure */
typedef struct {
    PPM_DRIVER_TYPE_RC rc_type;
    unsigned int channel[PPM_DRIVER_INPUT_LINES];
    PPM_DRIVER_CHAN_TYPE  channel_type[PPM_DRIVER_INPUT_LINES];
    PPM_DRIVER_SIGNAL_STRUCT channel_signal[PPM_DRIVER_INPUT_SIGNALS];
    PPM_DRIVER_SWITCH_STRUCT channel_switch[PPM_DRIVER_INPUT_SWITCHES];
    unsigned int channel_min[PPM_DRIVER_INPUT_LINES];
    unsigned int channel_mid[PPM_DRIVER_INPUT_LINES];
    unsigned int channel_max[PPM_DRIVER_INPUT_LINES];
    unsigned int channel_range[PPM_DRIVER_INPUT_LINES];
} PPM_DRIVER_INRC;

typedef enum {
    PULSE_NONE,
    PULSE_FOUND,
} PPM_DRIVER_PULSE_DECODE;

/* Struct for Queue */
typedef struct {
    unsigned char channel;
    unsigned int  value;
} PPM_DRIVER_CHANNEL_QUEUE;

#define PPM_DRIVER_QUEUE_DEEP PPM_DRIVER_INPUT_LINES

/* Init PPM decoder */
void vPPM_DRIVER_INIT(void);

#define PPM_DRIVER_IN1 1 << 8
#define PPM_DRIVER_IN2 1 << 13
#define PPM_DRIVER_IN3 1 << 5
#define PPM_DRIVER_IN4 1 << 11
#define PPM_DRIVER_IN5 1 << 6
#define PPM_DRIVER_IN6 1 << 7
#define PPM_DRIVER_IN7 1 << 12
#define PPM_DRIVER_IN8 1 << 13

#define PPM_DRIVER_IN1_PORT            0
#define PPM_DRIVER_IN2_PORT  CN19_ENABLE
#define PPM_DRIVER_IN3_PORT  CN14_ENABLE
#define PPM_DRIVER_IN4_PORT            0
#define PPM_DRIVER_IN5_PORT  CN15_ENABLE
#define PPM_DRIVER_IN6_PORT  CN16_ENABLE
#define PPM_DRIVER_IN7_PORT            0
#define PPM_DRIVER_IN8_PORT            0

#define PPM_DRIVER_PORT_CHANNEL_1_7 PORTD
#define PPM_DRIVER_PORT_CHANNEL_8   PORTF

#define PPM_DRIVER_IN1_INPUT TRISDbits.TRISD8  = 1
#define PPM_DRIVER_IN2_INPUT TRISDbits.TRISD13 = 1
#define PPM_DRIVER_IN3_INPUT TRISDbits.TRISD5  = 1
#define PPM_DRIVER_IN4_INPUT TRISDbits.TRISD11 = 1
#define PPM_DRIVER_IN5_INPUT TRISDbits.TRISD6  = 1
#define PPM_DRIVER_IN6_INPUT TRISDbits.TRISD7  = 1
#define PPM_DRIVER_IN7_INPUT TRISDbits.TRISD12 = 1
#define PPM_DRIVER_IN8_INPUT TRISFbits.TRISF0  = 1

#ifdef	__cplusplus
}
#endif

#endif	/* PPM_DRIVER_H */

