/*
 * File:   sensor_pressure.h
 *
 * Autopilot for UAV
 *
 * Some defines for pressure sensor.
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */


#ifndef SENSOR_PRESSURE_H
#define	SENSOR_PRESSURE_H

#ifdef	__cplusplus
extern "C" {
#endif

/* Sampling number for determine ground level  */
#define BAR_GROUND_SAMPLE      25

/* ----------------------------------------- */
/* Defines for barometer sensor BMP085 start */

#define BAR_TYPE_BMP085         0
#define BAR_BMP085_ADDR      0xEE >> 1

#define BAR_BMP085_CHIP_ID   0x55
#define BAR_BMP085_ROM_ADDR  0xAA
#define BAR_BMP085_ROM_LEN     22
#define BAR_BMP085_REG_ID    0xD0
#define BAR_BMP085_REG_VER   0xD1

#define BAR_BMP085_REG_CTRL  0xF4
#define BAR_BMP085_REG_ADC_H 0xF6
#define BAR_BMP085_REG_ADC_L 0xF7
#define BAR_BMP085_REG_ADC_E 0xF8

#define BAR_BMP085_REG_SRES  0xE0

#define BAR_BMP085_REG_TEMP  0x2E

#define BAR_BMP085_IDLE_TIME    5
#define BAR_BMP085_CONV_TIME    2

/* Defines for barometer sensor BMP085 stop  */
/* ----------------------------------------- */


#ifdef	__cplusplus
}
#endif

#endif	/* SENSOR_PRESSURE_H */

