/*
 * File:   log.c
 *
 * Autopilot for UAV
 *
 * Logging subsystem
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <p32xxxx.h>

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "croutine.h"
#include "timers.h"
#include "queue.h"

/* Project setting includes */
#include "projdefs.h"
#include "logd.h"

/* Local time with 1ms resolution */
extern unsigned long ulLOCALTIME;

/* Queue for message system */
extern xQueueHandle xQueueMessage;
/* System log level filter */
extern unsigned char ucMainMessageLevel;

/* Send log messages to USB-TTY */
void vLogdPrint(unsigned char flag, char *messages, unsigned char time)
{
    LOGD_MESSAGE_STRUCT logmsg;

    if (flag <= ucMainMessageLevel)
    {
        logmsg.time = ulLOCALTIME;
        logmsg.flag = flag;
        sprintf(logmsg.message, messages);
        xQueueSend(xQueueMessage, &logmsg, time/portTICK_RATE_MS);
    }
}

/* Send log message from ROM to USB-TTY */
void vLogdPrintROM(unsigned char flag, const char *messages, unsigned char time)
{
    LOGD_MESSAGE_STRUCT logmsg;

    if (flag <= ucMainMessageLevel)
    {
        logmsg.time = ulLOCALTIME;
        logmsg.flag = flag;
        sprintf(logmsg.message, messages);
        xQueueSend(xQueueMessage, &logmsg, time/portTICK_RATE_MS);
    }
}