/* 
 * File:   main.c
 * Author: dimon
 *
 * Created on 5 ?????? 2013 ?., 15:54
 */

#include <stdio.h>
#include <stdlib.h>

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "croutine.h"
#include "timers.h"
#include "queue.h"

/* Hardware specific includes. */
#include "ConfigPerformance.h"

/* Core configuratin fuse settings */

#pragma config FPLLIDIV = DIV_2 /* External crystal is 8MHz, divide it by 2 */
#pragma config FPLLMUL = MUL_20 /* Set Freq multipler to 20, core Freq 80MHz */
#pragma config FPLLODIV = DIV_1 /* Didn't divide core Freq after multiplier */
#pragma config FWDTEN = OFF     /* Disable Watchdog */
#pragma config POSCMOD = HS     /* External high-speed crystal select */
#pragma config FNOSC = PRIPLL   /* Enable PLL for Freq from HS crystal */
#pragma config FPBDIV = DIV_2   /* Set divider for peripherial bus clock to 2 */
#pragma config FSOSCEN = OFF    /* Secondary Oscillator Disable */
#pragma config UPLLEN = OFF     /* USB PLL OFF */
#pragma config OSCIOFNC = OFF   /* CLKO output signal Disable */
#pragma config FCKSM = CSDCMD   /* Clock Switching & Fail Safe Clock Monitor */
#pragma config IESO = ON        /* Internal/External Switch-over */

#pragma config FCANIO = OFF     /* Standard/alternate CAN pin select (OFF=Alt) */
#pragma config FMIIEN = OFF     /* Standard/alternate ETH pin select (OFF=Alt) */
#pragma config FETHIO = OFF     /* MII/RMII select (OFF=RMII) */

#pragma config CP = OFF         /* Code protection OFF */
#pragma config BWP = OFF        /* Boot flash write protection OFF */
#pragma config PWP = OFF        /* Programm flash write protection OFF */

#pragma config FSRSSEL = PRIORITY_7 /* SRS interrupt priority */

#pragma config ICESEL = ICS_PGx1 /* ICE EMUC1/EMUD1 pins shared with PGC1/PGD1 */
#pragma config DEBUG = OFF       /* Background debug */

#define SYS_FREQ         (80000000L)

/*
 * Set up the hardware ready to run this demo.
 */
static void prvSetupHardware( void );

/*
 * Live blink task
 */
void vLiveBlink(void *pvParameters)
{
    for(;;)
    {
        LED_LIVE_TOG;
        vTaskDelay(500/portTICK_RATE_MS);
    }
    vTaskDelete(NULL);
}

/*
 * 
 */
int main(void) {

    prvSetupHardware();

    /* Start the scheduler. */
    vTaskStartScheduler();

    for( ;; );

    return (EXIT_SUCCESS);
}

/*-----------------------------------------------------------*/

static void prvSetupHardware( void )
{
	/* Configure the hardware for maximum performance. */
	vHardwareConfigurePerformance();

	/* Setup to use the external interrupt controller. */
	vHardwareUseMultiVectoredInterrupts();

/*	portDISABLE_INTERRUPTS(); */

        /* Configure LEDs */
        LED_SET_OUTPUT;
        LED_SET_ODC;
        LED_SET_DEFAULT;

}
/*-----------------------------------------------------------*/

void vApplicationMallocFailedHook( void )
{
	/* vApplicationMallocFailedHook() will only be called if
	configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h.  It is a hook
	function that will get called if a call to pvPortMalloc() fails.
	pvPortMalloc() is called internally by the kernel whenever a task, queue,
	timer or semaphore is created.  It is also called by various parts of the
	demo application.  If heap_1.c or heap_2.c are used, then the size of the
	heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
	FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
	to query the size of free heap space that remains (although it does not
	provide information on how the remaining heap might be fragmented). */
	taskDISABLE_INTERRUPTS();
	for( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationIdleHook( void )
{
	/* vApplicationIdleHook() will only be called if configUSE_IDLE_HOOK is set
	to 1 in FreeRTOSConfig.h.  It will be called on each iteration of the idle
	task.  It is essential that code added to this hook function never attempts
	to block in any way (for example, call xQueueReceive() with a block time
	specified, or call vTaskDelay()).  If the application makes use of the
	vTaskDelete() API function (as this demo application does) then it is also
	important that vApplicationIdleHook() is permitted to return to its calling
	function, because it is the responsibility of the idle task to clean up
	memory allocated by the kernel to any task that has since been deleted. */
}
/*-----------------------------------------------------------*/

void vApplicationStackOverflowHook( xTaskHandle pxTask, signed char *pcTaskName )
{
	( void ) pcTaskName;
	( void ) pxTask;

	/* Run time task stack overflow checking is performed if
	configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook	function is
	called if a task stack overflow is detected.  Note the system/interrupt
	stack is not checked. */
	taskDISABLE_INTERRUPTS();
	for( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationTickHook( void )
{
	/* This function will be called by each tick interrupt if
	configUSE_TICK_HOOK is set to 1 in FreeRTOSConfig.h.  User code can be
	added here, but the tick hook is called from an interrupt context, so
	code must not attempt to block, and only the interrupt safe FreeRTOS API
	functions can be used (those that end in FromISR()). */
}
/*-----------------------------------------------------------*/

void _general_exception_handler( unsigned long ulCause, unsigned long ulStatus )
{
	/* This overrides the definition provided by the kernel.  Other exceptions
	should be handled here. */
	for( ;; );
}

