/*
 * File:   range_meters.c
 *
 * Autopilot for UAV
 *
 * Some functions for work with the UltraSonic and InfraRed range meters
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <p32xxxx.h>
#include "range_meters.h"
/* Project setting includes */
#include "projdefs.h"

struct stRangeMetersBottom stRangeMeterBot;

void vRangeMetersInit(void)
{
    stRangeMeterBot.IR_near = 0;
    stRangeMeterBot.IR_long = 0;
    stRangeMeterBot.US_long = 0;
    stRangeMeterBot.Result = 0;
    stRangeMeterBot.b_flags.ALL_flags = 0;
}

/*
 * Calculate distance on mm after ADC measurement
 * function only for Sharp GP2D120X IR range meters
 */
unsigned int uiRange_MetersCalcIRDistance2D120(unsigned int uiData)
{
    if ((uiData<=930)&&(uiData>=620))
    {
        return (unsigned int)(60 - 0.080645*(uiData - 620));
    }
    else if ((uiData<=620)&&(uiData>=390))
    {
        return (unsigned int)(100 - 0.1739*(uiData - 390));
    }
    else if ((uiData<=390)&&(uiData>=288))
    {
        return (unsigned int)(140 - 0.3922*(uiData - 288));
    }
    else if ((uiData<=288)&&(uiData>=204))
    {
        return (unsigned int)(200 - 0.7144*(uiData - 204));
    }
    else if ((uiData<=204)&&(uiData>=133))
    {
        return (unsigned int)(300 - 1.4*(uiData - 133));
    }
    else if ((uiData<=133)&&(uiData>=98))
    {
        return (unsigned int)(400 - 2.857*(uiData - 98));
    }
    else
    {
        return 0;
    }
}

/*
 * Calculate distance on mm after UltraSonic measure
 * function only for HC-SR04 US range meters
 */
unsigned int uiRange_MetersCalcUSDistanceHCSR04(unsigned int uiData)
{
    return (unsigned int)(uiData * US_DISTANCE_HCSR04_K);
}
