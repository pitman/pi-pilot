/*
 * File:   autopilot.h
 *
 * Autopilot for UAV
 *
 * The header file for autopilot subsystem.
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */
#ifndef AUTOPILOT_H
#define	AUTOPILOT_H

#ifdef	__cplusplus
extern "C" {
#endif

typedef enum {
    /* Do nothink */
    AUTOPILOT_SIG_NONE,
    /* Signal for set elerons and other to null point */
    AUTOPILOT_SIG_NULL,
    /* Signal for recalc control PWM from fresh Euler angles */
    AUTOPILOT_SIG_EULER_REFRESH,
    /* Signal for recalc control PWM from fresh altitude data */
    AUTOPILOT_SIG_BAR_ALT,
} AUTOPILOT_SIG_TYPE;

typedef struct {
    float  roll;
    float pitch;
    float   yaw;
    float   alt;
} AUTOPILOT_COURSE_STRUCT;

#define AUTOPILOT_SIG_DEEP 8

/* Defines for non-motor planer */
#define AUTOPILOT_PLANER_AILERON_LEFT  0
#define AUTOPILOT_PLANER_AILERON_RIGHT 2
#define AUTOPILOT_PLANER_ELEVATOR      1
#define AUTOPILOT_PLANER_RUDDER        3

/* Autopilot for non motor planer */
void vAUTOPILOT_PLANER(void *pvParameters);

#ifdef	__cplusplus
}
#endif

#endif	/* AUTOPILOT_H */

