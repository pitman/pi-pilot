/*
 * File:   main.c
 *
 * Autopilot for UAV
 *
 * Main file
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <p32xxxx.h>
#include <xc.h>
#include <sys/attribs.h>
#include <math.h>

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "croutine.h"
#include "timers.h"
#include "queue.h"

/* Hardware specific includes. */
#include "ConfigPerformance.h"
#include "i2c.h"
#include "spi.h"
#include "usb.h"
#include "usb_function_cdc.h"
#include "peripheral/uart.h"
#include "range_meters.h"
#include "pid.h"
#include "bmp085.h"
#include "lsm303dlhc.h"
#include "l3g4200d.h"
#include "pwm-driver.h"
#include "ppm-driver.h"

/* Project setting includes */
#include "projdefs.h"
#include "config.h"
#include "sensor_pressure.h"
#include "sensor_mems.h"
#include "console.h"
#include "logd.h"
#include "sens.h"
#include "autopilot.h"
#include "rf-link.h"
#include "mavlink/v1.0/common/mavlink.h"

/* Core configuratin fuse settings */

#pragma config FPLLIDIV = DIV_2 /* External crystal is 8MHz, divide it by 2 */
#pragma config FPLLMUL = MUL_20 /* Set Freq multipler to 20, core Freq 80MHz */
#pragma config FPLLODIV = DIV_1 /* Didn't divide core Freq after multiplier */
#pragma config FWDTEN = OFF     /* Disable Watchdog */
#pragma config POSCMOD = HS     /* External high-speed crystal select */
#pragma config FNOSC = PRIPLL   /* Enable PLL for Freq from HS crystal */
#pragma config FPBDIV = DIV_2   /* Set divider for peripherial bus clock to 2 */
#pragma config FSOSCEN = OFF    /* Secondary Oscillator Disable */
#pragma config UPLLEN = ON      /* USB PLL ON */
#pragma config UPLLIDIV = DIV_2 /* Divide 8MHz input to 4Mhz befor USB PLL */
#pragma config OSCIOFNC = OFF   /* CLKO output signal Disable */
#pragma config FCKSM = CSDCMD   /* Clock Switching & Fail Safe Clock Monitor */
#pragma config IESO = ON        /* Internal/External Switch-over */

#pragma config FCANIO = OFF     /* Standard/alternate CAN pin select (OFF=Alt) */
#pragma config FMIIEN = OFF     /* Standard/alternate ETH pin select (OFF=Alt) */
#pragma config FETHIO = OFF     /* MII/RMII select (OFF=RMII) */

#pragma config CP = OFF         /* Code protection OFF */
#pragma config BWP = OFF        /* Boot flash write protection OFF */
#pragma config PWP = OFF        /* Programm flash write protection OFF */

#pragma config FSRSSEL = PRIORITY_7 /* SRS interrupt priority */

#pragma config ICESEL = ICS_PGx1 /* ICE EMUC1/EMUD1 pins shared with PGC1/PGD1 */
#pragma config DEBUG = OFF       /* Background debug */

/*
 * Set up the hardware ready to run this demo.
 */
static void prvSetupHardware( void );

/* Autopilot for non motor planer */
extern void vAUTOPILOT_PLANER(void *pvParameters);

/* Just for test */
char cBuff[CONSOLE_BUFFER_SIZE];

/* Queue for LEDs */
volatile xQueueHandle xQueueLEDs;

/* Queue for configure some subsystems */
volatile xQueueHandle xQueueConfd;

/* Queue for message system */
volatile xQueueHandle xQueueMessage;
char prvcMessageBuffer[LOGD_MESSAGE_CHAR_BUFFER];
volatile unsigned char ucMainMessageLevel = LOGD_ERROR;

/* Queue for data from an I2C1 sensors */
volatile xQueueHandle xQueueI2C1Sensors;
/* Queue for read/write data to I2C */
volatile xQueueHandle xQueueI2CRWPKT;
/* Queue for result of read/write data to I2C */
volatile xQueueHandle xQueueI2CRWResult;
#ifdef DEBUG_I2C_BUFFER_ENABLE
/* Debug I2C bus data structure */
volatile I2C_DRIVER_DEBUG i2c_dbg[I2C_DRIVER_DEBUG_DATA_LENGHT];
volatile unsigned int i2c_dbg_cnt = 0;
#endif

/* Queue for the signals to FLY main FSM */
volatile xQueueHandle xQueueFLYSIG;

/* Queue for the signals to AUTOPILOT code */
volatile xQueueHandle xQueueAUTOPILOTSIG;

/* Queue for send decoded PPM value to main system */
volatile xQueueHandle xQueuePPMDECODER;

/* Queue for TX data to ground */
volatile xQueueHandle xQueueRFLINKTX;
/* Queue for RX data from a ground */
volatile xQueueHandle xQueueRFLINKRX;

/* Local time with 1ms resolution */
volatile unsigned long ulLOCALTIME = 0;

/* IDLE free resource counter */
volatile PROJDEFS_IDLE_STAT stats;

/* Main data structure of Pi-Pilot */
volatile PROJDEFS_PIPILOT pilot;

/* Configure data structure of Pi-Pilot */
volatile PROJDEFS_PIPILOT_CFG pilot_cfg;

static BYTE uPROJDEFS_LedSignals[4][19] =
{
    {0x42, 0x85, 0x05, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0x52, 0x81, 0x01, 0x81, 0x01, 0x81, 0x03, 0x83, 0x01, 0x83, 0x01, 0x83,
            0x03, 0x81, 0x01, 0x81, 0x01, 0x81, 0x1E},
    {0x01, 0x82, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0x42, 0x81, 0x09, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
};

/* Main task for LEDs */
void prvLEDSd(void *pvParameters)
{
    static portTickType xLastWakeTime;
    static PROJDEFS_LED_STRUCT leds[4]  = {0, 0, 0, 0};
    static BYTE k, led_idx;
    static PROJDEFS_LED_QUEUE led_cmd;
    static portBASE_TYPE xStatus;
    const char bug_type[] = "prvLEDSd led type\r\n";

    xLastWakeTime = xTaskGetTickCount();

    for(;;)
    {
        if (uxQueueMessagesWaiting(xQueueLEDs) != 0)
        {
            /* Receive message from Queue */
            xStatus = xQueuePeek(xQueueLEDs, &led_cmd, 0);

            if (xStatus == pdPASS)
            {
                if ((leds[led_cmd.led_num].fsm == 0) &&
                        (leds[led_cmd.led_num].cnt == 0))
                {
                    /* receive new led data */
                    xQueueReceive(xQueueLEDs, &led_cmd, 0);
                    /* clear old data */
                    leds[led_cmd.led_num].all_data = 0x0000;
                    /* storage new data */
                    leds[led_cmd.led_num].on = led_cmd.on;
                    leds[led_cmd.led_num].type = led_cmd.type;
                }
                else
                {
                    leds[led_cmd.led_num].refresh = 1;
                }
            }
        }

        for (k = 0; k < 4; k++)
        {
            if ((leds[k].type > 0) || (leds[k].on))
            {
                if (leds[k].on)
                {
                    /* Switch ON led all time */
                    LATGCLR = 1 << (k + 12);
                }
                else
                {
                    /* Blink some signals */
                    led_idx = 0xFF;
                    switch (leds[k].type)
                    {
                        case PROJDEFS_LED_PULSE:
                            led_idx = 0;
                            break;
                        case PROJDEFS_LED_SOS:
                            led_idx = 1;
                            break;
                        case PROJDEFS_LED_BLINK:
                            led_idx = 2;
                            break;
                        case PROJDEFS_LED_0_1:
                            led_idx = 3;
                            break;
                        default:
                            /* Undefined signal - send message and switch OFF a LED */
                            vLogdPrintROM(LOGD_BUG, bug_type, 0);
                            LATGSET = 1 << (k + 12);
                            break;
                    }

                    if (led_idx != 0xFF)
                    {
                        if (leds[k].cnt >= (uPROJDEFS_LedSignals[led_idx][leds[k].fsm + 1]&0x3F))
                        {
                            leds[k].cnt = 0;
                            
                            if (leds[k].fsm >= (uPROJDEFS_LedSignals[led_idx][0]&0x3F))
                            {
                                leds[k].fsm = 0;

                                if ((uPROJDEFS_LedSignals[led_idx][0]&0x40) &&
                                        (leds[k].refresh == 0))
                                {
                                    /* Repeate the signal */
                                    if (uPROJDEFS_LedSignals[led_idx][leds[k].fsm + 1]&0x80)
                                        /* switch a led ON */
                                        LATGCLR = 1 << (k + 12);
                                    else
                                        /* switch a led OFF */
                                        LATGSET = 1 << (k + 12);
                                }
                                else
                                {
                                    leds[k].all_data = 0x00;
                                    /* switch a led OFF */
                                    LATGSET = 1 << (k + 12);
                                }
                            }
                            else
                            {
                                leds[k].fsm++;

                                if (uPROJDEFS_LedSignals[led_idx][leds[k].fsm + 1]&0x80)
                                { /* switch a led ON */
                                    LATGCLR = 1 << (k + 12);


                                }
                                else
                                    /* switch a led OFF */
                                    LATGSET = 1 << (k + 12);
                            }
                        }
                        else
                        {
                            leds[k].cnt++;

                            if (uPROJDEFS_LedSignals[led_idx][leds[k].fsm + 1]&0x80)
                                /* switch a led ON */
                                LATGCLR = 1 << (k + 12);
                            else
                                /* switch a led OFF */
                                LATGSET = 1 << (k + 12);
                        }
                    }
                }
            }
            else
            {
                /* Switch OFF led all time */
                LATGSET = 1 << (k + 12);
            }
        }

        vTaskDelayUntil( &xLastWakeTime, ( 100 / portTICK_RATE_MS ) );
    }
    vTaskDelete(NULL);
}

/* Service task for configure some parts. */
void prvCONFd(void *pvParameters)
{
    static PROJDEFS_CONF_ENUM conf_cmd;
    static I2C_DRIVER_RW_PKT i2c_cmd;
    static I2C_DRIVER_RW_RESULT i2c_res;
    static char cBuff[LOGD_MESSAGE_CHAR_BUFFER];

    for(;;)
    {
        /* receive new configure message */
        xQueueReceive(xQueueConfd, &conf_cmd, portMAX_DELAY);
        switch (conf_cmd)
        {
            case PROJDEFS_CONF_I2C1_PON:
                vProjdefsFiniI2C1Power();
                vTaskDelay(1/portTICK_RATE_MS);
                vProjdefsInitI2C1Power();
                pilot.i2c1.pwr = I2C_PWR_ON;
                sprintf(cBuff, "prvCONFd I2C1 PON\r\n");
                vLogdPrint(LOGD_DEBUG, cBuff, 10);
                break;
            case PROJDEFS_CONF_I2C1_POFF:
                vProjdefsFiniI2C1Power();
                pilot.i2c1.pwr = I2C_PWR_OFF;
                sprintf(cBuff, "prvCONFd I2C1 POFF\r\n");
                vLogdPrint(LOGD_DEBUG, cBuff, 10);
                break;
            case PROJDEFS_CONF_I2C1_PRST:
                vProjdefsReInitI2C1Power();
                pilot.i2c1.pwr = I2C_PWR_ON;
                sprintf(cBuff, "prvCONFd I2C1 RST\r\n");
                vLogdPrint(LOGD_DEBUG, cBuff, 10);
                break;
            case PROJDEFS_CONF_I2C1_INIT:
                if (pilot.i2c1.pwr == I2C_PWR_ON)
                {
                    I2CEnable(I2C1, FALSE);
                    vTaskDelay(1/portTICK_RATE_MS);
                    I2CConfigure(I2C1, I2C_ENABLE_SLAVE_CLOCK_STRETCHING|I2C_ENABLE_HIGH_SPEED);
                    I2C1CONbits.DISSLW = 0;
                    if (pilot.i2c1.freq == 0)
                        I2CSetFrequency(I2C1, configPERIPHERAL_CLOCK_HZ, I2C1_CLOCK_FREQ);
                    else
                        I2CSetFrequency(I2C1, configPERIPHERAL_CLOCK_HZ, pilot.i2c1.freq);
                    INTDisableInterrupts();
                    /* Configure I2C1 IRQ level */
                    INTSetVectorPriority(INT_I2C_1_VECTOR, INT_PRIORITY_LEVEL_2);
                    INTSetVectorSubPriority(INT_I2C_1_VECTOR, INT_SUB_PRIORITY_LEVEL_0);
                    /* Clear I2C1 master interrupt */
                    INTClearFlag(INT_I2C1);
                    /* Enable I2C1 master */
                    I2CEnable(I2C1, TRUE);
                    /* Enable I2C1 master interrupt */
                    INTEnable(INT_I2C1, INT_ENABLED);
                    INTEnableInterrupts();                    

                    pilot.i2c1.state = I2C_BUS_IDLE;
                    pilot.i2c1.pkt = I2C_PKT_NONE;
                    sprintf(cBuff, "prvCONFd I2C1 INI\r\n");
                    vLogdPrint(LOGD_DEBUG, cBuff, 10);
                }
                else
                {
                    sprintf(cBuff, "prvCONFd I2C1 INI ERR\r\n");
                    vLogdPrint(LOGD_ERROR, cBuff, 10);
                }
                break;
            case PROJDEFS_CONF_I2C1_FINI:
                /* Disable I2C1 master interrupt */
                INTEnable(INT_I2C1, INT_DISABLED);
                I2CEnable(I2C1, FALSE);
                pilot.i2c1.state = I2C_BUS_NONE;
                pilot.i2c1.pkt = I2C_PKT_NONE;
                sprintf(cBuff, "prvCONFd I2C1 FIN\r\n");
                vLogdPrint(LOGD_DEBUG, cBuff, 10);
                break;
            case PROJDEFS_CONF_L3G4200D:
                /* Try read ID from the HYR MEMS sensor */
                i2c_cmd.port = I2C1;
                i2c_cmd.addr = L3G4200D_HYR_I2C_ADDR;
                i2c_cmd.rw_fl = 0;
                i2c_cmd.reg = L3G4200D_HYR_ID_REG;
                i2c_cmd.data_cnt = 1;
                i2c_cmd.uid = ulLOCALTIME;
                xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                vProjdefsI2C1Start();
                xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                if ((i2c_res.uid == i2c_cmd.uid) && (i2c_res.result == I2C_OP_READ_OK))
                {
                    if (i2c_res.data[0] == L3G4200D_HYR_ID)
                    {
                        sprintf(cBuff, "prvCONFd L3G4200D INI\r\n");
                        vLogdPrint(LOGD_DEBUG, cBuff, 10);
                        /* Configure the L3G4200D sensor */
                        i2c_cmd.port = I2C1;
                        i2c_cmd.addr = L3G4200D_HYR_I2C_ADDR;
                        i2c_cmd.rw_fl = 1;
                        i2c_cmd.reg = L3G4200D_HYR_CTRL_REG1;
                        i2c_cmd.data_cnt = 1;
                        i2c_cmd.data[0] = ((pilot.hyr.data_rate << 6) |
                            (pilot.hyr.bandwidth << 4) | L3G4200D_HYR_AXIS_EN);
                        i2c_cmd.uid = ulLOCALTIME;
                        xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                        vProjdefsI2C1Start();
                        xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                        if (i2c_res.result != I2C_OP_WRITE_OK)
                        {
                            break;
                        }
                        i2c_cmd.reg = L3G4200D_HYR_CTRL_REG4;
                        i2c_cmd.data[0] = (0xC0 | (pilot.hyr.scale << 4));
                        i2c_cmd.uid = ulLOCALTIME;
                        xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                        vProjdefsI2C1Start();
                        xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                        if (i2c_res.result != I2C_OP_WRITE_OK)
                        {
                            break;
                        }
                        pilot.hyr.state = L3G4200D_READY;
                        pilot.sensors_enabled |= MAV_SYS_STATUS_SENSOR_3D_GYRO;
                        pilot.sensors_health |= MAV_SYS_STATUS_SENSOR_3D_GYRO;
                    }
                    else
                    {
                        sprintf(cBuff, "prvCONFd L3G4200D NOT FOUND\r\n");
                        vLogdPrint(LOGD_ERROR, cBuff, 10);
                    }
                }
                break;
            case PROJDEFS_CONF_LSM303DLHC:
                /* LSM303DLHC Acceletometer */
                i2c_cmd.port = I2C1;
                i2c_cmd.addr = LSM303DLHC_ACC_I2C_ADDR;
                i2c_cmd.rw_fl = 1;
                /* For test configure data rate */
                i2c_cmd.data[0] = ((pilot.acc_mag.acc_rate << 4) |
                        LSM303DLHC_ACC_AXIS_EN);
                i2c_cmd.reg = LSM303DLHC_ACC_CTRL_REG_1;
                i2c_cmd.data_cnt = 1;
                i2c_cmd.uid = ulLOCALTIME;
                xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                vProjdefsI2C1Start();
                xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                if ((i2c_res.result == I2C_OP_WRITE_OK) &&
                        (i2c_res.uid == i2c_cmd.uid))
                {
                    sprintf(cBuff, "prvCONFd LSM303DLHC ACC INI\r\n");
                    vLogdPrint(LOGD_DEBUG, cBuff, 10);
                    /* Write data is OK */
                    /* Configure scale */
                    i2c_cmd.data[0] = 0x00;
                    i2c_cmd.uid = ulLOCALTIME;
                    i2c_cmd.reg = LSM303DLHC_ACC_CTRL_REG_2;
                    xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                    vProjdefsI2C1Start();
                    xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                    if (i2c_res.result != I2C_OP_WRITE_OK)
                    {
                        break;
                    }
                    /* Configure scale */
                    i2c_cmd.data[0] = (0xC0 | (pilot.acc_mag.acc_scale << 4) | 0x08);
                    i2c_cmd.uid = ulLOCALTIME;
                    i2c_cmd.reg = LSM303DLHC_ACC_CTRL_REG_4;
                    xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                    vProjdefsI2C1Start();
                    xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                    if (i2c_res.result != I2C_OP_WRITE_OK)
                    {
                        break;
                    }
                    pilot.sensors_enabled |= MAV_SYS_STATUS_SENSOR_3D_ACCEL;
                    pilot.sensors_health |= MAV_SYS_STATUS_SENSOR_3D_ACCEL;
                }
                else
                {
                    sprintf(cBuff, "prvCONFd LSM303DLHC ACC NOT FOUND\r\n");
                    vLogdPrint(LOGD_ERROR, cBuff, 10);
                }
                /* LSM303DLHC Magnetometer */
                i2c_cmd.port = I2C1;
                i2c_cmd.addr = LSM303DLHC_MAG_I2C_ADDR;
                i2c_cmd.rw_fl = 1;
                i2c_cmd.reg = LSM303DLHC_MAG_CRA_REG;
                i2c_cmd.data_cnt = 1;
                i2c_cmd.data[0] = (0x80 | (pilot.acc_mag.mag_rate << 2));
                i2c_cmd.uid = ulLOCALTIME;
                xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                vProjdefsI2C1Start();
                xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                if ((i2c_res.result == I2C_OP_WRITE_OK) && (i2c_res.uid == i2c_cmd.uid))
                {
                    sprintf(cBuff, "prvCONFd LSM303DLHC MAG INI\r\n");
                    vLogdPrint(LOGD_DEBUG, cBuff, 10);
                    /* Write data is OK */
                    /* Configure GAIN */
                    i2c_cmd.data[0] = pilot.acc_mag.mag_gain << 5;
                    i2c_cmd.uid = ulLOCALTIME;
                    i2c_cmd.reg = LSM303DLHC_MAG_CRB_REG;
                    xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                    vProjdefsI2C1Start();
                    xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                    if (i2c_res.result != I2C_OP_WRITE_OK)
                    {
                        break;
                    }
                    i2c_cmd.data[0] = 0x00;
                    i2c_cmd.uid = ulLOCALTIME;
                    i2c_cmd.reg = LSM303DLHC_MAG_MR_REG;
                    xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                    vProjdefsI2C1Start();
                    xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                    if (i2c_res.result != I2C_OP_WRITE_OK)
                    {
                        break;
                    }
                    pilot.acc_mag.state = LSM303DLHC_READY;
                    pilot.sensors_enabled |= MAV_SYS_STATUS_SENSOR_3D_MAG;
                    pilot.sensors_health |= MAV_SYS_STATUS_SENSOR_3D_MAG;
                }
                else
                {
                    sprintf(cBuff, "prvCONFd LSM303DLHC MAG NOT FOUND\r\n");
                    vLogdPrint(LOGD_ERROR, cBuff, 10);
                }
                break;
            case PROJDEFS_CONF_BMP085:
                /* Read calibration data from internal ROM of the sensor */
                /* Try read ID from the HYR MEMS sensor */
                i2c_cmd.port = I2C1;
                i2c_cmd.addr = BMP085_I2C_ADDR;
                i2c_cmd.rw_fl = 0;
                i2c_cmd.reg = BMP085_REG_ID;
                i2c_cmd.data_cnt = 1;
                i2c_cmd.uid = ulLOCALTIME;
                xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                vProjdefsI2C1Start();
                xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                if ((i2c_res.uid == i2c_cmd.uid) && (i2c_res.result == I2C_OP_READ_OK))
                {
                    if (i2c_res.data[0] == BMP085_CHIP_ID)
                    {
                        sprintf(cBuff, "prvCONFd BMP085 INI\r\n");
                        vLogdPrint(LOGD_DEBUG, cBuff, 10);
                        i2c_cmd.port = I2C1;
                        i2c_cmd.addr = BMP085_I2C_ADDR;
                        i2c_cmd.rw_fl = 0;
                        i2c_cmd.reg = BMP085_REG_AC1;
                        i2c_cmd.data_cnt = 2;
                        i2c_cmd.uid = ulLOCALTIME;
                        xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                        vProjdefsI2C1Start();
                        xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                        if (i2c_res.result != I2C_OP_READ_OK)
                        {
                            break;
                        }
                        pilot.bar.ac1 = ((i2c_res.data[0] << 8) | i2c_res.data[1]);
                        pilot.bar.ac1m = pilot.bar.ac1 * 4;

                        i2c_cmd.port = I2C1;
                        i2c_cmd.addr = BMP085_I2C_ADDR;
                        i2c_cmd.rw_fl = 0;
                        i2c_cmd.reg = BMP085_REG_AC2;
                        i2c_cmd.data_cnt = 2;
                        i2c_cmd.uid = ulLOCALTIME;
                        xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                        vProjdefsI2C1Start();
                        xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                        if (i2c_res.result != I2C_OP_READ_OK)
                        {
                            break;
                        }
                        pilot.bar.ac2 = ((i2c_res.data[0] << 8) | i2c_res.data[1]);

                        i2c_cmd.port = I2C1;
                        i2c_cmd.addr = BMP085_I2C_ADDR;
                        i2c_cmd.rw_fl = 0;
                        i2c_cmd.reg = BMP085_REG_AC3;
                        i2c_cmd.data_cnt = 2;
                        i2c_cmd.uid = ulLOCALTIME;
                        xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                        vProjdefsI2C1Start();
                        xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                        if (i2c_res.result != I2C_OP_READ_OK)
                        {
                            break;
                        }
                        pilot.bar.ac3 = ((i2c_res.data[0] << 8) | i2c_res.data[1]);

                        i2c_cmd.port = I2C1;
                        i2c_cmd.addr = BMP085_I2C_ADDR;
                        i2c_cmd.rw_fl = 0;
                        i2c_cmd.reg = BMP085_REG_AC4;
                        i2c_cmd.data_cnt = 2;
                        i2c_cmd.uid = ulLOCALTIME;
                        xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                        vProjdefsI2C1Start();
                        xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                        if (i2c_res.result != I2C_OP_READ_OK)
                        {
                            break;
                        }
                        pilot.bar.ac4 = ((i2c_res.data[0] << 8) | i2c_res.data[1]);

                        i2c_cmd.port = I2C1;
                        i2c_cmd.addr = BMP085_I2C_ADDR;
                        i2c_cmd.rw_fl = 0;
                        i2c_cmd.reg = BMP085_REG_AC5;
                        i2c_cmd.data_cnt = 2;
                        i2c_cmd.uid = ulLOCALTIME;
                        xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                        vProjdefsI2C1Start();
                        xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                        if (i2c_res.result != I2C_OP_READ_OK)
                        {
                            break;
                        }
                        pilot.bar.ac5 = ((i2c_res.data[0] << 8) | i2c_res.data[1]);

                        i2c_cmd.port = I2C1;
                        i2c_cmd.addr = BMP085_I2C_ADDR;
                        i2c_cmd.rw_fl = 0;
                        i2c_cmd.reg = BMP085_REG_AC6;
                        i2c_cmd.data_cnt = 2;
                        i2c_cmd.uid = ulLOCALTIME;
                        xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                        vProjdefsI2C1Start();
                        xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                        if (i2c_res.result != I2C_OP_READ_OK)
                        {
                            break;
                        }
                        pilot.bar.ac6 = ((i2c_res.data[0] << 8) | i2c_res.data[1]);

                        i2c_cmd.port = I2C1;
                        i2c_cmd.addr = BMP085_I2C_ADDR;
                        i2c_cmd.rw_fl = 0;
                        i2c_cmd.reg = BMP085_REG_B1;
                        i2c_cmd.data_cnt = 2;
                        i2c_cmd.uid = ulLOCALTIME;
                        xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                        vProjdefsI2C1Start();
                        xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                        if (i2c_res.result != I2C_OP_READ_OK)
                        {
                            break;
                        }
                        pilot.bar.b1 = ((i2c_res.data[0] << 8) | i2c_res.data[1]);

                        i2c_cmd.port = I2C1;
                        i2c_cmd.addr = BMP085_I2C_ADDR;
                        i2c_cmd.rw_fl = 0;
                        i2c_cmd.reg = BMP085_REG_B2;
                        i2c_cmd.data_cnt = 2;
                        i2c_cmd.uid = ulLOCALTIME;
                        xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                        vProjdefsI2C1Start();
                        xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                        if (i2c_res.result != I2C_OP_READ_OK)
                        {
                            break;
                        }
                        pilot.bar.b2 = ((i2c_res.data[0] << 8) | i2c_res.data[1]);

                        i2c_cmd.port = I2C1;
                        i2c_cmd.addr = BMP085_I2C_ADDR;
                        i2c_cmd.rw_fl = 0;
                        i2c_cmd.reg = BMP085_REG_MB;
                        i2c_cmd.data_cnt = 2;
                        i2c_cmd.uid = ulLOCALTIME;
                        xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                        vProjdefsI2C1Start();
                        xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                        if (i2c_res.result != I2C_OP_READ_OK)
                        {
                            break;
                        }
                        pilot.bar.mb = ((i2c_res.data[0] << 8) | i2c_res.data[1]);

                        i2c_cmd.port = I2C1;
                        i2c_cmd.addr = BMP085_I2C_ADDR;
                        i2c_cmd.rw_fl = 0;
                        i2c_cmd.reg = BMP085_REG_MC;
                        i2c_cmd.data_cnt = 2;
                        i2c_cmd.uid = ulLOCALTIME;
                        xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                        vProjdefsI2C1Start();
                        xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                        if (i2c_res.result != I2C_OP_READ_OK)
                        {
                            break;
                        }
                        pilot.bar.mc = ((i2c_res.data[0] << 8) | i2c_res.data[1]);
                        pilot.bar.mcm = pilot.bar.mc * 2048;

                        i2c_cmd.port = I2C1;
                        i2c_cmd.addr = BMP085_I2C_ADDR;
                        i2c_cmd.rw_fl = 0;
                        i2c_cmd.reg = BMP085_REG_MD;
                        i2c_cmd.data_cnt = 2;
                        i2c_cmd.uid = ulLOCALTIME;
                        xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                        vProjdefsI2C1Start();
                        xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                        if (i2c_res.result != I2C_OP_READ_OK)
                        {
                            break;
                        }
                        pilot.bar.md = ((i2c_res.data[0] << 8) | i2c_res.data[1]);

                        pilot.bar.state = BMP085_READY;
                        pilot.sensors_enabled |= MAV_SYS_STATUS_SENSOR_ABSOLUTE_PRESSURE;
                        pilot.sensors_health |= MAV_SYS_STATUS_SENSOR_ABSOLUTE_PRESSURE;
                    }
                    else
                    {
                        sprintf(cBuff, "prvCONFd BMP085 NOT FOUND\r\n");
                        vLogdPrint(LOGD_ERROR, cBuff, 10);
                    }
                }
                break;
            /* Configure PWM */
            case PROJDEFS_CONF_HPWM:
                sprintf(cBuff, "prvCONFd HPWM INI\r\n");
                vLogdPrint(LOGD_DEBUG, cBuff, 10);
                vPWMDRIVER_INIT_PWM();
                /* Start PWM on 1 sec for set null point */
                vPWMDRIVER_START_PWM();
                vTaskDelay(1000/portTICK_RATE_MS);
                vPWMDRIVER_STOP_PWM();
                pilot.sensors_enabled |= MAV_SYS_STATUS_SENSOR_MOTOR_OUTPUTS;
                pilot.sensors_health |= MAV_SYS_STATUS_SENSOR_MOTOR_OUTPUTS;
                break;
            /* Read configure data from a SD card */
            case PROJDEFS_CONF_READCFG:
                sprintf(cBuff, "prvCONFd READ CFG\r\n");
                vLogdPrint(LOGD_DEBUG, cBuff, 10);
                vCONFIG_READ_DATA(&pilot_cfg);
                break;
            /* Configure main pilot structure from a reading data */
            case PROJDEFS_CONF_PILOT:
                sprintf(cBuff, "prvCONFd PILOT INIT\r\n");
                vLogdPrint(LOGD_DEBUG, cBuff, 10);
                vProjdefsConfigurePilot(&pilot_cfg, &pilot);
                break;
            case PROJDEFS_CONF_PPM_DECODER:
                sprintf(cBuff, "prvCONFd PPM INIT\r\n");
                vLogdPrint(LOGD_DEBUG, cBuff, 10);
                if (pilot.rc.rc_type == RC_TYPE_PPM8)
                {
                    vPPM_DRIVER_INIT();
                    pilot.sensors_enabled |= MAV_SYS_STATUS_SENSOR_RC_RECEIVER;
                    pilot.sensors_health |= MAV_SYS_STATUS_SENSOR_RC_RECEIVER;
                }
                break;
            case PROJDEFS_CONF_RFLINK:
                /* Configure UART */
                vProjdefsConfigureUART(UART_RFLINK);
                break;
            default:
                /* Undefined configuration message. Log it */
                sprintf(cBuff, "prvCONFd conf UNKNOWN CMD\r\n");
                vLogdPrint(LOGD_BUG, cBuff, 10);
                break;
        }
    }
    vTaskDelete(NULL);
}

/* Task for message system */
void prvMESSAGEd(void *pvParameters)
{
    LOGD_MESSAGE_STRUCT logmsg;
    int char_pos;

    for(;;)
    {
        if((USBDeviceState >= CONFIGURED_STATE)&&(USBSuspendControl!=1))
        {
            xQueueReceive(xQueueMessage, &logmsg, portMAX_DELAY);
            char_pos = sprintf(prvcMessageBuffer, "%d:%d:", logmsg.time, logmsg.flag);
            sprintf( &prvcMessageBuffer[char_pos], logmsg.message);
            vConsolePrintData(prvcMessageBuffer);
            /* No more than 100 message per secong */
            vTaskDelay(10/portTICK_RATE_MS);
        }
        else
            vTaskDelay(100/portTICK_RATE_MS);
    }
    vTaskDelete(NULL);
}

/* Task for calculate position */
void prvCALCd(void *pvParameters)
{
    static SENS_I2C_QUEUE_STRUCT sens_data;
    static char cBuff[LOGD_MESSAGE_CHAR_BUFFER];
    static long x1, x2, x3;
    static long b3, b6, b6x2d;
    static unsigned long b4, b7;
    static float pvLSM303DLHC_ACC_t[4] = {0.001, 0.002, 0.004, 0.012};
    static float pvL3G4200D_HYR_t[3] = {0.00875, 0.0175, 0.07};
    static float pvLSM303DLHC_MAG_t[7][2] = {
        {0.000909,   0.00102},
        {0.00116959, 0.0013158},
        {0.0014925,  0.166667},
        {0.0022222,  0.0025},
        {0.0025,     0.0028169},
        {0.00303,    0.00338983},
        {0.0043478,  0.004878},
    };
    static float calc_alt;
    static float tmp_acc_x2,tmp_acc_y2, tmp_acc_z2, tmp_acc_norm;
    static float dT;
    static float roll_prev = 0, pitch_prev = 0, yaw_prev = 0;
    static float tmp_mag_x2, tmp_mag_y2, tmp_roll_rad, tmp_pitch_rad, tmp_mag_norm;
    static PROJDEFS_FLYFSM_SIG_STRUCT prveTOFFSIG;
    static AUTOPILOT_SIG_TYPE prvASIG;
#ifdef PROJDEFS_SENS_FILTER_MADGWICK
#define gyroMeasError 3.14159265358979f * (5.0f / 180.0f) // gyroscope measurement error in rad/s (shown as 5 deg/s)
#define beta sqrt(3.0f / 4.0f) * gyroMeasError // compute beta

    static float w_x, w_y, w_z, a_x, a_y, a_z;
    static float norm;
    static float SEq_1, SEq_2, SEq_3, SEq_4;
    static float SEqDot_omega_1, SEqDot_omega_2, SEqDot_omega_3, SEqDot_omega_4; // quaternion derrivative from gyroscopes elements
    static float f_1, f_2, f_3; // objective function elements
    static float J_11or24, J_12or23, J_13or22, J_14or21, J_32, J_33; // objective function Jacobian elements
    static float SEqHatDot_1, SEqHatDot_2, SEqHatDot_3, SEqHatDot_4; // estimated direction of the gyroscope error
    // Axulirary variables to avoid reapeated calcualtions
    static float halfSEq_1, halfSEq_2, halfSEq_3, halfSEq_4;
    static float twoSEq_1, twoSEq_2, twoSEq_3;
#endif /* PROJDEFS_SENS_FILTER_MADGWICK */

    /* TODO: remove it - Debug Hardware PWM outputs */
    static unsigned char pwm_ch_fl[PWM_DRIVER_HPWM_CNT] = {0, 0, 0, 0};

    for(;;)
    {
        xQueueReceive(xQueueI2C1Sensors, &sens_data, portMAX_DELAY);
        switch (sens_data.sens_type)
        {
            case SENS_BMP085_TEMP:
                pilot.bar.ut = (sens_data.data[0] << 8 | sens_data.data[1]);
                /* Calculate temperature */
                x1 = ((pilot.bar.ut - pilot.bar.ac6) * pilot.bar.ac5) >> 15;
                x2 = pilot.bar.mcm / (x1 + pilot.bar.md);
                pilot.bar.b5 = x1 + x2;
                pilot.bar.t = (pilot.bar.b5 + 8) >> 4;

#if 1
                sprintf(cBuff, "%d:BAR_T:%f\r\n", sens_data.time, ((float)pilot.bar.t / 10.0));
                vLogdPrint(LOGD_INFO, cBuff, 10);
#endif
                break;
            case SENS_BMP085_PRESS:
                pilot.bar.up = (((unsigned long) sens_data.data[0] << 16) |
                        ((unsigned long) sens_data.data[1] << 8) |
                        ((unsigned long) sens_data.data[2])) >> (8 - pilot.bar.oss);
                pilot.bar.mcnt++;
                /* Calculate pressure */
                b6 = pilot.bar.b5 - 4000;
                b6x2d = (b6 * b6) >> 12;
                x1 = (b6x2d * pilot.bar.b2) >> 11;
                x2 = (b6 * pilot.bar.ac2) >> 11;
                x3 = x1 + x2;
                b3 = (((x3 + pilot.bar.ac1m) << pilot.bar.oss) + 2) >> 2;
                x1 = (b6 * pilot.bar.ac3) >> 13;
                x2 = (b6x2d * pilot.bar.b1) >> 16;
                x3 = ((x1 + x2) + 2) >> 2;
                b4 = (unsigned long)((x3 + 32768) * pilot.bar.ac4) >> 15;
                b7 = ((unsigned long)pilot.bar.up - b3) * (50000 >> pilot.bar.oss);
                if (b7 < 0x80000000)
                {
                    pilot.bar.press = (b7 << 1) / b4;
                }
                else
                {
                    pilot.bar.press = (b7 / b4) << 1;
                }
                x1 = (pilot.bar.press >> 8) * (pilot.bar.press >> 8);
                x1 = (x1 * 3038) >> 16;
                x2 = (-7357 * pilot.bar.press) >> 16;
                pilot.bar.press = pilot.bar.press + ((x1 + x2 + 3791) >> 4);

                /* Calculate altitude */
                calc_alt = (float)44330 * (1 - pow(((float)pilot.bar.press / pilot.p0), 0.190295));

                /* Calibration ground altitude */
                if (pilot.fly_state == FF_SENS_COLL)
                {
                    if (pilot.bar.ccnt == 0)
                    {
                        pilot.ground_altitude = calc_alt;
                    }
                    else
                    {
                        pilot.ground_altitude = (pilot.ground_altitude + calc_alt) / 2;
                    }
                    pilot.bar.ccnt += 1;
                }

                /* Calculate altitude offset ground */
                pilot.altitude = calc_alt - pilot.ground_altitude;

                /* Send a signal to autopilot task about new ALT data */
                prvASIG = AUTOPILOT_SIG_BAR_ALT;
                xQueueSend(xQueueAUTOPILOTSIG, &prvASIG, 0);
#if 1
                sprintf(cBuff, "%d:BAR_P:%d:ALT:%f\r\n", sens_data.time, pilot.bar.press, pilot.altitude);
                vLogdPrint(LOGD_INFO, cBuff, 10);
#endif
                break;
            case SENS_LSM303DLHC_ACC:
                pilot.acc_mag.acc_raw_x = ((sens_data.data[0] << 8) | sens_data.data[1]) >> 4;
                pilot.acc_mag.acc_raw_y = ((sens_data.data[2] << 8) | sens_data.data[3]) >> 4;
                pilot.acc_mag.acc_raw_z = ((sens_data.data[4] << 8) | sens_data.data[5]) >> 4;
                pilot.acc_mag.acc_x = sProjdefs2Compire(pilot.acc_mag.acc_raw_x, 12) *
                        pvLSM303DLHC_ACC_t[pilot.acc_mag.acc_scale];
                pilot.acc_mag.acc_y = sProjdefs2Compire(pilot.acc_mag.acc_raw_y, 12) *
                        pvLSM303DLHC_ACC_t[pilot.acc_mag.acc_scale];
                pilot.acc_mag.acc_z = sProjdefs2Compire(pilot.acc_mag.acc_raw_z, 12) *
                        pvLSM303DLHC_ACC_t[pilot.acc_mag.acc_scale];
#if 1
                sprintf(cBuff, "%d:ACC:%f:%f:%f\r\n", sens_data.time,
                        pilot.acc_mag.acc_x, pilot.acc_mag.acc_y,
                        pilot.acc_mag.acc_z);
                vLogdPrint(LOGD_INFO, cBuff, 10);
#endif
                pilot.acc_mag.acc_mcnt++;

                /* Normalize ACC data */
                tmp_acc_norm = sqrt(pilot.acc_mag.acc_x * pilot.acc_mag.acc_x +
                        pilot.acc_mag.acc_y * pilot.acc_mag.acc_y +
                        pilot.acc_mag.acc_z * pilot.acc_mag.acc_z);
                /* Check freefall state */
                if ((pilot.toff_mode == TOFF_CATAPULT) && (pilot.fly_state == FF_FLY_ON))
                {
                    if (tmp_acc_norm <= pilot.acc_catapult)
                    {
                        /* Found freefall state of the aircraft */
                        /* send the signal for start FLY */
                        prveTOFFSIG.signal = SIG_CMD_TOFF_FREEFALL;
                        prveTOFFSIG.on = 1;
                        xQueueSend(xQueueFLYSIG, &prveTOFFSIG, 10/portTICK_RATE_MS);
                    }
                }
                pilot.acc_mag.acc_x = pilot.acc_mag.acc_x / tmp_acc_norm;
                pilot.acc_mag.acc_y = pilot.acc_mag.acc_y / tmp_acc_norm;
                pilot.acc_mag.acc_z = pilot.acc_mag.acc_z / tmp_acc_norm;
                /* Calculate ACC Euler angles */
                tmp_acc_x2 = pilot.acc_mag.acc_x * pilot.acc_mag.acc_x;
                tmp_acc_y2 = pilot.acc_mag.acc_y * pilot.acc_mag.acc_y;
                tmp_acc_z2 = pilot.acc_mag.acc_z * pilot.acc_mag.acc_z;

                pilot.acc_mag.acc_euler_roll =  -(atan2(pilot.acc_mag.acc_y, sqrt(tmp_acc_x2 + tmp_acc_z2))) * PROJDEFS_CONST_RAD_2_DEG;
                pilot.acc_mag.acc_euler_pitch = (atan2(pilot.acc_mag.acc_x, sqrt(tmp_acc_y2 + tmp_acc_z2))) * PROJDEFS_CONST_RAD_2_DEG;
               // pilot.acc_mag.acc_euler_yaw = (atan2(pilot.acc_mag.acc_z, sqrt(tmp_acc_x2 + tmp_acc_y2))) * PROJDEFS_CONST_RAD_2_DEG;

                prvASIG = AUTOPILOT_SIG_EULER_REFRESH;
                xQueueSend(xQueueAUTOPILOTSIG, &prvASIG, 0);
#if 0
                sprintf(cBuff, "%d:EULER:%f:%f:%f\r\n", sens_data.time,
                        pilot.acc_mag.acc_euler_roll, pilot.acc_mag.acc_euler_pitch,
                        pilot.acc_mag.acc_euler_yaw);
                vLogdPrint(LOGD_DEBUG, cBuff, 10);
#endif
                break;
            case SENS_L3G4200D_HYR:
                pilot.hyr.raw_x = ((sens_data.data[2] << 8) | sens_data.data[3]);
                pilot.hyr.raw_y = ((sens_data.data[0] << 8) | sens_data.data[1]);
                pilot.hyr.raw_z = ((sens_data.data[4] << 8) | sens_data.data[5]);
                pilot.hyr.mcnt++;

                /* Calibration hyr zero-drift */
                if (pilot.fly_state == FF_SENS_COLL)
                {
                    if (pilot.hyr.ccnt == 0)
                    {
                        pilot.hyr.err_x = pilot.hyr.raw_x * pvL3G4200D_HYR_t[pilot.hyr.scale];
                        pilot.hyr.err_y = pilot.hyr.raw_y * pvL3G4200D_HYR_t[pilot.hyr.scale];
                        pilot.hyr.err_z = pilot.hyr.raw_z * pvL3G4200D_HYR_t[pilot.hyr.scale];
                    }
                    else
                    {
                        pilot.hyr.err_x = (pilot.hyr.err_x + pilot.hyr.raw_x * pvL3G4200D_HYR_t[pilot.hyr.scale]) / 2;
                        pilot.hyr.err_y = (pilot.hyr.err_y + pilot.hyr.raw_y * pvL3G4200D_HYR_t[pilot.hyr.scale]) / 2;
                        pilot.hyr.err_z = (pilot.hyr.err_z + pilot.hyr.raw_z * pvL3G4200D_HYR_t[pilot.hyr.scale]) / 2;
                    }
                    pilot.hyr.ccnt += 1;
                }
                else if (pilot.fly_state == FF_FLY_ON)
                {
                    pilot.yaw = 0;
                }

                dT = (sens_data.time - pilot.hyr.prev_measure_time) / 1000.0;

#ifdef PROJDEFS_SENS_FILTER_COMPLIMENTARY
                /* Calculate rotate angles with zero-drift compensation */
                pilot.hyr.x = (pilot.hyr.raw_x * pvL3G4200D_HYR_t[pilot.hyr.scale] - pilot.hyr.err_x) * dT;
                pilot.hyr.y = (pilot.hyr.raw_y * pvL3G4200D_HYR_t[pilot.hyr.scale] - pilot.hyr.err_y) * dT;
                pilot.hyr.z = (pilot.hyr.raw_z * pvL3G4200D_HYR_t[pilot.hyr.scale] - pilot.hyr.err_z) * dT;

#if 1
                sprintf(cBuff, "%d:HYR:%f:%f:%f\r\n", sens_data.time,
                        pilot.hyr.x, pilot.hyr.y, pilot.hyr.z);
                vLogdPrint(LOGD_INFO, cBuff, 10);
#endif
                /* Calculate angles with complimentary filter */
                pilot.roll = PROJDEFS_CONST_HYR_CF * (pilot.roll + pilot.hyr.x) + PROJDEFS_CONST_ACC_CF * pilot.acc_mag.acc_euler_roll;
                pilot.pitch = PROJDEFS_CONST_HYR_CF * (pilot.pitch + pilot.hyr.y) + PROJDEFS_CONST_ACC_CF * pilot.acc_mag.acc_euler_pitch;
                pilot.yaw = pilot.yaw - pilot.hyr.z;
                if (pilot.yaw < 0)
                    pilot.yaw += 360;
                if (pilot.yaw > 360)
                    pilot.yaw -= 360;
#endif /* PROJDEFS_SENS_FILTER_COMPLIMENTARY */

                /* Filter of Sebastian Madgwick without magnetometer */
#ifdef PROJDEFS_SENS_FILTER_MADGWICK
                /* Copy to local ACC data*/
                a_x = pilot.acc_mag.acc_x;
                a_y = pilot.acc_mag.acc_y;
                a_z = pilot.acc_mag.acc_z;
                /* Copy to local Quaternion data */
                SEq_1 = pilot.q1;
                SEq_2 = pilot.q2;
                SEq_3 = pilot.q3;
                SEq_4 = pilot.q4;
//                w_x = pilot.hyr.raw_x * pvL3G4200D_HYR_t[pilot.hyr.scale];
//                w_y = pilot.hyr.raw_y * pvL3G4200D_HYR_t[pilot.hyr.scale];
                w_x = pilot.hyr.raw_y * pvL3G4200D_HYR_t[pilot.hyr.scale];
                w_y = -1.0 * pilot.hyr.raw_x * pvL3G4200D_HYR_t[pilot.hyr.scale];
                w_z = pilot.hyr.raw_z * pvL3G4200D_HYR_t[pilot.hyr.scale];
                // Axulirary variables to avoid reapeated calcualtions
                halfSEq_1 = 0.5f * SEq_1;
                halfSEq_2 = 0.5f * SEq_2;
                halfSEq_3 = 0.5f * SEq_3;
                halfSEq_4 = 0.5f * SEq_4;
                twoSEq_1 = 2.0f * SEq_1;
                twoSEq_2 = 2.0f * SEq_2;
                twoSEq_3 = 2.0f * SEq_3;
                // Compute the objective function and Jacobian
                f_1 = twoSEq_2 * SEq_4 - twoSEq_1 * SEq_3 - a_x;
                f_2 = twoSEq_1 * SEq_2 + twoSEq_3 * SEq_4 - a_y;
                f_3 = 1.0f - twoSEq_2 * SEq_2 - twoSEq_3 * SEq_3 - a_z;
                J_11or24 = twoSEq_3; // J_11 negated in matrix multiplication
                J_12or23 = 2.0f * SEq_4;
                J_13or22 = twoSEq_1; // J_12 negated in matrix multiplication
                J_14or21 = twoSEq_2;
                J_32 = 2.0f * J_14or21; // negated in matrix multiplication
                J_33 = 2.0f * J_11or24; // negated in matrix multiplication
                // Compute the gradient (matrix multiplication)
                SEqHatDot_1 = J_14or21 * f_2 - J_11or24 * f_1;
                SEqHatDot_2 = J_12or23 * f_1 + J_13or22 * f_2 - J_32 * f_3;
                SEqHatDot_3 = J_12or23 * f_2 - J_33 * f_3 - J_13or22 * f_1;
                SEqHatDot_4 = J_14or21 * f_1 + J_11or24 * f_2;
                // Normalise the gradient
                norm = sqrt(SEqHatDot_1 * SEqHatDot_1 + SEqHatDot_2 * SEqHatDot_2 + SEqHatDot_3 * SEqHatDot_3 + SEqHatDot_4 * SEqHatDot_4);
                SEqHatDot_1 /= norm;
                SEqHatDot_2 /= norm;
                SEqHatDot_3 /= norm;
                SEqHatDot_4 /= norm;
                // Compute the quaternion derrivative measured by gyroscopes
                SEqDot_omega_1 = -halfSEq_2 * w_x - halfSEq_3 * w_y - halfSEq_4 * w_z;
                SEqDot_omega_2 = halfSEq_1 * w_x + halfSEq_3 * w_z - halfSEq_4 * w_y;
                SEqDot_omega_3 = halfSEq_1 * w_y - halfSEq_2 * w_z + halfSEq_4 * w_x;
                SEqDot_omega_4 = halfSEq_1 * w_z + halfSEq_2 * w_y - halfSEq_3 * w_x;
                // Compute then integrate the estimated quaternion derrivative
                SEq_1 += (SEqDot_omega_1 - (beta * SEqHatDot_1)) * dT;
                SEq_2 += (SEqDot_omega_2 - (beta * SEqHatDot_2)) * dT;
                SEq_3 += (SEqDot_omega_3 - (beta * SEqHatDot_3)) * dT;
                SEq_4 += (SEqDot_omega_4 - (beta * SEqHatDot_4)) * dT;
                // Normalise quaternion
                norm = sqrt(SEq_1 * SEq_1 + SEq_2 * SEq_2 + SEq_3 * SEq_3 + SEq_4 * SEq_4);
                SEq_1 /= norm;
                SEq_2 /= norm;
                SEq_3 /= norm;
                SEq_4 /= norm;
                /* Calculate angles with Madgwick filter */
                pilot.roll = (atan2(2 * (SEq_1 * SEq_2 + SEq_3 * SEq_4), 1 - 2 * (SEq_2 * SEq_2 + SEq_3 * SEq_3))) * PROJDEFS_CONST_RAD_2_DEG;
                pilot.pitch = (asin(2 * (SEq_1 * SEq_3 - SEq_2 * SEq_4))) * PROJDEFS_CONST_RAD_2_DEG;
                pilot.yaw = (atan2(2 * (SEq_1 * SEq_4 + SEq_2 * SEq_3), 1 - 2 * (SEq_3 * SEq_3 + SEq_4 * SEq_4))) * PROJDEFS_CONST_RAD_2_DEG;
                pilot.q1 = SEq_1;
                pilot.q2 = SEq_2;
                pilot.q3 = SEq_3;
                pilot.q4 = SEq_4;
#endif /* PROJDEFS_SENS_FILTER_MADGWICK */

                /* Filter of Sebastian Madgwick with magnetometer */
#ifdef PROJDEFS_SENS_FILTER_MADGWICK_WITHMAG
#endif /* PROJDEFS_SENS_FILTER_MADGWICK_WITHMAG */

#if 1
                sprintf(cBuff, "EULER:%f:%f:%f\r\n", pilot.roll, pilot.pitch, pilot.yaw);
                vLogdPrint(LOGD_TODO, cBuff, 10);
#endif
                /* Calculate angular speed */
                pilot.roll_speed = (pilot.roll - roll_prev) / dT;
                pilot.pitch_speed = (pilot.pitch - pitch_prev) / dT;
                pilot.yaw_speed = (pilot.yaw - yaw_prev) / dT;
                roll_prev = pilot.roll;
                pitch_prev = pilot.pitch;
                yaw_prev = pilot.yaw;
                /* Set last hyr measure time for angles calculation */
                pilot.hyr.prev_measure_time = sens_data.time;
                break;
            case SENS_LSM303DLHC_MAG:
                /* Store RAW value */
#if 0
                sprintf(cBuff, "%d:MAG:0x%X:0x%X:0x%X:0x%X:0x%X:0x%X\r\n", sens_data.time,
                        sens_data.data[0], sens_data.data[1], sens_data.data[2],
                        sens_data.data[3], sens_data.data[4], sens_data.data[5]);
                vLogdPrint(LOGD_TODO, cBuff, 10);
#endif
                pilot.acc_mag.mag_raw_x = ((sens_data.data[0] << 8) | sens_data.data[1]) >> 4;
                pilot.acc_mag.mag_raw_y = ((sens_data.data[2] << 8) | sens_data.data[3]) >> 4;
                pilot.acc_mag.mag_raw_z = ((sens_data.data[4] << 8) | sens_data.data[5]) >> 4;
#if 0
                sprintf(cBuff, "%d:MAG:0x%X:0x%X:0x%X\r\n", sens_data.time,
                        pilot.acc_mag.mag_raw_x, pilot.acc_mag.mag_raw_y,
                        pilot.acc_mag.mag_raw_z);
                vLogdPrint(LOGD_TODO, cBuff, 10);
#endif
                /* Calculate magnetic date */
                pilot.acc_mag.mag_x = sProjdefs2Compire(pilot.acc_mag.mag_raw_x, 12) *
                        pvLSM303DLHC_MAG_t[pilot.acc_mag.mag_gain - 1][1];
                pilot.acc_mag.mag_y = sProjdefs2Compire(pilot.acc_mag.mag_raw_y, 12) *
                        pvLSM303DLHC_MAG_t[pilot.acc_mag.mag_gain - 1][1];
                pilot.acc_mag.mag_z = sProjdefs2Compire(pilot.acc_mag.mag_raw_z, 12) *
                        pvLSM303DLHC_MAG_t[pilot.acc_mag.mag_gain - 1][2];
#if 0
                sprintf(cBuff, "%d:MAG:%f:%f:%f\r\n", sens_data.time,
                        pilot.acc_mag.mag_x, pilot.acc_mag.mag_y,
                        pilot.acc_mag.mag_z);
                vLogdPrint(LOGD_TODO, cBuff, 10);
#endif
                pilot.acc_mag.mag_mcnt++;

                /* Normalaize MAG data */
#if 0
                tmp_mag_norm = sqrt(pilot.acc_mag.mag_x * pilot.acc_mag.mag_x +
                        pilot.acc_mag.mag_y * pilot.acc_mag.mag_y +
                        pilot.acc_mag.mag_z * pilot.acc_mag.mag_z);
                pilot.acc_mag.mag_x = pilot.acc_mag.mag_x / tmp_mag_norm;
                pilot.acc_mag.mag_y = pilot.acc_mag.mag_y / tmp_mag_norm;
                pilot.acc_mag.mag_z = pilot.acc_mag.mag_z / tmp_mag_norm;

                /* Calculate heading */
                tmp_roll_rad = pilot.roll * PROJDEFS_CONST_DEG_2_RAD;
                tmp_pitch_rad = pilot.pitch * PROJDEFS_CONST_DEG_2_RAD;

                tmp_mag_x2 = pilot.acc_mag.mag_x * cos(tmp_pitch_rad) +
                        pilot.acc_mag.mag_z * sin(tmp_pitch_rad);
                tmp_mag_y2 = pilot.acc_mag.mag_x * sin(tmp_roll_rad) * sin(tmp_pitch_rad) +
                        pilot.acc_mag.mag_y * cos(tmp_roll_rad) -
                        pilot.acc_mag.mag_z * sin(tmp_roll_rad) * cos(tmp_pitch_rad);
                pilot.yaw = atan2(tmp_mag_y2, tmp_mag_x2) * PROJDEFS_CONST_RAD_2_DEG;
                if (pilot.yaw < 0)
                    pilot.yaw += 360;
#endif
#if 0
                sprintf(cBuff, "YAW:%f\r\n", pilot.yaw);
                vLogdPrint(LOGD_TODO, cBuff, 10);
#endif
                break;
            case SENS_LSM303DLHC_TEMP:
                pilot.acc_mag.t = 25 + sens_data.data[0];

#if 0
                sprintf(cBuff, "%d:MAG_T:%d\r\n", sens_data.time, pilot.acc_mag.t);
                vLogdPrint(LOGD_TODO, cBuff, 10);
#endif
                break;
            case SENS_L3G4200D_TEMP:
                pilot.hyr.t = sens_data.data[0];
                /* Calculate real temperature from a RAW data */
                pilot.hyr.t = 25 - pilot.hyr.t;
                pilot.hyr.t = pilot.hyr.t + 25;
#if 0
                sprintf(cBuff, "%d:HYR_T:%d\r\n", sens_data.time, pilot.hyr.t);
                vLogdPrint(LOGD_TODO, cBuff, 10);
#endif
                break;
            default:
                /* Incorrect type of a sensor */
                sprintf(cBuff, "Incorrect TYPE a sensor\r\n");
                vLogdPrint(LOGD_BUG, cBuff, 10);
                break;
        }
    }
    vTaskDelete(NULL);
}

/* FLY FSM service task */
void prvFLYd(void *pvParameters)
{
    static portBASE_TYPE xResult;
    static PROJDEFS_FLYFSM_SIG_STRUCT fly_sig;
    static char cBuff[LOGD_MESSAGE_CHAR_BUFFER];
    static PROJDEFS_LED_QUEUE led_cmd;
    static xTaskHandle xHandle = NULL;
    static AUTOPILOT_SIG_TYPE prvASIG;

    for(;;)
    {
        fly_sig.signal = SIG_CMD_NONE;
        fly_sig.on = 0;

        if (uxQueueMessagesWaiting(xQueueFLYSIG) != 0)
        {
            xQueueReceive(xQueueFLYSIG, &fly_sig, portMAX_DELAY);

#ifdef DEBUG_SIGNAL_ENABLE
            sprintf(cBuff,"SIG:%d:%d\r\n", fly_sig.signal, fly_sig.on);
            vLogdPrint(LOGD_INFO, cBuff, 10);
#endif
        }

        switch (pilot.fly_state)
        {
        case FF_IDLE:
            if ((fly_sig.signal == SIG_CMD_BUTTON) && (fly_sig.on))
            {
                /* Check sensors state before fly */
                if ((pilot.sensors_health & MAV_SYS_STATUS_SENSOR_3D_MAG) &&
                        (pilot.sensors_health & MAV_SYS_STATUS_SENSOR_3D_ACCEL) &&
                        (pilot.sensors_health & MAV_SYS_STATUS_SENSOR_3D_GYRO) &&
                        (pilot.sensors_health & MAV_SYS_STATUS_SENSOR_ABSOLUTE_PRESSURE))
                {
                    /* Skip first bad data */
                    pilot.fly_state = FF_SENS_SKIP;
                    /* Set skip bad data timeout */
                    pilot.sensors_time = ulLOCALTIME + SENS_DATA_SKIP_TIME;
                    pilot.i2c1.data_flow = I2C_DF_START;
                    /* Set short blink while dataflow enabled */
                    vProjdefsARMshortblink();
                    /* Start dataflow */
                    vProjdefsI2C1Start();
                    sprintf(cBuff, "Fly begin...\r\n");
                    vLogdPrint(LOGD_INFO, cBuff, 10);
                    sprintf(cBuff, "prvFLYd IDLE -> SENS_SKIP\r\n");
                    vLogdPrint(LOGD_DEBUG, cBuff, 10);
                    pilot.mav_system_state = MAV_STATE_CALIBRATING;
                }
                else
                {
                    sprintf(cBuff, "Sensors FAILED: 0x%X\r\n", pilot.sensors_health);
                    vLogdPrint(LOGD_INFO, cBuff, 10);
                }
            }
            break;
        case FF_SENS_SKIP:
            if ((fly_sig.signal == SIG_CMD_BUTTON) && (fly_sig.on))
            {
                pilot.fly_state = FF_LANDING;
                sprintf(cBuff, "prvFLYd SENS_SKIP -> LANDING\r\n");
                vLogdPrint(LOGD_DEBUG, cBuff, 10);
            }
            if (pilot.sensors_time < ulLOCALTIME)
            {
                /* Next state is callibrate the sensors */
                pilot.fly_state = FF_SENS_COLL;
                /* Clear calibration data counter */
                pilot.bar.ccnt = 0;
                pilot.hyr.ccnt = 0;
                /* Set calibration period */
                pilot.sensors_time = ulLOCALTIME + SENS_DATA_COLL_TIME;
                sprintf(cBuff, "Start the sensors calibration...\r\n");
                vLogdPrint(LOGD_INFO, cBuff, 10);
                sprintf(cBuff, "prvFLYd SENS_SKIP -> SENS_COLL\r\n");
                vLogdPrint(LOGD_DEBUG, cBuff, 10);
            }
            break;
        /* Sensors calibration start */
        case FF_SENS_COLL:
            if ((fly_sig.signal == SIG_CMD_BUTTON) && (fly_sig.on))
            {
                pilot.fly_state = FF_LANDING;
                sprintf(cBuff, "prvFLYd SENS_COLL -> LANDING\r\n");
                vLogdPrint(LOGD_DEBUG, cBuff, 10);
            }
            if (pilot.sensors_time < ulLOCALTIME)
            {
                /* Calibration finished. Start FLY */
                pilot.fly_state = FF_START;
                sprintf(cBuff, "Ground level %f m\r\n", pilot.ground_altitude);
                vLogdPrint(LOGD_INFO, cBuff, 10);
                sprintf(cBuff, "HYR drift %f %f %f deg/s\r\n",
                        pilot.hyr.err_x, pilot.hyr.err_y, pilot.hyr.err_z);
                vLogdPrint(LOGD_INFO, cBuff, 10);
                sprintf(cBuff, "prvFLYd SENS_COLL -> START\r\n");
                vLogdPrint(LOGD_DEBUG, cBuff, 10);
                /* Set PULSE ARM LED */
                led_cmd.led_num = PROJDEFS_LED_ARM;
                led_cmd.type = PROJDEFS_LED_PULSE;
                led_cmd.on = 0;
                xQueueSend(xQueueLEDs, &led_cmd, 0);
                pilot.mav_system_state = MAV_STATE_ACTIVE;
                pilot.mav_system_mode = MAV_MODE_AUTO_ARMED;
            }
            break;
        case FF_START:
            pilot.fly_state = FF_FLY_ON;
            if ((fly_sig.signal == SIG_CMD_BUTTON) && (fly_sig.on))
            {
                pilot.fly_state = FF_LANDING;
                sprintf(cBuff, "prvFLYd START -> LANDING\r\n");
                vLogdPrint(LOGD_DEBUG, cBuff, 10);
            }
            if (pilot.xHandleAutopilot == NULL)
            {
                switch (pilot.aircraft)
                {
                case TYPE_PLANER:
                    /* Start autopilot process for non-motor planer */
                    xTaskCreate( vAUTOPILOT_PLANER, ( signed char * ) "vAUTOPILOT_PLANER",
                    configMINIMAL_STACK_SIZE, NULL, 3, &xHandle);
                    //configASSERT( xHandle );
                    if (xHandle == NULL)
                    {
                        pilot.fly_state = FF_LANDING;
                        sprintf(cBuff, "Can't start autopilot task\r\n");
                        vLogdPrint(LOGD_INFO, cBuff, 10);
                    }
                    else
                    {
                        pilot.xHandleAutopilot = xHandle;
                    }
                    break;
                default:
                    pilot.fly_state = FF_LANDING;
                    sprintf(cBuff, "Unknown aircraft type, FLY STOPPED\r\n");
                    vLogdPrint(LOGD_INFO, cBuff, 10);
                    break;
                }
            }
            else
            {
                vTaskResume(pilot.xHandleAutopilot);
            }
            if (pilot.fly_state == FF_FLY_ON)
            {
                /* Reset Queue of CMD for Autopilot */
                xQueueReset(xQueueAUTOPILOTSIG);
                sprintf(cBuff, "prvFLYd START -> FLY_ON\r\n");
                vLogdPrint(LOGD_DEBUG, cBuff, 10);
            }
            break;
        case FF_FLY_ON:
            if ((fly_sig.signal == SIG_CMD_BUTTON) && (fly_sig.on))
            {
                pilot.fly_state = FF_LANDING;
                sprintf(cBuff, "prvFLYd FLY_ON -> LANDING\r\n");
                vLogdPrint(LOGD_DEBUG, cBuff, 10);
            }
            if (pilot.toff_mode == TOFF_CATAPULT)
            {
                if ((fly_sig.signal == SIG_CMD_TOFF_FREEFALL) && (fly_sig.on))
                {
                    pilot.fly_state = FF_FLY;
                    /* Start PWMs for servos */
                    vPWMDRIVER_START_PWM();
                    /* Set servos to null point */
                    prvASIG = AUTOPILOT_SIG_NULL;
                    xQueueSend(xQueueAUTOPILOTSIG, &prvASIG, 0);
                    /* Set ON GPS LED */
                    led_cmd.led_num = PROJDEFS_LED_ARM;
                    led_cmd.on = 1;
                    xQueueSend(xQueueLEDs, &led_cmd, 0);
                    sprintf(cBuff, "prvFLYd FLY_ON -> FLY\r\n");
                    vLogdPrint(LOGD_DEBUG, cBuff, 10);
                }
            }
            else
            {
                pilot.fly_state = FF_FLY;
                /* Start PWMs for servos */
                vPWMDRIVER_START_PWM();
                /* Set servos to null point */
                prvASIG = AUTOPILOT_SIG_NULL;
                xQueueSend(xQueueAUTOPILOTSIG, &prvASIG, 0);
                /* Set ON ARM LED */
                led_cmd.led_num = PROJDEFS_LED_ARM;
                led_cmd.on = 1;
                xQueueSend(xQueueLEDs, &led_cmd, 0);
                sprintf(cBuff, "prvFLYd FLY_ON -> FLY\r\n");
                vLogdPrint(LOGD_DEBUG, cBuff, 10);
            }
            break;
        case FF_FLY:
            if ((fly_sig.signal == SIG_CMD_BUTTON) && (fly_sig.on))
            {
                pilot.fly_state = FF_LANDING;
                sprintf(cBuff, "prvFLYd FLY -> LANDING\r\n");
                vLogdPrint(LOGD_DEBUG, cBuff, 10);
            }
            break;
        case FF_LANDING:
            pilot.fly_state = FF_STOP;
            sprintf(cBuff, "prvFLYd LANDING -> STOP\r\n");
            vLogdPrint(LOGD_DEBUG, cBuff, 10);
            break;
        case FF_STOP:
            pilot.fly_state = FF_IDLE;
            pilot.i2c1.data_flow = I2C_DF_STOP;
            /* Stop PWMs for servos */
            vPWMDRIVER_STOP_PWM();
            /* Suspend task of Autopilot */
            vTaskSuspend(pilot.xHandleAutopilot);
            /* Short blink of ARM and OFF */
            vProjdefsARMblink();
            sprintf(cBuff, "prvFLYd STOP -> IDLE\r\n");
            vLogdPrint(LOGD_DEBUG, cBuff, 10);
            pilot.mav_system_state = MAV_STATE_STANDBY;
            pilot.mav_system_mode = MAV_MODE_PREFLIGHT;
            break;
        default:
            /* Undefined configuration message. Log it */
            sprintf(cBuff, "prvFLYd UNKNOWN FLY FSM STATE\r\n");
            vLogdPrint(LOGD_BUG, cBuff, 10);
            break;
        }

        vTaskDelay(50/portTICK_RATE_MS);
    }
    vTaskDelete(NULL);
}

/* Service task for convert RAW data from PPM decoder to system value */
void prvPPMd(void *pvParameters)
{
    static PPM_DRIVER_CHANNEL_QUEUE raw_data;
    static PROJDEFS_FLYFSM_SIG_STRUCT flysig;
    static unsigned char k, n;
    static float temp;

    for(;;)
    {
        /* receive new RAW PPM value from the ppm decoder */
        xQueueReceive(xQueuePPMDECODER, &raw_data, portMAX_DELAY);

        /* Check all channel reset from Timer2 interrupt see ppm-driver.c */
        if (raw_data.channel >= PPM_DRIVER_INPUT_LINES)
        {
            for (k = 0; k < PPM_DRIVER_INPUT_LINES; k++)
            {
                pilot.rc.channel[k] = 0;
                pilot.rc.channel_type[k].state = 0;
            }
        }
        else
        {
            /* Set state of decoding input channel */
            pilot.rc.channel_type[raw_data.channel].state = 1;

            /* Normalize RAW value */
            if (raw_data.value < pilot.rc.channel_min[raw_data.channel])
                raw_data.value = pilot.rc.channel_min[raw_data.channel];
            else if (raw_data.value > pilot.rc.channel_max[raw_data.channel])
                raw_data.value = pilot.rc.channel_max[raw_data.channel];

            /* Signals */
            if (pilot.rc.channel_type[raw_data.channel].type & PPM_DRIVER_CHAN_SIGNAL)
            {
                for (k = 0; k < PPM_DRIVER_INPUT_SIGNALS; k++)
                {
                    if ((pilot.rc.channel_signal[k].channel == raw_data.channel) &&
                        (pilot.rc.channel_signal[k].enable))
                    {
                        if (raw_data.value < pilot.rc.channel_mid[raw_data.channel])
                            pilot.rc.channel_signal[k].state = PPM_DRIVER_CHAN_OFF;
                        else
                            pilot.rc.channel_signal[k].state = PPM_DRIVER_CHAN_ON;

                        /* Check state of signal */
                        if (pilot.rc.channel_signal[k].state != pilot.rc.channel_signal[k].prev_state)
                        {
                            /* Send the new signal to FLY FSM */
                            flysig.signal = pilot.rc.channel_signal[k].signal;
                            flysig.on = pilot.rc.channel_signal[k].state;
                            xQueueSend(xQueueFLYSIG, &flysig, 10/portTICK_RATE_MS);

                            /* Refresh previouse state of signal */
                            pilot.rc.channel_signal[k].prev_state = pilot.rc.channel_signal[k].state;
                        }
                    }
                }
            }

            /* Switches*/
            if (pilot.rc.channel_type[raw_data.channel].type & PPM_DRIVER_CHAN_SWITCH)
            {
                for (k = 0; k < PPM_DRIVER_INPUT_SWITCHES; k++)
                {
                    if ((pilot.rc.channel_switch[k].channel == raw_data.channel) && (pilot.rc.channel_switch[k].enable))
                    {
                        if (raw_data.value < pilot.rc.channel_mid[raw_data.channel])
                            pilot.rc.channel_switch[k].state = PPM_DRIVER_CHAN_OFF;
                        else
                            pilot.rc.channel_switch[k].state = PPM_DRIVER_CHAN_ON;

                        /* Check state of switch */
                        if (pilot.rc.channel_switch[k].state != pilot.rc.channel_switch[k].prev_state)
                        {
                            /* Update hardware PWM MAP */
                            for (n = 0; n < PWM_DRIVER_HPWM_CNT; n++)
                            {
                                if (pilot.rc.channel_switch[k].state)
                                {
                                    /* MAP for ON switch */
                                    if ((pilot.rc.channel_switch[k].hpwm_use[n]) &&
                                            (pilot.pwm.hpwm_type[n] == TYPE_MAP_DYNAMIC))
                                        pilot.pwm.hpwm_servo_map[n] = pilot.rc.channel_switch[k].hpwm_map_on[n];
                                }
                                else
                                {
                                    /* MAP for OFF switch */
                                    if ((pilot.rc.channel_switch[k].hpwm_use[n]) &&
                                            (pilot.pwm.hpwm_type[n] == TYPE_MAP_DYNAMIC))
                                        pilot.pwm.hpwm_servo_map[n] = pilot.rc.channel_switch[k].hpwm_map_off[n];
                                }
                            }

                            /* Update software PWM MAP */
                            for (n = 0; n < PWM_DRIVER_SPWM_CNT; n++)
                            {
                                if (pilot.rc.channel_switch[k].state)
                                {
                                    /* MAP for ON switch */
                                    if ((pilot.rc.channel_switch[k].spwm_use[n]) &&
                                            (pilot.pwm.spwm_type[n] == TYPE_MAP_DYNAMIC))
                                        pilot.pwm.spwm_servo_map[n] = pilot.rc.channel_switch[k].spwm_map_on[n];
                                }
                                else
                                {
                                    /* MAP for OFF switch */
                                    if ((pilot.rc.channel_switch[k].spwm_use[n]) &&
                                            (pilot.pwm.spwm_type[n] == TYPE_MAP_DYNAMIC))
                                        pilot.pwm.spwm_servo_map[n] = pilot.rc.channel_switch[k].spwm_map_off[n];
                                }
                            }

                            /* Refresh previouse state of switch */
                            pilot.rc.channel_switch[k].prev_state = pilot.rc.channel_switch[k].state;
                        }
                    }
                }
            }

            /* Calculate RAW to absolute value */
            temp = raw_data.value - pilot.rc.channel_min[raw_data.channel];
            pilot.rc.channel[raw_data.channel] = temp;

            if (pilot.rc.channel_type[raw_data.channel].type & PPM_DRIVER_CHAN_RC)
            {
                temp /= pilot.rc.channel_range[raw_data.channel];

                /* Update mapped channels */
                for (k = 0; k < PWM_DRIVER_HPWM_CNT; k++)
                {
                    if (pilot.pwm.hpwm_servo_map[k] == raw_data.channel)
                    {
                        if ((pilot.pwm.hpwm_type[k] == TYPE_MAP_STATIC) ||
                                (pilot.pwm.hpwm_type[k] == TYPE_MAP_DYNAMIC))
                            pilot.pwm.hpwm[k] = (unsigned int)(temp *
                                    (PWM_DRIVER_HPWM_MAX_VAL - PWM_DRIVER_HPWM_MIN_VAL) +
                                    PWM_DRIVER_HPWM_MIN_VAL);
                    }
                }

                for (k = 0; k < PWM_DRIVER_SPWM_CNT; k++)
                {
                    if (pilot.pwm.spwm_servo_map[k] == raw_data.channel)
                    {
                        if ((pilot.pwm.spwm_type[k] == TYPE_MAP_STATIC) ||
                                (pilot.pwm.spwm_type[k] == TYPE_MAP_DYNAMIC))
                            pilot.pwm.spwm[k] = (unsigned int)(temp *
                                    (PWM_DRIVER_SPWM_MAX_VAL - PWM_DRIVER_SPWM_MIN_VAL) +
                                    PWM_DRIVER_SPWM_MIN_VAL);
                    }
                }
            }
        }
    }
    vTaskDelete(NULL);
}

/*
 * USB-serial console task
 */
void prvCONSOLEd(void *pvParameters)
{
    static PROJDEFS_FLYFSM_SIG_STRUCT prveBTNSIG;

    for(;;)
    {
        if (BTN_CMD == 0)
        {
            prveBTNSIG.signal = SIG_CMD_BUTTON;
            prveBTNSIG.on = 1;
            xQueueSend(xQueueFLYSIG, &prveBTNSIG, 10/portTICK_RATE_MS);
            vTaskDelay(200/portTICK_RATE_MS);
        }
        vTaskDelay(50/portTICK_RATE_MS);
    }
    vTaskDelete(NULL);
}

void prvRFLINKTXd(void *pvParameters)
{
    static PROJDEFS_CONF_RFLINK_MSG msg;

    for(;;)
    {
        if (pilot.rflink_state.enable)
        {
            if (!pilot.rflink_state.tx_busy)
            {
                if (uxQueueMessagesWaiting(xQueueRFLINKTX) != 0)
                {
                    INTEnable(RFLINK_INT_TX, INT_ENABLED);
                }
            }
            vTaskDelay(1/portTICK_RATE_MS);
        }
        else
        {
            vTaskDelay(200/portTICK_RATE_MS);
        }
    }
    vTaskDelete(NULL);
}

#if RFLINK_UART==UART1
void __attribute__( (interrupt(ipl0), vector(_UART_1_VECTOR), nomips16)) vRFLINKInterruptWrapper( void );
#elif RFLINK_UART==UART2
void __attribute__( (interrupt(ipl0), vector(_UART_2_VECTOR), nomips16)) vRFLINKInterruptWrapper( void );
#elif RFLINK_UART==UART3
void __attribute__( (interrupt(ipl0), vector(_UART_3_VECTOR), nomips16)) vRFLINKInterruptWrapper( void );
#endif

/* Handler RFLINK RX interrupt */
void vRFLINKInterruptHandler(void)
{
    static portBASE_TYPE xHigherPriorityTaskWoken;
    static PROJDEFS_CONF_RFLINK_MSG msgmav_rx, msgmav_tx;
    static unsigned short k, tx_byte_cnt;
    static BYTE rx_byte;

    /* We want woken a task at the start of the ISR. */
    xHigherPriorityTaskWoken = pdFALSE;

    /* Send MAVLINK data packet */
    if (INTGetFlag(RFLINK_INT_TX) && INTGetEnable(RFLINK_INT_TX)) /* transmit buffer empty? */
    {
        if (!pilot.rflink_state.tx_busy)
        {
            if (uxQueueMessagesWaitingFromISR(xQueueRFLINKTX) != 0)
            {
                /* Get MAVLINK message from Queue */
                xQueueReceiveFromISR(xQueueRFLINKTX, &msgmav_tx, &xHigherPriorityTaskWoken);
                pilot.rflink_state.tx_busy = 1;
                /* init counter */
                tx_byte_cnt = 1;
                /* start send a data */
                UARTSendDataByte(RFLINK_UART, msgmav_tx.msg[0]);
                LED_COM_ON;
            }
            else
            {
                /* No more packet for send */
                pilot.rflink_state.tx_busy = 0;
                INTEnable(RFLINK_INT_TX, INT_DISABLED);
                LED_COM_OFF;
            }
        }
        else
        {
            UARTSendDataByte(RFLINK_UART, msgmav_tx.msg[tx_byte_cnt]);
            tx_byte_cnt++;
            LED_COM_TOG;
            if (tx_byte_cnt >= msgmav_tx.len)
            {
                /* No more byte in a packet for send */
                pilot.rflink_state.tx_busy = 0;

                /* More packet in Queue for send? */
                if (uxQueueMessagesWaitingFromISR(xQueueRFLINKTX) == 0)
                {
                    INTEnable(RFLINK_INT_TX, INT_DISABLED);
                    LED_COM_OFF;
                }
            }
        }

#if RFLINK_UART==UART1
        mU1TXClearIntFlag();
#elif RFLINK_UART==UART2
        mU2TXClearIntFlag();
#elif RFLINK_UART==UART3
        mU3TXClearIntFlag();
#endif
    }

    /* Receive MAVLINK data packet */
    if (INTGetFlag(RFLINK_INT_RX) && INTGetEnable(RFLINK_INT_RX))
    {
        rx_byte = UARTGetDataByte(RFLINK_UART);

        switch (pilot.rflink_rx)
        {
        case RFLINK_RX_WAIT:
        case RFLINK_RX_ERROR:
            if (rx_byte == MAVLINK_STX)
            {
                pilot.rflink_rx = RFLINK_RX_START_FOUND;
                k = 0;
                msgmav_rx.msg[0] = rx_byte;
            }
            break;
        case RFLINK_RX_START_FOUND:
            msgmav_rx.len = rx_byte;
            msgmav_rx.msg[1] = rx_byte;
            pilot.rflink_rx = RFLINK_RX_SEQ;
            break;
        case RFLINK_RX_SEQ:
            msgmav_rx.msg[2] = rx_byte;
            pilot.rflink_rx = RFLINK_RX_SYSID;
            break;
        case RFLINK_RX_SYSID:
            msgmav_rx.msg[3] = rx_byte;
            pilot.rflink_rx = RFLINK_RX_COMPID;
            break;
        case RFLINK_RX_COMPID:
            msgmav_rx.msg[4] = rx_byte;
            pilot.rflink_rx = RFLINK_RX_MSG_TYPE;
            break;
        case RFLINK_RX_MSG_TYPE:
            msgmav_rx.msg[5] = rx_byte;
            pilot.rflink_rx = RFLINK_RX_PAYLOAD_RCV;
            break;
        case RFLINK_RX_PAYLOAD_RCV:
            if (k >= (msgmav_rx.len))
            {
                pilot.rflink_rx = RFLINK_RX_CRC_1;
            }
            msgmav_rx.msg[k + 6] = rx_byte;
            k++;
            break;
        case RFLINK_RX_CRC_1:
            msgmav_rx.msg[msgmav_rx.len + 7] = rx_byte;
            pilot.rflink_rx = RFLINK_RX_CRC_2;
            break;
        case RFLINK_RX_CRC_2:
            msgmav_rx.msg[msgmav_rx.len + 8] = rx_byte;
            msgmav_rx.len += 8;
            pilot.rflink_rx = RFLINK_RX_WAIT;
            /* Send received mavlinkpacet here */
            xQueueSendFromISR(xQueueRFLINKRX, &msgmav_rx, &xHigherPriorityTaskWoken);
            break;
        }

#if RFLINK_UART==UART1
        mU1RXClearIntFlag();
#elif RFLINK_UART==UART2
        mU2RXClearIntFlag();
#elif RFLINK_UART==UART3
        mU3RXClearIntFlag();
#endif
    }

    /* If sending or receiving necessitates a context switch, then switch now. */
    portEND_SWITCHING_ISR( xHigherPriorityTaskWoken );
}

void prvRFLINKRXd(void *pvParameters)
{
    static PROJDEFS_FLYFSM_SIG_STRUCT flysig;
    static PROJDEFS_CONF_RFLINK_MSG msgmav_rx;
    static mavlink_message_t msg;
    static mavlink_status_t status;
    static mavlink_request_data_stream_t request_data_stream;
    static unsigned short k;

    for(;;)
    {
        /* receive new data */
        xQueueReceive(xQueueRFLINKRX, &msgmav_rx, portMAX_DELAY);
#if 0
        for (k = 0; k < msgmav_rx.len; k++)
        {
            if(mavlink_parse_char(MAVLINK_COMM_0, msgmav_rx.msg[k], &msg, &status))
            {
                if (msg.sysid == pilot.mav_msg.ground_station_id)
                {
                    switch(msg.msgid)
                    {
                    case MAVLINK_MSG_ID_HEARTBEAT:
                        /* restore link after lost */
                        if (!pilot.mav_msg.link_state)
                        {
                            pilot.mav_msg.link_state = 1;
                            pilot.mav_msg.link_prev_state = 0;
                            flysig.signal = SIG_CMD_MAVLINK;
                            flysig.on = 1;
                            xQueueSend(xQueueFLYSIG, &flysig, 10/portTICK_RATE_MS);
                        }
                        /* Clear mavlink timeout counter */
                        pilot.mav_msg.timeout_cnt = 0;
                        break;
                    case MAVLINK_MSG_ID_REQUEST_DATA_STREAM:
                        mavlink_msg_request_data_stream_decode(&msg, &request_data_stream);
                        switch (request_data_stream.req_stream_id)
                        {

                        }
                        break;
                    case MAVLINK_MSG_ID_COMMAND_LONG:
                        break;

                        default:
                        //Do nothing
                        break;
		    }
                    break;
                }

                pilot.mav_packet_rx_drops += status.packet_rx_drop_count;
                pilot.mav_packet_rx_ok += status.packet_rx_success_count;
            }
        }
#endif
    }
    vTaskDelete(NULL);
}

void prvMAVLINKd(void *pvParameters)
{
    static PROJDEFS_MAVLINK_SHED_STRUCT msg_sched_table[PROJDEFS_MAVLINK_SCHED_SIZE] = {
        {MAV_COMP_ID_ALL, MAVLINK_MSG_ID_HEARTBEAT, 1000, 0},
        {MAV_COMP_ID_ALL, MAVLINK_MSG_ID_SYS_STATUS, 5000, 0},
        {MAV_COMP_ID_ALL, MAVLINK_MSG_ID_SYSTEM_TIME, 5000, 0},
        {MAV_COMP_ID_IMU, MAVLINK_MSG_ID_SCALED_PRESSURE, 1000, 0},
        {MAV_COMP_ID_IMU, MAVLINK_MSG_ID_ATTITUDE, 100, 0},
        {MAV_COMP_ID_ALL, MAVLINK_MSG_ID_RC_CHANNELS_SCALED, 500, 0},
        {MAV_COMP_ID_SERVO1, MAVLINK_MSG_ID_SERVO_OUTPUT_RAW, 40, 0},
        {MAV_COMP_ID_SERVO2, MAVLINK_MSG_ID_SERVO_OUTPUT_RAW, 40, 0},
        //{MAV_COMP_ID_IMU, MAVLINK_MSG_ID_SCALED_IMU, 1000, 0},
    };
    static PROJDEFS_CONF_RFLINK_MSG msgmav;
    static mavlink_message_t msg;
    static unsigned char k, n, msg_cnt,msg_fl;
    static short rc_tmp[PPM_DRIVER_INPUT_LINES];
    static unsigned short pwm_tmp[8];

    for(;;)
    {
        /* Send MAVLINK messages if enabled */
        if (pilot.mav_msg.enable)
        {
            /* Send messages if link is ready or force mode */
            if ((pilot.mav_msg.link_state) || (pilot.mav_msg.force_data_send))
            {
                msg_cnt = 0;

                for (k = 0; k < PROJDEFS_MAVLINK_SCHED_SIZE; k++)
                {
                    if (ulLOCALTIME > msg_sched_table[k].start_time)
                    {
                        msg_fl = 0;

                        switch (msg_sched_table[k].msg_num)
                        {
                        case MAVLINK_MSG_ID_HEARTBEAT:
                            mavlink_msg_heartbeat_pack(pilot.mavlink_system.sysid,
                                    msg_sched_table[k].compid, &msg,
                                    pilot.mav_system_type, pilot.mav_autopilot_type,
                                    pilot.mav_system_mode, pilot.mav_custom_mode,
                                    pilot.mav_system_state);
                            msg_fl = 1;
                            break;
                        case MAVLINK_MSG_ID_SYS_STATUS:
                            mavlink_msg_sys_status_pack(pilot.mavlink_system.sysid,
                                    msg_sched_table[k].compid, &msg, pilot.sensors_present,
                                    pilot.sensors_enabled, pilot.sensors_health,
                                    pilot.mcu_load, 11250, 5, -1, 0, pilot.mav_packet_rx_drops,
                                    0, 0, 0, 0);
                            msg_fl = 1;
                            break;
                        case MAVLINK_MSG_ID_SYSTEM_TIME:
                            mavlink_msg_system_time_pack(pilot.mavlink_system.sysid,
                                    msg_sched_table[k].compid, &msg, 0, (unsigned int)ulLOCALTIME);
                            msg_fl = 1;
                            break;
                        case MAVLINK_MSG_ID_SCALED_IMU:
                            mavlink_msg_scaled_imu_pack(pilot.mavlink_system.sysid,
                                    msg_sched_table[k].compid, &msg, (unsigned int)ulLOCALTIME,
                                    (short)(pilot.acc_mag.acc_x * 1000),
                                    (short)(pilot.acc_mag.acc_y * 1000),
                                    (short)(pilot.acc_mag.acc_z * 1000),
                                    (short)(pilot.acc_mag.mag_x * 1000),
                                    (short)(pilot.acc_mag.mag_y * 1000),
                                    (short)(pilot.acc_mag.mag_z * 1000),
                                    (short)(pilot.hyr.x * 1000),
                                    (short)(pilot.hyr.y * 1000),
                                    (short)(pilot.hyr.z * 1000));
                            msg_fl = 1;
                            break;
                        case MAVLINK_MSG_ID_SCALED_PRESSURE:
                            mavlink_msg_scaled_pressure_pack(pilot.mavlink_system.sysid,
                                    msg_sched_table[k].compid, &msg, (unsigned int)ulLOCALTIME,
                                    (float)pilot.bar.press, 0, (short)(pilot.bar.t * 100));
                            msg_fl = 1;
                            break;
                        case MAVLINK_MSG_ID_ATTITUDE:
                            mavlink_msg_attitude_pack(pilot.mavlink_system.sysid,
                                    msg_sched_table[k].compid, &msg, (unsigned int)ulLOCALTIME,
                                    pilot.roll * PROJDEFS_CONST_DEG_2_RAD,
                                    pilot.pitch * PROJDEFS_CONST_DEG_2_RAD,
                                    pilot.yaw * PROJDEFS_CONST_DEG_2_RAD,
                                    pilot.roll_speed * PROJDEFS_CONST_DEG_2_RAD,
                                    pilot.pitch_speed * PROJDEFS_CONST_DEG_2_RAD,
                                    pilot.yaw_speed * PROJDEFS_CONST_DEG_2_RAD);
                            msg_fl = 1;
                            break;
                        case MAVLINK_MSG_ID_RC_CHANNELS_SCALED:
                            for (n = 0; n < PPM_DRIVER_INPUT_LINES; n++)
                            {
                                if ((pilot.rc.channel_type[n].state) &&
                                        (pilot.rc.channel_type[n].enable))
                                {
                                    rc_tmp[n] = (short)(pilot.rc.channel[n] - pilot.rc.channel_mid[n]) *
                                            (10000 / pilot.rc.channel_range[n]);
                                }
                                else
                                {
                                    rc_tmp[n] = INT16_MAX;
                                }
                            }
                            mavlink_msg_rc_channels_scaled_pack(pilot.mavlink_system.sysid,
                                    msg_sched_table[k].compid, &msg, (unsigned int)ulLOCALTIME,
                                    1, rc_tmp[0], rc_tmp[1], rc_tmp[2], rc_tmp[3],
                                    rc_tmp[4], rc_tmp[5], rc_tmp[6], rc_tmp[7], 255);
                            msg_fl = 1;
                            break;
                        case MAVLINK_MSG_ID_SERVO_OUTPUT_RAW:
                            if (msg_sched_table[k].compid == MAV_COMP_ID_SERVO1)
                            {
                                for (n = 0; n < PWM_DRIVER_HPWM_CNT; n++)
                                {
                                    pwm_tmp[n] = (unsigned short)((pilot.pwm.hpwm[0] - PWM_DRIVER_HPWM_MIN_VAL) * 0.7 + PWM_DRIVER_HPWM_MIN_VAL);
                                }
                                mavlink_msg_servo_output_raw_pack(pilot.mavlink_system.sysid,
                                    msg_sched_table[k].compid, &msg, (unsigned int)ulLOCALTIME,
                                        1, pwm_tmp[0], pwm_tmp[1], pwm_tmp[2], pwm_tmp[3],
                                        0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF);
                            }
                            else
                            {
                                for (n = 0; n < PWM_DRIVER_SPWM_CNT; n++)
                                {
                                    pwm_tmp[n] = (unsigned short)((pilot.pwm.spwm[0] - PWM_DRIVER_SPWM_MIN_VAL) * 0.8 + PWM_DRIVER_SPWM_MIN_VAL);
                                }
                                mavlink_msg_servo_output_raw_pack(pilot.mavlink_system.sysid,
                                    msg_sched_table[k].compid, &msg, (unsigned int)ulLOCALTIME,
                                        2, pwm_tmp[0], pwm_tmp[1], pwm_tmp[2], pwm_tmp[3],
                                        pwm_tmp[4], pwm_tmp[5], pwm_tmp[6], pwm_tmp[7]);
                            }
                            msg_fl = 1;
                            break;
                        }

                        /* Set new message time */
                        msg_sched_table[k].start_time = ulLOCALTIME + msg_sched_table[k].msg_period;

                        msg_cnt++;

                        if (msg_fl)
                        {
                            /* Copy the message to the send buffer */
                            msgmav.len = mavlink_msg_to_send_buffer(msgmav.msg, &msg);
                            xQueueSend(xQueueRFLINKTX, &msgmav, 0);
                        }
                        /* Send few messages and exit, other will be send at the next time */
                        if (msg_cnt >= PROJDEFS_MAVLINK_MSG_CNT)
                            goto fexit;
                    }
                }
            }
fexit:      vTaskDelay(10/portTICK_RATE_MS);
        }
        else
        {
            vTaskDelay(200/portTICK_RATE_MS);
        }
    }
    vTaskDelete(NULL);
}
/* RFLINK stop */

/* Main init procedure */
void prvINITd(void *pvParameters)
{
    PROJDEFS_CONF_ENUM conf_cmd;
    PROJDEFS_LED_QUEUE led_cmd;

    for(;;)
    {
        if (pilot.init_start)
        {
            /* Wait befor start configure */
            vTaskDelay(500/portTICK_RATE_MS);

            /* Read configure data from a SD card */
            conf_cmd = PROJDEFS_CONF_READCFG;
            xQueueSend(xQueueConfd, &conf_cmd, 10/portTICK_RATE_MS);
            vTaskDelay(5/portTICK_RATE_MS);

            /* Configure main pilot structure from a reading data */
            conf_cmd = PROJDEFS_CONF_PILOT;
            xQueueSend(xQueueConfd, &conf_cmd, 10/portTICK_RATE_MS);
            vTaskDelay(5/portTICK_RATE_MS);

            /* power ON I2C1 bus */
            conf_cmd = PROJDEFS_CONF_I2C1_PON;
            xQueueSend(xQueueConfd, &conf_cmd, 10/portTICK_RATE_MS);
            while (pilot.i2c1.pwr != I2C_PWR_ON)
            {
                vTaskDelay(5/portTICK_RATE_MS);
            }
            vTaskDelay(100/portTICK_RATE_MS);

            /* start I2C1 master */
            conf_cmd = PROJDEFS_CONF_I2C1_INIT;
            xQueueSend(xQueueConfd, &conf_cmd, 10/portTICK_RATE_MS);
            while (pilot.i2c1.state != I2C_BUS_IDLE)
            {
                vTaskDelay(5/portTICK_RATE_MS);
            }
            vTaskDelay(100/portTICK_RATE_MS);

            /* Configure the LSM303DLHC ACC/MAG sensor */
            conf_cmd = PROJDEFS_CONF_LSM303DLHC;
            xQueueSend(xQueueConfd, &conf_cmd, 10/portTICK_RATE_MS);
            vTaskDelay(5/portTICK_RATE_MS);

            /* Configure the L3G4200D HYR sensor */
            conf_cmd = PROJDEFS_CONF_L3G4200D;
            xQueueSend(xQueueConfd, &conf_cmd, 10/portTICK_RATE_MS);
            vTaskDelay(5/portTICK_RATE_MS);

            /* Configure the BMP085 BAR sensor */
            conf_cmd = PROJDEFS_CONF_BMP085;
            xQueueSend(xQueueConfd, &conf_cmd, 10/portTICK_RATE_MS);
            vTaskDelay(5/portTICK_RATE_MS);

            /* Configure the hardware PWM channels */
            conf_cmd = PROJDEFS_CONF_HPWM;
            xQueueSend(xQueueConfd, &conf_cmd, 10/portTICK_RATE_MS);
            vTaskDelay(5/portTICK_RATE_MS);

            /* Configure the PPM decoder */
            conf_cmd = PROJDEFS_CONF_PPM_DECODER;
            xQueueSend(xQueueConfd, &conf_cmd, 10/portTICK_RATE_MS);
            vTaskDelay(5/portTICK_RATE_MS);

            /* Configure the RF-link */
            conf_cmd = PROJDEFS_CONF_RFLINK;
            xQueueSend(xQueueConfd, &conf_cmd, 10/portTICK_RATE_MS);
            vTaskDelay(5/portTICK_RATE_MS);

            /* All subsystem inited OK */
            pilot.init_start = 0;
            /* Set short blink mode on the LIVE LED */
            led_cmd.all_data = 0;
            led_cmd.led_num = PROJDEFS_LED_LIVE;
            led_cmd.type = PROJDEFS_LED_PULSE;
            xQueueSend(xQueueLEDs, &led_cmd, 10/portTICK_RATE_MS);

            pilot.mav_system_state = MAV_STATE_STANDBY;
        }
        else
        {
            vTaskDelay(200/portTICK_RATE_MS);
        }

    }
    vTaskDelete(NULL);
}

/* System service for read slow date from the sensors */
void prvSENSORSd(void *pvParameters)
{
    static I2C_DRIVER_RW_PKT i2c_cmd;
    static I2C_DRIVER_RW_RESULT i2c_res;
    static SENS_I2C_QUEUE_STRUCT i2c_sens;

    for(;;)
    {
        if (pilot.i2c1.data_flow == I2C_DF_WORK)
        {
            if (pilot.init_start == 0)
            {
                if (pilot.acc_mag.time_t < ulLOCALTIME)
                {
                    /* Refresh next time for read temperature */
                    pilot.acc_mag.time_t = ulLOCALTIME + pilot.acc_mag.time_t_step;
                    /* Read temperature from the LSM303DLHC */
                    i2c_cmd.port = I2C1;
                    i2c_cmd.addr = LSM303DLHC_MAG_I2C_ADDR;
                    i2c_cmd.rw_fl = 0;
                    i2c_cmd.reg = LSM303DLHC_MAG_TEMP_H;
                    i2c_cmd.data_cnt = 1;
                    i2c_cmd.uid = ulLOCALTIME;
                    xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                    vProjdefsI2C1Start();
                    xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                    if ((i2c_res.result == I2C_OP_READ_OK) && (i2c_res.uid == i2c_cmd.uid))
                    {
                        i2c_sens.time = i2c_res.uid;
                        i2c_sens.sens_type = SENS_LSM303DLHC_TEMP;
                        i2c_sens.data_cnt = 1;
                        i2c_sens.data[0] = i2c_res.data[0];
                        xQueueSend(xQueueI2C1Sensors, &i2c_sens, 10/portTICK_RATE_MS);
                    }
                }
                if (pilot.hyr.time_t < ulLOCALTIME)
                {
                    /* Refresh next time for read temperature */
                    pilot.hyr.time_t = ulLOCALTIME + pilot.hyr.time_t_step;
                    /* Read temperature from the L3G4200D */
                    i2c_cmd.port = I2C1;
                    i2c_cmd.addr = L3G4200D_HYR_I2C_ADDR;
                    i2c_cmd.rw_fl = 0;
                    i2c_cmd.reg = L3G4200D_HYR_OUT_TEMP;
                    i2c_cmd.data_cnt = 1;
                    i2c_cmd.uid = ulLOCALTIME;
                    xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                    vProjdefsI2C1Start();
                    xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                    if ((i2c_res.result == I2C_OP_READ_OK) && (i2c_res.uid == i2c_cmd.uid))
                    {
                        i2c_sens.time = i2c_res.uid;
                        i2c_sens.sens_type = SENS_L3G4200D_TEMP;
                        i2c_sens.data_cnt = 1;
                        i2c_sens.data[0] = i2c_res.data[0];
                        xQueueSend(xQueueI2C1Sensors, &i2c_sens, 10/portTICK_RATE_MS);
                    }
                }
                /* Start temperature conversion on the BMP085 */
                i2c_cmd.port = I2C1;
                i2c_cmd.addr = BMP085_I2C_ADDR;
                i2c_cmd.rw_fl = 1;
                i2c_cmd.data[0] = BMP085_TEMP;
                i2c_cmd.reg = BMP085_REG_CTRL;
                i2c_cmd.data_cnt = 1;
                i2c_cmd.uid = ulLOCALTIME;
                xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                vProjdefsI2C1Start();
                xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                if ((i2c_res.result == I2C_OP_WRITE_OK) && (i2c_res.uid == i2c_cmd.uid))
                {
                    vTaskDelay(5/portTICK_RATE_MS);
                    /* Read temperature from the BMP085 */
                    i2c_cmd.port = I2C1;
                    i2c_cmd.addr = BMP085_I2C_ADDR;
                    i2c_cmd.rw_fl = 0;
                    i2c_cmd.reg = BMP085_REG_ADC_H;
                    i2c_cmd.data_cnt = 2;
                    i2c_cmd.uid = ulLOCALTIME;
                    xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                    vProjdefsI2C1Start();
                    xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                    if ((i2c_res.result == I2C_OP_READ_OK) && (i2c_res.uid == i2c_cmd.uid))
                    {
                        i2c_sens.time = i2c_res.uid;
                        i2c_sens.sens_type = SENS_BMP085_TEMP;
                        i2c_sens.data_cnt = 2;
                        i2c_sens.data[0] = i2c_res.data[0];
                        i2c_sens.data[1] = i2c_res.data[1];
                        xQueueSend(xQueueI2C1Sensors, &i2c_sens, 10/portTICK_RATE_MS);
                        /* Start pressure conversion on the BMP085 */
                        i2c_cmd.port = I2C1;
                        i2c_cmd.addr = BMP085_I2C_ADDR;
                        i2c_cmd.rw_fl = 1;
                        i2c_cmd.data[0] = (0x34 + (pilot.bar.oss << 6));
                        i2c_cmd.reg = BMP085_REG_CTRL;
                        i2c_cmd.data_cnt = 1;
                        i2c_cmd.uid = ulLOCALTIME;
                        xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                        vProjdefsI2C1Start();
                        xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                        if ((i2c_res.result == I2C_OP_WRITE_OK) && (i2c_res.uid == i2c_cmd.uid))
                        {
                            vTaskDelay((2 + (3 << pilot.bar.oss))/portTICK_RATE_MS);
                            /* Read pressure from the BMP085 */
                            i2c_cmd.port = I2C1;
                            i2c_cmd.addr = BMP085_I2C_ADDR;
                            i2c_cmd.rw_fl = 0;
                            i2c_cmd.reg = BMP085_REG_ADC_H;
                            i2c_cmd.data_cnt = 3;
                            i2c_cmd.uid = ulLOCALTIME;
                            xQueueSend(xQueueI2CRWPKT, &i2c_cmd, 10/portTICK_RATE_MS);
                            vProjdefsI2C1Start();
                            xQueueReceive(xQueueI2CRWResult, &i2c_res, 10/portTICK_RATE_MS);
                            if ((i2c_res.result == I2C_OP_READ_OK) && (i2c_res.uid == i2c_cmd.uid))
                            {
                                i2c_sens.time = i2c_res.uid;
                                i2c_sens.sens_type = SENS_BMP085_PRESS;
                                i2c_sens.data_cnt = 3;
                                i2c_sens.data[0] = i2c_res.data[0];
                                i2c_sens.data[1] = i2c_res.data[1];
                                i2c_sens.data[2] = i2c_res.data[2];
                                xQueueSend(xQueueI2C1Sensors, &i2c_sens, 10/portTICK_RATE_MS);
                            }
                        }
                    }
                }
                vTaskDelay(10/portTICK_RATE_MS);
            }
            else
            {
                vTaskDelay(500/portTICK_RATE_MS);
            }
        }
        else
        {
            vTaskDelay(100/portTICK_RATE_MS);
        }
    }
    vTaskDelete(NULL);
}

/*
 * 
 */
int main(void) {
    PROJDEFS_LED_QUEUE led_cmd;
    portBASE_TYPE xResult;
    const char start_msg[] = "Pi-Pilot started...\r\n";
    const char shed_msg[] = "Scheduler start\r\n";

    prvSetupHardware();

    /* Register Queues */
    /* Queue for LEDs */
    xQueueLEDs = xQueueCreate(16, sizeof(PROJDEFS_LED_QUEUE));
    if (xQueueLEDs == NULL)
    {
        /* ON GPS led for show error no Queue for LEDs */
        LED_GPS_ON;
    }
    else
        pilot.init_queues_state &= ~(1 << LOGD_INIT_QUEUE_LED);

    /* Queue for configure task */
    xQueueConfd = xQueueCreate(8, sizeof(BYTE));
    /* Blink at once of ARM LED when error */
    if (xQueueConfd == NULL)
        vProjdefsARMblink();
    else
        pilot.init_queues_state &= ~(1 << LOGD_INIT_QUEUE_CONFD);

    /* Queue for message system */
    xQueueMessage = xQueueCreate(LOGD_MESSAGE_QUEUE_SIZE, sizeof(LOGD_MESSAGE_STRUCT));
    /* Blink at once of ARM LED when error */
    if (xQueueMessage == NULL)
        vProjdefsARMblink();
    else
        pilot.init_queues_state &= ~(1 << LOGD_INIT_QUEUE_MSG);

    /* Queue for I2C1 sensors */
    xQueueI2C1Sensors = xQueueCreate(SENS_I2C_QUEUE_DEEP, sizeof(SENS_I2C_QUEUE_STRUCT));
    /* Blink at once of ARM LED when error */
    if (xQueueI2C1Sensors == NULL)
        vProjdefsARMblink();
    else
        pilot.init_queues_state &= ~(1 << LOGD_INIT_QUEUE_I2C1);

    /* Queue for I2C read/write CMD */
    xQueueI2CRWPKT = xQueueCreate(I2C_DRIVER_PKT_DEEP, sizeof(I2C_DRIVER_RW_PKT));
    /* Blink at once of ARM LED when error */
    if (xQueueI2CRWPKT == NULL)
        vProjdefsARMblink();
    else
        pilot.init_queues_state &= ~(1 << LOGD_INIT_QUEUE_I2CRWPKT);

    /* Queue for I2C read/write CMD result */
    xQueueI2CRWResult = xQueueCreate(I2C_DRIVER_PKT_RES_DEEP,
            sizeof(I2C_DRIVER_RW_RESULT));
    /* Blink at once of ARM LED when error */
    if (xQueueI2CRWResult == NULL)
        vProjdefsARMblink();
    else
        pilot.init_queues_state &= ~(1 << LOGD_INIT_QUEUE_I2CRWRES);

    /* Queue the signals of the FLY FSM */
    xQueueFLYSIG = xQueueCreate(PROJDEFS_FLYSIG_DEEP,
            sizeof(PROJDEFS_FLYFSM_SIG_STRUCT));
    /* Blink at once of ARM LED when error */
    if (xQueueFLYSIG == NULL)
        vProjdefsARMblink();
    else
        pilot.init_queues_state &= ~(1 << LOGD_INIT_QUEUE_FLYSIG);

    /* Queue the signals of the AUTOPILOT */
    xQueueAUTOPILOTSIG = xQueueCreate(AUTOPILOT_SIG_DEEP,
            sizeof(AUTOPILOT_SIG_TYPE));
    /* Blink at once of ARM LED when error */
    if (xQueueAUTOPILOTSIG == NULL)
        vProjdefsARMblink();
    else
        pilot.init_queues_state &= ~(1 << LOGD_INIT_QUEUE_AUTOPILOTSIG);

    /* Queue for PPM decoder */
    xQueuePPMDECODER = xQueueCreate(PPM_DRIVER_QUEUE_DEEP,
            sizeof(PPM_DRIVER_CHANNEL_QUEUE));
    /* Blink at once of ARM LED when error */
    if (xQueueAUTOPILOTSIG == NULL)
        vProjdefsARMblink();
    else
        pilot.init_queues_state &= ~(1 << LOGD_INIT_QUEUE_PPM_DECODER);

    /* RFLINK start */
    /* Init Queue for transmit data to ground */
    xQueueRFLINKTX = xQueueCreate(PROJDEFS_CONF_RFLINK_MSG_DEEP,
            sizeof(PROJDEFS_CONF_RFLINK_MSG));
    /* Blink at once of ARM LED when error */
    if (xQueueRFLINKTX == NULL)
        vProjdefsARMblink();
    else
        pilot.init_queues_state &= ~(1 << LOGD_INIT_QUEUE_RFLINK_TX);

    /* Init Queue for receive data from ground */
    xQueueRFLINKRX = xQueueCreate(PROJDEFS_CONF_RFLINK_MSG_DEEP,
            sizeof(PROJDEFS_CONF_RFLINK_MSG));
    /* Blink at once of ARM LED when error */
    if (xQueueRFLINKRX == NULL)
        vProjdefsARMblink();
    else
        pilot.init_queues_state &= ~(1 << LOGD_INIT_QUEUE_RFLINK_RX);
    /* RFLINK stop */

    vLogdPrintROM(LOGD_INFO, start_msg, 10);

    /* Register tasks */
    /* The main task for LEDs */
    xResult = xTaskCreate( prvLEDSd, ( signed char * ) "prvLEDSd",
            configMINIMAL_STACK_SIZE, NULL, 1, NULL);
    if (xResult == pdTRUE)
        pilot.init_tasks_state &= ~(1 << LOGD_INIT_TASK_LED);

    /* Configure service task */
    xResult = xTaskCreate( prvCONFd, ( signed char * ) "prvCONFd",
            configMINIMAL_STACK_SIZE, NULL, 1, NULL);
    if (xResult == pdTRUE)
        pilot.init_tasks_state &= ~(1 << LOGD_INIT_TASK_CONF);

    /* Configure message task */
    xResult = xTaskCreate( prvMESSAGEd, ( signed char * ) "prvMESSAGEd",
            configMINIMAL_STACK_SIZE, NULL, 1, NULL);
    if (xResult == pdTRUE)
        pilot.init_tasks_state &= ~(1 << LOGD_INIT_TASK_MSG);

    /* Configure calculate task */
    xResult = xTaskCreate( prvCALCd, ( signed char * ) "prvCALCd",
            configMINIMAL_STACK_SIZE, NULL, 4, NULL);
    if (xResult == pdTRUE)
        pilot.init_tasks_state &= ~(1 << LOGD_INIT_TASK_CALC);

    /* USB-serial console service */
    xResult = xTaskCreate( prvCONSOLEd, ( signed char * ) "prvCONSOLEd",
            configMINIMAL_STACK_SIZE, NULL, 1, NULL);
    if (xResult == pdTRUE)
        pilot.init_tasks_state &= ~(1 << LOGD_INIT_TASK_CONSOLE);

    /* Main INIT procedure */
    xResult = xTaskCreate( prvINITd, ( signed char * ) "prvINITd",
            configMINIMAL_STACK_SIZE << 1, NULL, 1, NULL);
    if (xResult == pdTRUE)
        pilot.init_tasks_state &= ~(1 << LOGD_INIT_TASK_INIT);

    /* Service task for read slow data from a sensors like temperature and pressure */
    xResult = xTaskCreate( prvSENSORSd, ( signed char * ) "prvSENSORSd",
            configMINIMAL_STACK_SIZE << 1, NULL, 3, NULL);
    if (xResult == pdTRUE)
        pilot.init_tasks_state &= ~(1 << LOGD_INIT_SENS_SLOW_DATA);

    /* FLY FSM service task */
    xResult = xTaskCreate( prvFLYd, ( signed char * ) "prvFLYd",
            configMINIMAL_STACK_SIZE, NULL, 2, NULL);
    if (xResult == pdTRUE)
        pilot.init_tasks_state &= ~(1 << LOGD_INIT_TASK_FLY);

    /* PPM service task */
    xResult = xTaskCreate( prvPPMd, ( signed char * ) "prvPPMd",
            configMINIMAL_STACK_SIZE, NULL, 3, NULL);
    if (xResult == pdTRUE)
        pilot.init_tasks_state &= ~(1 << LOGD_INIT_TASK_PPM);

    /* RFLINK start */
    /* Service task for transmit data to ground */
    xResult = xTaskCreate( prvRFLINKTXd, ( signed char * ) "prvRFLINKTXd",
            configMINIMAL_STACK_SIZE, NULL, 1, NULL);
    if (xResult == pdTRUE)
        pilot.init_tasks_state &= ~(1 << LOGD_INIT_TASK_RFLINK_TX);

    /* Service task for receive data from ground */
    xResult = xTaskCreate( prvRFLINKRXd, ( signed char * ) "prvRFLINKRXd",
            configMINIMAL_STACK_SIZE, NULL, 1, NULL);
    if (xResult == pdTRUE)
        pilot.init_tasks_state &= ~(1 << LOGD_INIT_TASK_RFLINK_RX);

    /* Service task for make and send MAVLINK messages  */
    xResult = xTaskCreate( prvMAVLINKd, ( signed char * ) "prvMAVLINKd",
            configMINIMAL_STACK_SIZE, NULL, 1, NULL);
     if (xResult == pdTRUE)
        pilot.init_tasks_state &= ~(1 << LOGD_INIT_TASK_MAVLINKD);
    /* RFLINK stop */

    /* Set short blink mode on the LIVE LED */
    led_cmd.all_data = 0;
    led_cmd.led_num = PROJDEFS_LED_LIVE;
    led_cmd.type = PROJDEFS_LED_0_1;
    xQueueSend(xQueueLEDs, &led_cmd, 0);

    vLogdPrintROM(LOGD_INFO, shed_msg, 10);

    /* Start the scheduler. */
    vTaskStartScheduler();

    for( ;; );

    return (EXIT_SUCCESS);
}

/*******************************************************************
 * Function:        BOOL USER_USB_CALLBACK_EVENT_HANDLER(
 *                        USB_EVENT event, void *pdata, WORD size)
 *
 * PreCondition:    None
 *
 * Input:           USB_EVENT event - the type of event
 *                  void *pdata - pointer to the event data
 *                  WORD size - size of the event data
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        This function is called from the USB stack to
 *                  notify a user application that a USB event
 *                  occured.  This callback is in interrupt context
 *                  when the USB_INTERRUPT option is selected.
 *
 * Note:            None
 *******************************************************************/
BOOL USER_USB_CALLBACK_EVENT_HANDLER(USB_EVENT event, void *pdata, WORD size)
{
    switch(event)
    {
        case EVENT_CONFIGURED:
            CDCInitEP();
            break;
        case EVENT_EP0_REQUEST:
            USBCheckCDCRequest();
            break;
        default:
            break;
    }
    return TRUE;
}

/*-----------------------------------------------------------*/


// ****************************************************************************
// ****************************************************************************
// Application Main Entry Point
// ****************************************************************************
// ****************************************************************************

static void prvSetupHardware( void )
{
    static unsigned char k;

    /* Configure the hardware for maximum performance. */
    vHardwareConfigurePerformance();

    /* Setup to use the external interrupt controller. */
    vHardwareUseMultiVectoredInterrupts();

    portDISABLE_INTERRUPTS();

    /* Init some data structure */
    /* Set default value os Init process */
    pilot.init_start = 0xFFFFFFFF;
    pilot.init_queues_state = 0xFFFFFFFF;
    pilot.init_tasks_state = 0xFFFFFFFF;
    pilot.fly_state = FF_IDLE;
    pilot.toff_mode = TOFF_MANUAL;
    pilot.aircraft = TYPE_PLANER;
    if (pilot.aircraft == TYPE_PLANER)
    {
        /* Init default value of autopilot course angles for planer type */
        pilot.course.pitch = -5.0;
        pilot.course.roll = 0.0;
    }
    pilot.xHandleAutopilot = NULL;
    pilot.acc_catapult = 0.2;
    /* Init I2C data structure */
    /* I2C1 */
    pilot.i2c1.pwr = I2C_PWR_OFF;
    pilot.i2c1.freq = I2C1_CLOCK_FREQ;
    pilot.i2c1.state = I2C_BUS_NONE;
    pilot.i2c1.pkt = I2C_PKT_NONE;
    pilot.i2c1.irq_cnt = 0;
    pilot.i2c1.data_flow = I2C_DF_STOP;
    pilot.i2c1.data_overflow_cnt = 0;
    /* I2C2 */
    pilot.i2c2.pwr = I2C_PWR_OFF;
    pilot.i2c2.freq = I2C2_CLOCK_FREQ;
    pilot.i2c2.state = I2C_BUS_NONE;
    pilot.i2c2.pkt = I2C_PKT_NONE;
    pilot.i2c2.irq_cnt = 0;
    pilot.i2c2.data_flow = I2C_DF_STOP;
    pilot.i2c2.data_overflow_cnt = 0;
    /* Init default setting for the sensors */
    pilot.sensors_present = MAV_SYS_STATUS_SENSOR_3D_GYRO | MAV_SYS_STATUS_SENSOR_3D_ACCEL |
            MAV_SYS_STATUS_SENSOR_3D_MAG | MAV_SYS_STATUS_SENSOR_ABSOLUTE_PRESSURE |
            MAV_SYS_STATUS_SENSOR_MOTOR_OUTPUTS | MAV_SYS_STATUS_SENSOR_RC_RECEIVER;
    pilot.sensors_enabled = 0;
    pilot.sensors_health = 0;
    /* BMP085 */
    pilot.bar.oss = BMP085_OSS_3;
    pilot.bar.state = BMP085_STOP;
    pilot.bar.mcnt = 0;
    /* Pressure at sea level */
    pilot.p0 = 101325;
    /* HYR L3G4200D */
    pilot.hyr.data_rate = L3G4200D_HYR_RATE_100;
    pilot.hyr.bandwidth = L3G4200D_HYR_BW_HIGH;
    pilot.hyr.scale = L3G4200D_HYR_2000;
    pilot.hyr.state = L3G4200D_STOP;
    pilot.hyr.mcnt = 0;
    pilot.hyr.time_t = 0;
    pilot.hyr.time_t_step = 1000;
    /* ACC LSM303DLHC */
    pilot.acc_mag.acc_rate = LSM303DLHC_ACC_RATE_100;
    pilot.acc_mag.acc_scale = LSM303DLHC_ACC_2G;
    pilot.acc_mag.acc_mcnt = 0;
    pilot.acc_mag.mag_rate = LSM303DLHC_MAG_RATE_75;
    pilot.acc_mag.mag_gain = LSM303DLHC_MAG_GAIN_1_3;
    pilot.acc_mag.mag_mcnt = 0;
    pilot.acc_mag.state = LSM303DLHC_STOP;
    pilot.acc_mag.time_t = 0;
    pilot.acc_mag.time_t_step = 1000;
    /* Init altitude value */
    pilot.altitude = 0;
    pilot.ground_altitude = 0;
    /* Init sensors time */
    pilot.sensors_time = 0;
    /* Init complimentary filter */
    pilot.cf_acc_def = PROJDEFS_CONST_ACC_CF;
    pilot.cf_hyr_def = PROJDEFS_CONST_HYR_CF;
    /* Init default state for quaternions */
    pilot.q1 = 1.0;
    pilot.q2 = 0.0;
    pilot.q3 = 0.0;
    pilot.q4 = 0.0;
    /* Init default PID value */
    /* for PID of ROLL */
    pilot.roll_pid.Kp_gain = PID_ROLL_Kp;
    pilot.roll_pid.Ki_gain = PID_ROLL_Ki;
    pilot.roll_pid.Kd_gain = PID_ROLL_Kd;
    pilot.roll_pid.Ki_max  = PID_ROLL_Ki_max;
    pilot.roll_pid.err_prev = 0;
    pilot.roll_pid.int_prev = 0;
    /* for PID of PITCH */
    pilot.pitch_pid.Kp_gain = PID_PITCH_Kp;
    pilot.pitch_pid.Ki_gain = PID_PITCH_Ki;
    pilot.pitch_pid.Kd_gain = PID_PITCH_Kd;
    pilot.pitch_pid.Ki_max  = PID_PITCH_Ki_max;
    pilot.pitch_pid.err_prev = 0;
    pilot.pitch_pid.int_prev = 0;
    /* for PID of YAW */
    pilot.yaw_pid.Kp_gain = PID_YAW_Kp;
    pilot.yaw_pid.Ki_gain = PID_YAW_Ki;
    pilot.yaw_pid.Kd_gain = PID_YAW_Kd;
    pilot.yaw_pid.Ki_max  = PID_YAW_Ki_max;
    pilot.yaw_pid.err_prev = 0;
    pilot.yaw_pid.int_prev = 0;
    /* for PID of Altitude */
    pilot.alt_pid.Kp_gain = PID_ALT_Kp;
    pilot.alt_pid.Ki_gain = PID_ALT_Ki;
    pilot.alt_pid.Kd_gain = PID_ALT_Kd;
    pilot.alt_pid.Ki_max  = PID_ALT_Ki_max;
    pilot.alt_pid.err_prev = 0;
    pilot.alt_pid.int_prev = 0;
    /* Configure RFLINK */
    pilot.rflink_state.tx_busy = 0;
    pilot.rflink.uart_cfg = UART_ENABLE_PINS_TX_RX_ONLY;
    pilot.rflink.uart_ctrl = UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1;
    pilot.rflink.uart_fifo = UART_INTERRUPT_ON_TX_BUFFER_EMPTY | UART_INTERRUPT_ON_RX_NOT_EMPTY;
    pilot.rflink.uart_flags = UART_ENABLE | UART_PERIPHERAL | UART_RX | UART_TX;
    pilot.rflink.uart_port = RFLINK_UART;
    pilot.rflink.uart_rate = RFLINK_BAUD;
    pilot.rflink_rx = RFLINK_RX_WAIT;
    /* Configure MAVLINK */
    pilot.mav_packet_rx_drops = 0;
    pilot.mav_packet_rx_ok = 0;


    /* Init statistics counters */
    stats.idle_cnt = 0;
    stats.idle_period_cnt = 0;

    /* Init generic GPIOs */
    vProjdefsInitGPIO();

//	The USB specifications require that USB peripheral devices must never source
//	current onto the Vbus pin.  Additionally, USB peripherals should not source
//	current on D+ or D- when the host/hub is not actively powering the Vbus line.
//	When designing a self powered (as opposed to bus powered) USB peripheral
//	device, the firmware should make sure not to turn on the USB module and D+
//	or D- pull up resistor unless Vbus is actively powered.  Therefore, the
//	firmware needs some means to detect when Vbus is being powered by the host.
//	A 5V tolerant I/O pin can be connected to Vbus (through a resistor), and
// 	can be used to detect when Vbus is high (host actively powering), or low
//	(host is shut down or otherwise not supplying power).  The USB firmware
// 	can then periodically poll this I/O pin to know when it is okay to turn on
//	the USB module/D+/D- pull up resistor.  When designing a purely bus powered
//	peripheral device, it is not possible to source current on D+ or D- when the
//	host is not actively providing power on Vbus. Therefore, implementing this
//	bus sense feature is optional.  This firmware can be made to use this bus
//	sense feature by making sure "USE_USB_BUS_SENSE_IO" has been defined in the
//	HardwareProfile.h file.
    #if defined(USE_USB_BUS_SENSE_IO)
    tris_usb_bus_sense = INPUT_PIN; // See HardwareProfile.h
    #endif

//	If the host PC sends a GetStatus (device) request, the firmware must respond
//	and let the host know if the USB peripheral device is currently bus powered
//	or self powered.  See chapter 9 in the official USB specifications for details
//	regarding this request.  If the peripheral device is capable of being both
//	self and bus powered, it should not return a hard coded value for this request.
//	Instead, firmware should check if it is currently self or bus powered, and
//	respond accordingly.  If the hardware has been configured like demonstrated
//	on the PICDEM FS USB Demo Board, an I/O pin can be polled to determine the
//	currently selected power source.  On the PICDEM FS USB Demo Board, "RA2"
//	is used for	this purpose.  If using this feature, make sure "USE_SELF_POWER_SENSE_IO"
//	has been defined in HardwareProfile - (platform).h, and that an appropriate I/O pin
//  has been mapped	to it.
    #if defined(USE_SELF_POWER_SENSE_IO)
    tris_self_power = INPUT_PIN;	// See HardwareProfile.h
    #endif

    USBDeviceInit();	//usb_device.c.  Initializes USB module SFRs and firmware
    					//variables to known states.
}
/*-----------------------------------------------------------*/

void vApplicationMallocFailedHook( void )
{
	/* vApplicationMallocFailedHook() will only be called if
	configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h.  It is a hook
	function that will get called if a call to pvPortMalloc() fails.
	pvPortMalloc() is called internally by the kernel whenever a task, queue,
	timer or semaphore is created.  It is also called by various parts of the
	demo application.  If heap_1.c or heap_2.c are used, then the size of the
	heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
	FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
	to query the size of free heap space that remains (although it does not
	provide information on how the remaining heap might be fragmented). */
	taskDISABLE_INTERRUPTS();
	for( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationIdleHook( void )
{
	/* vApplicationIdleHook() will only be called if configUSE_IDLE_HOOK is set
	to 1 in FreeRTOSConfig.h.  It will be called on each iteration of the idle
	task.  It is essential that code added to this hook function never attempts
	to block in any way (for example, call xQueueReceive() with a block time
	specified, or call vTaskDelay()).  If the application makes use of the
	vTaskDelete() API function (as this demo application does) then it is also
	important that vApplicationIdleHook() is permitted to return to its calling
	function, because it is the responsibility of the idle task to clean up
	memory allocated by the kernel to any task that has since been deleted. */

    LOGD_MESSAGE_STRUCT logmsg;

    /* show CPU free IDLE statistics */
    if (stats.show_fl)
    {
        stats.show_fl = 0;
        logmsg.time = ulLOCALTIME;
        logmsg.flag = LOGD_INFO;
        sprintf(logmsg.message, "IDLE cnt: %d\r\n", stats.idle_value);
        xQueueSend(xQueueMessage, &logmsg, 0);
        /* Debug print input capture RC */
        logmsg.time = ulLOCALTIME;
        logmsg.flag = LOGD_INFO;
        sprintf(logmsg.message, "IN3 : %d : H2 : %d : H3 :%d : S2 : %d\r\n",
                pilot.rc.channel[2], pilot.pwm.hpwm[1], pilot.pwm.hpwm[2], pilot.pwm.spwm[1]);
        xQueueSend(xQueueMessage, &logmsg, 0);
#if 0
        sprintf(logmsg.message, "IN2 : %d : IN3 : %d : IN5 : %d : IN6 : %d\r\n",
                pilot.rc.channel[1], pilot.rc.channel[2], pilot.rc.channel[4], pilot.rc.channel[5]);
        xQueueSend(xQueueMessage, &logmsg, 0);
#endif
#if 0
        sprintf(logmsg.message, "%d %d %d %d %d\r\n",
                pilot.pwm.hpwm_servo_map[0], pilot.pwm.hpwm_servo_map[1], pilot.pwm.hpwm_servo_map[2],
                pilot.pwm.spwm_servo_map[0], pilot.pwm.spwm_servo_map[1]);
        xQueueSend(xQueueMessage, &logmsg, 0);
#endif
#if 0
        sprintf(logmsg.message, "%d %d %d %d %d\r\n", pilot.pwm.hpwm[0],
                pilot.pwm.hpwm[1], pilot.pwm.hpwm[2], pilot.pwm.spwm[0],
                pilot.pwm.spwm[1]);
        xQueueSend(xQueueMessage, &logmsg, 0);
#endif
#if 0
        sprintf(logmsg.message, "%d %d %d %d\r\n", pilot.rc.channel_type[1].type & PPM_DRIVER_CHAN_RC,
                pilot.rc.channel_type[2].type & PPM_DRIVER_CHAN_RC, pilot.rc.channel_type[4].type & PPM_DRIVER_CHAN_SIGNAL,
                pilot.rc.channel_type[5].type & PPM_DRIVER_CHAN_SWITCH);
        xQueueSend(xQueueMessage, &logmsg, 0);
#endif
#if 0
        /* Debug print MEMS sensors data read counter */
        logmsg.time = ulLOCALTIME;
        logmsg.flag = LOGD_INFO;
        sprintf(logmsg.message, "ACC : %d, MAG : %d, HYR : %d\r\n",
                pilot.acc_mag.acc_mcnt, pilot.acc_mag.mag_mcnt, pilot.hyr.mcnt);
        xQueueSend(xQueueMessage, &logmsg, 0);
#endif
    }

    /* CPU free resourse counter */
    stats.idle_cnt++;
#if 1
    /* USB service */
    USBDeviceTasks();
    if((USBDeviceState >= CONFIGURED_STATE)&&(USBSuspendControl!=1))
    {
        CDCTxService();
    }
#endif
}
/*-----------------------------------------------------------*/

void vApplicationStackOverflowHook( xTaskHandle pxTask, signed char *pcTaskName )
{
	( void ) pcTaskName;
	( void ) pxTask;

	/* Run time task stack overflow checking is performed if
	configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook	function is
	called if a task stack overflow is detected.  Note the system/interrupt
	stack is not checked. */
	taskDISABLE_INTERRUPTS();
	for( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationTickHook( void )
{
    /* This function will be called by each tick interrupt if
       configUSE_TICK_HOOK is set to 1 in FreeRTOSConfig.h.  User code can be
       added here, but the tick hook is called from an interrupt context, so
       code must not attempt to block, and only the interrupt safe FreeRTOS API
       functions can be used (those that end in FromISR()). */
    static PROJDEFS_FLYFSM_SIG_STRUCT flysig;

    ulLOCALTIME++;

    /* CPU free resourse counter some code */
    stats.idle_period_cnt++;
    /* show idle stat every 1 sec. */
    if (stats.idle_period_cnt >= 1000)
    {
        stats.idle_value = stats.idle_cnt;
        stats.show_fl = 1;
        stats.idle_period_cnt = 0;
        stats.idle_cnt = 0;
        /* RFLINK start */
        pilot.mcu_load = (unsigned short)(1000 - stats.idle_value / PROJDEFS_CONF_MCU_FREE);
        /* Mavlink */
        if (pilot.mav_msg.link_state)
        {
            pilot.mav_msg.timeout_cnt++;
            if (pilot.mav_msg.timeout_cnt > PROJDEFS_MAVLINK_TIMEOUT)
            {
                pilot.mav_msg.link_state = 0;
                pilot.mav_msg.link_prev_state = 1;
                flysig.signal = SIG_CMD_MAVLINK;
                flysig.on = 0;
                xQueueSend(xQueueFLYSIG, &flysig, 10/portTICK_RATE_MS);
            }
        }
        /* RFLINK stop */
    }
}
/*-----------------------------------------------------------*/

void _general_exception_handler( unsigned long ulCause, unsigned long ulStatus )
{
    /* This overrides the definition provided by the kernel.  Other exceptions
    should be handled here. */

    LED_GPS_ON;
    LED_LIVE_OFF;
    LED_COM_OFF;
    LED_ARM_OFF;
    for( ;; );
}

