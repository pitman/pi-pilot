/*
 * File:   config.c
 *
 * Autopilot for UAV
 *
 * Read configuration from the microSD card.
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <p32xxxx.h>
#include <xc.h>
#include <sys/attribs.h>
#include <math.h>

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "croutine.h"
#include "timers.h"
#include "queue.h"

/* Hardware specific includes. */
#include "ConfigPerformance.h"
#include "i2c.h"
#include "spi.h"
#include "usb.h"
#include "usb_function_cdc.h"
#include "range_meters.h"
#include "pid.h"
#include "bmp085.h"
#include "lsm303dlhc.h"
#include "l3g4200d.h"
#include "pwm-driver.h"
#include "ppm-driver.h"

/* Project setting includes */
#include "projdefs.h"
#include "config.h"

/* Read data from a config file */
void vCONFIG_READ_DATA(volatile PROJDEFS_PIPILOT_CFG * cfg)
{
    unsigned int k, n;

    /* TODO: Hardcoded config value for test only. Remove start */
    cfg->toff_mode = TOFF_MANUAL;
    cfg->acc_catapult = 0.2;
    cfg->aircraft = TYPE_PLANER;
    cfg->bar.oss = BMP085_OSS_3;
    cfg->acc_mag.acc_rate = LSM303DLHC_ACC_RATE_100;
    cfg->acc_mag.acc_scale = LSM303DLHC_ACC_2G;
    cfg->acc_mag.mag_gain = LSM303DLHC_MAG_GAIN_1_3;
    cfg->acc_mag.mag_rate = LSM303DLHC_MAG_RATE_75;
    cfg->hyr.data_rate = L3G4200D_HYR_RATE_100;
    cfg->hyr.bandwidth = L3G4200D_HYR_BW_HIGH;
    cfg->hyr.scale = L3G4200D_HYR_2000;
    cfg->cf_acc_def = PROJDEFS_CONST_ACC_CF;
    cfg->cf_hyr_def = PROJDEFS_CONST_HYR_CF;
        /* Set default value for all PWMs */
    for (k = 0; k < PWM_DRIVER_HPWM_CNT; k++)
    {
        cfg->pwm.hpwm_type[k] = TYPE_NONE;
        cfg->pwm.hpwm_min[k] = PWM_DRIVER_HPWM_MIN_VAL;
        cfg->pwm.hpwm_max[k] = PWM_DRIVER_HPWM_MAX_VAL;
        cfg->pwm.hpwm_servo_mode[k] = 1;
        cfg->pwm.hpwm_servo_map[k] = PPM_DRIVER_CHAN_NMAP;
    }
    for (k = 0; k < PWM_DRIVER_SPWM_CNT; k++)
    {
        cfg->pwm.spwm_type[k] = TYPE_0_100;
        cfg->pwm.spwm_min[k] = PWM_DRIVER_SPWM_MIN_VAL;
        cfg->pwm.spwm_max[k] = PWM_DRIVER_SPWM_MAX_VAL;
        cfg->pwm.spwm_servo_mode[k] = 1;
        cfg->pwm.spwm_servo_map[k] = PPM_DRIVER_CHAN_NMAP;
    }
    /* Set default type of servos for my planer */
    cfg->pwm.hpwm_type[AUTOPILOT_PLANER_AILERON_LEFT] = TYPE_50;
    cfg->pwm.hpwm_servo_mode[AUTOPILOT_PLANER_AILERON_LEFT] = -1;
    cfg->pwm.hpwm_type[AUTOPILOT_PLANER_AILERON_RIGHT] = TYPE_50;
    cfg->pwm.hpwm_type[AUTOPILOT_PLANER_ELEVATOR] = TYPE_50;
    cfg->pwm.hpwm_servo_mode[AUTOPILOT_PLANER_ELEVATOR] = -1;
    cfg->pwm.hpwm_type[AUTOPILOT_PLANER_RUDDER] = TYPE_50;
#if 0
    cfg->pwm.hpwm_type[0] = TYPE_MAP_STATIC;
    cfg->pwm.hpwm_servo_map[0] = 1;
    cfg->pwm.hpwm_type[1] = TYPE_MAP_DYNAMIC;
    cfg->pwm.hpwm_type[2] = TYPE_MAP_DYNAMIC;

    cfg->pwm.spwm_type[0] = TYPE_MAP_STATIC;
    cfg->pwm.spwm_servo_map[0] = 1;
    cfg->pwm.spwm_type[1] = TYPE_MAP_DYNAMIC;
#endif
    /* Set RC type */
    for (k = 0; k < PPM_DRIVER_INPUT_LINES; k++)
    {
        cfg->rc.channel_type[k].data = 0;
    }
    for (k = 0; k < PPM_DRIVER_INPUT_SIGNALS; k++)
    {
       cfg->rc.channel_signal[k].enable = 0;
    }
    for (k = 0; k < PPM_DRIVER_INPUT_SWITCHES; k++)
    {
        cfg->rc.channel_switch[k].enable = 0;
        for (n = 0; n < PWM_DRIVER_HPWM_CNT; n++)
        {
            cfg->rc.channel_switch[k].hpwm_use[n] = PPM_DRIVER_CHAN_OFF;
        }
        for (n = 0; n < PWM_DRIVER_SPWM_CNT; n++)
        {
            cfg->rc.channel_switch[k].spwm_use[n] = PPM_DRIVER_CHAN_OFF;
        }
    }
    //cfg->rc.rc_type = RC_TYPE_PPM8;
    cfg->rc.rc_type = RC_TYPE_PPM8;
    cfg->rc.channel_type[1].enable = PPM_DRIVER_CHAN_ON;
    cfg->rc.channel_type[1].type = PPM_DRIVER_CHAN_RC;
    cfg->rc.channel_type[2].enable = PPM_DRIVER_CHAN_ON;
    cfg->rc.channel_type[2].type = PPM_DRIVER_CHAN_RC;
    cfg->rc.channel_type[4].enable = PPM_DRIVER_CHAN_ON;
    cfg->rc.channel_type[4].type = PPM_DRIVER_CHAN_SIGNAL;
#if 0
    cfg->rc.channel_type[5].enable = PPM_DRIVER_CHAN_ON;
    cfg->rc.channel_type[5].type = PPM_DRIVER_CHAN_SWITCH;
#endif
    for (k = 0; k < PPM_DRIVER_INPUT_LINES; k++)
    {
        cfg->rc.channel_min[k] = 2800;
        cfg->rc.channel_max[k] = 4800;
    }
    /* Configure SIGNAL */
    cfg->rc.channel_signal[0].state = 0;
    cfg->rc.channel_signal[0].prev_state = 0;
    cfg->rc.channel_signal[0].channel = 4;
    cfg->rc.channel_signal[0].enable = 1;
    cfg->rc.channel_signal[0].signal = SIG_CMD_BUTTON;
    /* Configure SWITCH */
#if 0
    cfg->rc.channel_switch[0].enable = 1;
    cfg->rc.channel_switch[0].state = 0;
    cfg->rc.channel_switch[0].prev_state = 0;
    cfg->rc.channel_switch[0].channel = 5;
#endif
    for (k = 0; k < PWM_DRIVER_HPWM_CNT; k++)
    {
        cfg->rc.channel_switch[0].hpwm_map_on[k] = PPM_DRIVER_CHAN_NMAP;
        cfg->rc.channel_switch[0].hpwm_map_off[k] = PPM_DRIVER_CHAN_NMAP;
    }
#if 0
    cfg->rc.channel_switch[0].hpwm_use[1] = PPM_DRIVER_CHAN_ON;
    cfg->rc.channel_switch[0].hpwm_use[2] = PPM_DRIVER_CHAN_ON;
    cfg->rc.channel_switch[0].hpwm_map_off[1] = 2;
    cfg->rc.channel_switch[0].hpwm_map_on[2] = 2;
#endif
    for (k = 0; k < PWM_DRIVER_SPWM_CNT; k++)
    {
        cfg->rc.channel_switch[0].spwm_map_on[k] = PPM_DRIVER_CHAN_NMAP;
        cfg->rc.channel_switch[0].spwm_map_off[k] = PPM_DRIVER_CHAN_NMAP;
    }
#if 0
    cfg->rc.channel_switch[0].spwm_use[1] = PPM_DRIVER_CHAN_ON;
    cfg->rc.channel_switch[0].spwm_map_on[1] = 2;
#endif
    /* Set RC channel of decode period */
    
    /* for PID of ROLL */
    cfg->roll_pid.Kp_gain = PID_ROLL_Kp;
    cfg->roll_pid.Ki_gain = PID_ROLL_Ki;
    cfg->roll_pid.Kd_gain = PID_ROLL_Kd;
    cfg->roll_pid.Ki_max  = PID_ROLL_Ki_max;
    /* for PID of PITCH */
    cfg->pitch_pid.Kp_gain = PID_PITCH_Kp;
    cfg->pitch_pid.Ki_gain = PID_PITCH_Ki;
    cfg->pitch_pid.Kd_gain = PID_PITCH_Kd;
    cfg->pitch_pid.Ki_max  = PID_PITCH_Ki_max;
    /* for PID of YAW */
    cfg->yaw_pid.Kp_gain = PID_YAW_Kp;
    cfg->yaw_pid.Ki_gain = PID_YAW_Ki;
    cfg->yaw_pid.Kd_gain = PID_YAW_Kd;
    cfg->yaw_pid.Ki_max  = PID_YAW_Ki_max;
    /* for PID of Altitude */
    cfg->alt_pid.Kp_gain = PID_ALT_Kp;
    cfg->alt_pid.Ki_gain = PID_ALT_Ki;
    cfg->alt_pid.Kd_gain = PID_ALT_Kd;
    cfg->alt_pid.Ki_max  = PID_ALT_Ki_max;
    /* TODO: Remove stop  */
    /* RFLINK config start */
    cfg->rflink_enable = 1;
    /* RFLINK config stop */
    /* MAVLINK config start */
    cfg->mav_system_type = MAV_TYPE_FIXED_WING;
    cfg->mav_autopilot_type = MAV_AUTOPILOT_GENERIC;
    cfg->mav_system_mode = MAV_MODE_PREFLIGHT;
    cfg->mav_custom_mode = 0;
    cfg->mav_system_state = MAV_STATE_BOOT;
    cfg->mavlink_system.sysid = 20;
    cfg->mavlink_system.type = MAV_TYPE_FIXED_WING;
    cfg->mav_msg.all_data = 0x00000000;
    cfg->mav_msg.enable = 1;
    cfg->mav_msg.force_data_send = 1;
    cfg->mav_msg.ground_station_id = 255;
}
