/*
 * File:   projdefs.c
 *
 * Autopilot for UAV
 *
 * Common functions
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "projdefs.h"
#include <stdio.h>
#include <stdlib.h>
#include <p32xxxx.h>

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "croutine.h"
#include "timers.h"
#include "queue.h"

#include "peripheral/uart.h"

extern xQueueHandle xQueueLEDs;
/* Main data structure of Pi-Pilot */
extern PROJDEFS_PIPILOT pilot;

/* Init generic GPIOs */
void vProjdefsInitGPIO(void)
{
    /* Disable JTAG port, usefull for some GPIO function */
    DDPCONbits.JTAGEN = 0;

    /* Configure LEDs */
    LED_SET_OUTPUT;
    LED_SET_ODC;
    LED_SET_DEFAULT;
    /* Configure command button */
    BTN_CMD_SET_INPUT;
    /* Configure I2C1 power MOSFET */
    SENS_SW_SET_OUTPUT;
    SENS_SW_SET_ODC;
}

/* Init I2C1 sensors power */
void vProjdefsInitI2C1Power(void)
{
    SENS_ON;
}

/* Fini I2C1 sensors power */
void vProjdefsFiniI2C1Power(void)
{
    SENS_OFF;
}

/* Start I2C1 transaction */
void vProjdefsI2C1Start(void)
{
    switch (pilot.i2c1.data_flow)
    {
    case I2C_DF_WORK:
        return;
        break;
    case I2C_DF_START:
        pilot.i2c1.data_flow = I2C_DF_WORK;
    case I2C_DF_STOP:    
        if (pilot.i2c1.state == I2C_BUS_IDLE)
        {
            /* Start I2C transaction */
            pilot.i2c1.state = I2C_BUS_START;
            I2C1CONbits.SEN = 1;
        }
        else
        {
            return;
        }
    }
}

/* Set blink at once to ARM LED*/
void vProjdefsARMblink(void)
{
    PROJDEFS_LED_QUEUE led_cmd;

    led_cmd.led_num = PROJDEFS_LED_ARM;
    led_cmd.type = PROJDEFS_LED_BLINK;
    led_cmd.on = 0;
    xQueueSend(xQueueLEDs, &led_cmd, 0);
}

/* Set short blink to ARM LED*/
void vProjdefsARMshortblink(void)
{
    PROJDEFS_LED_QUEUE led_cmd;

    led_cmd.led_num = PROJDEFS_LED_ARM;
    led_cmd.type = PROJDEFS_LED_0_1;
    led_cmd.on = 0;
    xQueueSend(xQueueLEDs, &led_cmd, 0);
}

/* compute the 2's compliment of int value val */
short sProjdefs2Compire(short val, unsigned char bits)
{
    if ((val & (1 << (bits-1))) != 0 )
    {
        val = val - (1<<bits);
    }
    return val;
}

/* Configure main data structure */
void vProjdefsConfigurePilot(volatile PROJDEFS_PIPILOT_CFG * cfg, volatile PROJDEFS_PIPILOT * target)
{
    unsigned int k, n;

    target->toff_mode = cfg->toff_mode;
    target->acc_catapult = cfg->acc_catapult;
    target->aircraft = cfg->aircraft;
    target->bar.oss = cfg->bar.oss;
    target->acc_mag.acc_rate = cfg->acc_mag.acc_rate;
    target->acc_mag.acc_scale = cfg->acc_mag.acc_scale;
    target->acc_mag.mag_gain = cfg->acc_mag.mag_gain;
    target->acc_mag.mag_rate = cfg->acc_mag.mag_rate;
    target->hyr.data_rate = cfg->hyr.data_rate;
    target->hyr.bandwidth = cfg->hyr.bandwidth;
    target->hyr.scale = cfg->hyr.scale;
    target->cf_acc_def = cfg->cf_acc_def;
    target->cf_hyr_def = cfg->cf_hyr_def;
    target->sensors_time = cfg->sensors_time;
    for (k = 0; k < PWM_DRIVER_HPWM_CNT; k++)
    {
        target->pwm.hpwm_min[k] = cfg->pwm.hpwm_min[k];
        target->pwm.hpwm_max[k] = cfg->pwm.hpwm_max[k];
        target->pwm.hpwm_null_point[k] = cfg->pwm.hpwm_null_point[k];
        target->pwm.hpwm_servo_mode[k] = cfg->pwm.hpwm_servo_mode[k];
        target->pwm.hpwm_type[k] = cfg->pwm.hpwm_type[k];
        target->pwm.hpwm_servo_map[k] = cfg->pwm.hpwm_servo_map[k];
    }
        for (k = 0; k < PWM_DRIVER_SPWM_CNT; k++)
    {
        target->pwm.spwm_min[k] = cfg->pwm.spwm_min[k];
        target->pwm.spwm_max[k] = cfg->pwm.spwm_max[k];
        target->pwm.spwm_null_point[k] = cfg->pwm.spwm_null_point[k];
        target->pwm.spwm_servo_mode[k] = cfg->pwm.spwm_servo_mode[k];
        target->pwm.spwm_type[k] = cfg->pwm.spwm_type[k];
        target->pwm.spwm_servo_map[k] = cfg->pwm.spwm_servo_map[k];
    }
    /* Configure PPM capture */
    target->rc.rc_type = cfg->rc.rc_type;
    for (k = 0; k < PPM_DRIVER_INPUT_LINES; k++)
    {
        target->rc.channel_type[k].data = cfg->rc.channel_type[k].data;
        target->rc.channel_min[k] = cfg->rc.channel_min[k];
        target->rc.channel_max[k] = cfg->rc.channel_max[k];
        target->rc.channel_range[k] = cfg->rc.channel_max[k] - cfg->rc.channel_min[k];
        target->rc.channel_mid[k] = target->rc.channel_min[k] + (target->rc.channel_range[k] >> 1);
    }
    for (k = 0; k < PPM_DRIVER_INPUT_SIGNALS; k++)
    {
        target->rc.channel_signal[k].channel = cfg->rc.channel_signal[k].channel;
        target->rc.channel_signal[k].signal = cfg->rc.channel_signal[k].signal;
        target->rc.channel_signal[k].enable = cfg->rc.channel_signal[k].enable;
        target->rc.channel_signal[k].state = cfg->rc.channel_signal[k].prev_state;
        target->rc.channel_signal[k].prev_state = cfg->rc.channel_signal[k].prev_state;
    }
    for (k = 0; k < PPM_DRIVER_INPUT_SWITCHES; k++)
    {
        target->rc.channel_switch[k].channel = cfg->rc.channel_switch[k].channel;
        target->rc.channel_switch[k].enable = cfg->rc.channel_switch[k].enable;
        target->rc.channel_switch[k].state = cfg->rc.channel_switch[k].state;
        target->rc.channel_switch[k].prev_state = cfg->rc.channel_switch[k].prev_state;
        for (n = 0; n < PWM_DRIVER_HPWM_CNT; n++)
        {
            target->rc.channel_switch[k].hpwm_map_on[n] = cfg->rc.channel_switch[k].hpwm_map_on[n];
            target->rc.channel_switch[k].hpwm_map_off[n] = cfg->rc.channel_switch[k].hpwm_map_off[n];
            target->rc.channel_switch[k].hpwm_use[n] = cfg->rc.channel_switch[k].hpwm_use[n];
        }
        for (n = 0; n < PWM_DRIVER_SPWM_CNT; n++)
        {
            target->rc.channel_switch[k].spwm_map_on[n] = cfg->rc.channel_switch[k].spwm_map_on[n];
            target->rc.channel_switch[k].spwm_map_off[n] = cfg->rc.channel_switch[k].spwm_map_off[n];
            target->rc.channel_switch[k].spwm_use[n] = cfg->rc.channel_switch[k].spwm_use[n];
        }
    }
    /* Configure PID */
    target->roll_pid.Kp_gain = cfg->roll_pid.Kp_gain;
    target->roll_pid.Ki_gain = cfg->roll_pid.Ki_gain;
    target->roll_pid.Kd_gain = cfg->roll_pid.Kd_gain;
    target->roll_pid.Ki_max = cfg->roll_pid.Ki_max;
    target->pitch_pid.Kp_gain = cfg->pitch_pid.Kp_gain;
    target->pitch_pid.Ki_gain = cfg->pitch_pid.Ki_gain;
    target->pitch_pid.Kd_gain = cfg->pitch_pid.Kd_gain;
    target->pitch_pid.Ki_max = cfg->pitch_pid.Ki_max;
    target->yaw_pid.Kp_gain = cfg->yaw_pid.Kp_gain;
    target->yaw_pid.Ki_gain = cfg->yaw_pid.Ki_gain;
    target->yaw_pid.Kd_gain = cfg->yaw_pid.Kd_gain;
    target->yaw_pid.Ki_max = cfg->yaw_pid.Ki_max;
    target->alt_pid.Kp_gain = cfg->alt_pid.Kp_gain;
    target->alt_pid.Ki_gain = cfg->alt_pid.Ki_gain;
    target->alt_pid.Kd_gain = cfg->alt_pid.Kd_gain;
    target->alt_pid.Ki_max = cfg->alt_pid.Ki_max;
    /* RFLINK start */
    /* Configure RFLINK`s UART */
    if (cfg->rflink_enable)
        pilot.rflink_state.enable = 1;
    else
        pilot.rflink_state.enable = 0;
    /* Configure Mavlink */
    pilot.mav_system_type = cfg->mav_system_type;
    pilot.mav_autopilot_type = cfg->mav_autopilot_type;
    pilot.mav_system_mode = cfg->mav_system_mode;
    pilot.mav_custom_mode = cfg->mav_custom_mode;
    pilot.mav_system_state = cfg->mav_system_state;
    pilot.mavlink_system.sysid = cfg->mavlink_system.sysid;
    pilot.mavlink_system.type = cfg->mavlink_system.type;
    pilot.mav_msg.all_data = cfg->mav_msg.all_data;
    /* RFLINK stop */
}

/* UART configure */
void vProjdefsConfigureUART(PROJDEFS_CONF_UART_TYPE port)
{
    static unsigned int uart_port;
    static unsigned int uart_rate;
    static unsigned int uart_cfg;
    static unsigned int uart_ctrl;
    static unsigned int uart_fifo;
    static unsigned int uart_flags;
    static unsigned char uart_fl;

    uart_fl = 0;

    switch (port) {
    case UART_RFLINK:
        uart_port = pilot.rflink.uart_port;
        uart_rate = pilot.rflink.uart_rate;
        uart_cfg = pilot.rflink.uart_cfg;
        uart_ctrl = pilot.rflink.uart_ctrl;
        uart_fifo = pilot.rflink.uart_fifo;
        uart_flags = pilot.rflink.uart_flags;
        uart_fl = 1;
        break;
    }

    if (uart_fl)
    {
        UARTConfigure(uart_port, uart_cfg);
        UARTSetLineControl(uart_port, uart_ctrl);
        UARTSetFifoMode(uart_port, uart_fifo);
        UARTSetDataRate(uart_port, GetPeripheralClock(), uart_rate);
        UARTEnable(uart_port, UART_ENABLE_FLAGS(uart_flags));
        switch (port)
        {
        case UART_RFLINK:
            /* Configure UART Interrupt */
            INTDisableInterrupts();
            INTSetVectorPriority(RFLINK_UART_VECTOR, INT_PRIORITY_LEVEL_2);
            INTSetVectorSubPriority(RFLINK_UART_VECTOR, INT_SUB_PRIORITY_LEVEL_3);
            INTEnable(RFLINK_INT_RX, INT_ENABLED);
            INTEnableInterrupts();
            break;
        }
    }
}
